CREATE TABLE IF NOT EXISTS `unit_types`
(
  `unit_type_id` INT         NOT NULL AUTO_INCREMENT,
  `unit_type`    VARCHAR(50) NULL,
  PRIMARY KEY (`unit_type_id`)
);
