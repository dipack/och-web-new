CREATE TABLE IF NOT EXISTS `listing_locations`
(
  `listing_id` INT NOT NULL,
  `latitude`   DECIMAL(19, 7) DEFAULT 0.0,
  `longitude`  DECIMAL(19, 7) DEFAULT 0.0,
  PRIMARY KEY (`listing_id`),
  INDEX (`listing_id`)
)