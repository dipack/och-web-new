CREATE TABLE IF NOT EXISTS `localities`
(
  `postal_code` INT          NOT NULL,
  `name`        VARCHAR(100) NOT NULL,
  INDEX (`postal_code`)
)
  COMMENT = 'Localities, and their associated postal codes';
