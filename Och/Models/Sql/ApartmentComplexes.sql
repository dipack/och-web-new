CREATE TABLE IF NOT EXISTS `apartment_complexes`
(
  `apartment_id`          INT          NOT NULL AUTO_INCREMENT,
  `apartment_name`        VARCHAR(200) NULL,
  `listing_id`            INT          NOT NULL,
  `apartment_address`     VARCHAR(200) NULL,
  `contact_details_id`    INT          NOT NULL,
  `apartment_description` LONGTEXT     NULL,
  `apartment_url`         VARCHAR(200) NULL,
  `display_order`         INT          NULL DEFAULT 1,
  `listing_active`        INT          NULL DEFAULT 0,
  PRIMARY KEY (`apartment_id`),
  INDEX (`listing_id`)
)
  COMMENT = 'Detailed listings for apartment complexes';
