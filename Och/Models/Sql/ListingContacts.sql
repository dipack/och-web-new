CREATE TABLE IF NOT EXISTS `listing_contacts`
(
  `contact_id`      INT          NOT NULL AUTO_INCREMENT,
  `name`            VARCHAR(100) NOT NULL,
  `primary_phone`   VARCHAR(45)  NULL,
  `secondary_phone` VARCHAR(45)  NULL,
  `email`           VARCHAR(50)  NULL,
  PRIMARY KEY (`contact_id`),
  INDEX (`contact_id`)
)
  COMMENT = 'Table to store contact details. Will be referenced by Listings table.';
