#
# DUMP FILE
#
# Database is ported from MS Access
#------------------------------------------------------------------
# Created using "MS Access to MySQL" form http://www.bullzip.com
# Program Version 5.5.282
#
# OPTIONS:
#   sourcefilename=C:/Users/dipack/home/och/och-dipack-2019-02-19.mdb
#   sourceusername=
#   sourcepassword=
#   sourcesystemdatabase=
#   destinationdatabase=och-web
#   storageengine=InnoDB
#   dropdatabase=0
#   createtables=1
#   unicode=1
#   autocommit=1
#   transferdefaultvalues=1
#   transferindexes=1
#   transferautonumbers=1
#   transferrecords=1
#   columnlist=1
#   tableprefix=
#   negativeboolean=0
#   ignorelargeblobs=0
#   memotype=LONGTEXT
#   datetimetype=DATETIME
#

CREATE DATABASE IF NOT EXISTS `och-web`;
USE `och-web`;

#
# Table structure for table 'ApartmentComplexes'
#

DROP TABLE IF EXISTS `ApartmentComplexes`;

CREATE TABLE `ApartmentComplexes` (
  `id` INTEGER NOT NULL AUTO_INCREMENT, 
  `name` VARCHAR(255), 
  `listing_id` INTEGER, 
  `enhanced` TINYINT(1) DEFAULT 0, 
  `image_id` INTEGER, 
  `url` VARCHAR(255), 
  `description` LONGTEXT, 
  `address` VARCHAR(255), 
  `phone` VARCHAR(255), 
  `tracker_url` VARCHAR(255), 
  INDEX (`image_id`), 
  INDEX (`listing_id`), 
  PRIMARY KEY (`id`)
) ENGINE=innodb DEFAULT CHARSET=utf8;

SET autocommit=1;

#
# Dumping data for table 'ApartmentComplexes'
#

INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (2, 'Amherst Garden Association', 1226, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (3, 'Amherst Manor Apartments', 1227, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (4, 'Beechwood Manor Apartments', 1228, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (5, 'Bilbo Co.', 1229, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (6, 'Boulevard Tower Apartments', 1230, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (7, 'Camelot Court Apartments', 1231, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (8, 'Campus Manor', 1232, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (9, 'Colonie Apartments', 1233, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (10, 'Colvin Garden Apartments', 1234, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (11, 'Dannybrook Apartments', 1235, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (12, 'Drexel Hill Apartments', 1236, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (13, '1870 Apartment Rentals', 1237, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (14, 'Forrest Village', 1238, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (15, '400 Elmwood Apartments', 1239, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (16, 'Georgetown Apartments', 1240, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (17, 'Hertel Homes', 1241, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (18, 'Kenmore Development', 1242, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (19, 'Kensington Village', 1243, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (20, 'Millicent Town Houses', 1244, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (21, 'Olde Towne Village Apartments', 1245, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (22, 'Parkside Apartments', 1246, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (23, 'Princeton Court Apartments', 1247, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (24, 'Sturbrige Village Apartments', 1248, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (25, 'Sunrise Efficiency Apartments', 1249, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (26, 'University Village Apartments', 1071, 0, 64, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (27, 'Triad Apartments', 1250, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (28, 'Williams Manor Apartments', 1251, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (29, '2)1 Affinity Lane - Collegiate Village', 2273, 1, 484, 'http://www.cvbuffalo.com', 'Collegiate Village has just about everything a UB student is looking for!  Call for details on our financial aid payment plans. CV also has its own state of the art café complete with study rooms & a built- in convenience store.\r\n<br>-Choose between 1 bedroom, studio, 2 bedroom, 4 bedroom, and shared suite<br>-Graduate and undergraduate sections available<br>-Roommate matching<br>-Shuttles to UB North, South, shopping, entertainment, and more<br>-Fully furnished<br>-Washer and dryer in every apartment<br>-Fitness center<br>-Movie theater<br>Visit our website to see our early leasing specials!', '391 Eggert Road, Buffalo', '716-833-3700', 'http://subboard.com/och/resources/ads/cvillagerd.asp');
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (30, 'Lemontree Court', 2290, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (31, 'Raintree Island/Paradise Lane Apartments', 2765, 0, 854, 'Raintreeisland.com', '<a href=\"http://ParadiseLaneApts.com\">ParadiseLaneApts.com</a>', NULL, NULL, '\'http://Raintreeisland.com');
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (32, 'Colonie Apartments', 2863, 0, 910, 'www.colonieamherst.com', 'Colonie Apartments features bright, spacious apartments located one mile north of UB’s North Campus! We offer one and two bedroom apartments with lots of closet space and plenty of off-street parking. Our on-site pool and tennis court are waiting for you to indulge your active lifestyle. Welcome Home!', '7 Bristol Court, Amherst', '716-691-6448', 'http://subboard.com/och/resources/ads/colonierd.asp');
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (33, 'Woodlark Sidway Building', 2864, 0, 911, 'leasing@sidwaybuffalo.com', 'Apartment and loft-style living in the heart of downtown Buffalo! Welcome Home!', '775 Main Street, Buffalo', '716-852-4321', 'mailto:leasing@sidwaybuffalo.com');
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (35, '1) ACC- University Village at Sweethome', 1, 1, 1084, 'http://www.uvsweethome.com', '<br>\r\nApartments less than a mile from UB North Campus! Offering UB students their own privacy with individual bedrooms and bathrooms. We provide students with great amenities such as: <br><br>\r\n- Swimming pool/Hot tub<br>- 24 hour fitness center<br>\r\n- Coffee bar<br>\r\n- Business center with free printing<br>\r\n- BBQ Grills<br>\r\n- Shuttles to and from campus<br>\r\n- Washer & Dryer included<br>- Free Parking on-site<br><br>\r\nFor a limited time we are offering 5,10, and 12 month leases! All apartments are come fully furnished. Includes gas, water, and internet. Ride our shuttle from UB Rensch Loop to take a tour and see what we have to offer. Stop by our on campus office located in the UB Commons Suite 116 and ask about housing! Apply today!', '283 American Campus Drive <br> Amherst, NY 14228', '716-689-5800', 'http://subboard.com/och/resources/ads/uvsweethomerd.asp');
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (36, '6) ACC- Villas at Chestnut Ridge', 1, 1, 1085, 'http://www.villasatchestnutridge.com', '<br>\r\nLuxurious townhomes less than a mile from UB North Campus! Offering UB students their own privacy with individual bedrooms and bathrooms. We provide students with great amenities such as:<br><br>\r\n- 24 hour fitness center<br>\r\n- Coffee bar<br>\r\n- Hot tub<br>\r\n- Business center with free printing<br>\r\n- BBQ Grills<br>\r\n- Shuttles to and from campus<br>\r\n- Washer & Dryer included<br>- Free Parking on-site<br>-Newley Renovated Study Center<br><br>\r\nAll townhomes are fully furnished with black appliances and leather-style furniture. Includes gas , water, cable w/ HBO, and internet. Ride our shuttle from UB Rensch Loop to take a tour and see what we have to offer. Stop by our on campus office located in the UB Commons Suite 116 and ask about housing! Apply today!', '3751 Nickel Way<br> Amherst, NY 14228', '716-691-9600', 'http://subboard.com/och/resources/ads/villasrd.asp');
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (37, 'Copley Court Apartments', 1, 0, 1678, 'http://www.adcopropertiesinc.com/apt_profile_dtl.php?apt_id=4', 'Walking distance from UB.  A perfect find on Millersport Highway in Amherst, heat, water & high speed internet included in the rent. Apartments are equipped with new amenities, wall to wall carpeting, off-street parking and indoor locked parking available & Coin Operated laundry on premises.', '1345 Millersport Highway\r\nWilliamsville, NY, 14221', '716-631-0555', 'http://subboard.com/och/resources/ads/copley.asp');
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (38, 'Dockside Village Apartments', 1, 0, 1862, 'http://www.docksidevillageapartments.com/', 'Our 1, 2, and 3 bedroom apartments starting at 1095.00 per month include:\r\n \r\nFree cable\r\nAttached garage\r\nPrivate entrances\r\nGas fireplaces\r\nWasher and Dryer', '20 Dockside Parkway\r\nE. Amherst, NY 14051', '716-572-1002', 'http://subboard.com/och/resources/ads/dockside.asp');
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (39, '5) ACC- Villas on Rensch', 1, 1, 2235, 'http://www.villasonrensch.com', '<br>Buffalo’s most premier student community!  Ride our shuttle from UB Rensch Loop to take a tour and see what we have to offer.  Stop by our on-campus office located in the UB Commons Suite 116 and ask about housing! Apply today!\r\n<br><br>\r\n- 4 bedroom – 4 bath two-story townhomes<br>\r\n- Fully furnished with leather-style sectional sofa<br>\r\n- Hardwood-style flooring<br>\r\n- Stainless steel appliances<br>\r\n- Walk-in closets<br>\r\n- Washer & dryer included<br>\r\n- Hot tub<br>\r\n- 24 Hour, state-of-the-art Fitness Center<br>\r\n- Academic Success Center with PCs and free printing<br>\r\n- Group study tables with built-in outlets and TVs<br>\r\n- BBQ grills<br>\r\n- Free parking on-site<br>\r\n- Washer & Dryer Included<br>\r\n- Hot Tub<br>\r\n- State-of-the-art Fitness Center<br>\r\n- Game Room with Golden Tee & Big Buck Hunter Arcade Games<br>\r\n- Video Gaming Room<br>\r\n- BBQ Grills<br>\r\n- Free Parking on-site<br>', '100 Villas Drive East  Amherst, NY 14228', '716-932-7900', 'http://subboard.com/och/resources/ads/acc-rensch.asp');
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (40, 'Lofts at 136', NULL, 1, 2426, 'http://www.136lofts.com/', 'Welcome home UB students! The Lofts at 136 offers upscale urban living in gorgeous, spacious, Loft style apartments that fit your independent lifestyle.  Minutes away from the Buffalo Niagara Medical Campus, popular coffee shops, restaurants, professional and local theaters, and waterfront events; the Lofts at 136 is located just off the Interstate and right near public transportation. <p> Every Loft is fully furnished, including brand new state of the art appliances, utilities, cable, internet, and air conditioning.  Study suites are conveniently located on every floor, while the main floor features a Wilson Farms convenience store, a lounge with two flat screen t.v.s, gaming,  a projection screen, pool table, ping pong table, and fireplace.  The Lofts offers Wi-fi internet throughout the entire building,  24/7 onsite staff, and gated private parking.<p> With the price of our spacious beautiful Lofts on par with the cost of a campus cubicle, and less than the gouge of neglected apartment housing, your multiple choices are reduced to one! <p> Pictures do not do the Lofts justice!  Come take a tour, call us anytime, visit our website, or check us out on facebook!  Don’t forget to inquire about our Housing Scholarships!', NULL, NULL, 'http://subboard.com/och/resources/ads/136lofts.asp');
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (41, 'Alexander Estates and Beckingham Estate', 1, 0, 3140, 'http://www.wyseproperties.com', '<p>Alexander Estates and Beckingham Estates are luxury private entrance townhouse units featuring larger bedrooms, more spacious units, some with garages than any other complex in the UB North Campus area.  Beckingham Estates has 3 levels with 3 living rooms; one per floor.  Alexander Estates is a 3 story high townhouse.  Small complexes for that down home feel.\r\n\r\n<p>Located within a close proximity of UB North Campus and convenient to the I-990 and downtown locations.\r\n\r\n<p>We offer 4 and 5 bedroom units with 2 ¾ or 3 full baths.</br>\r\nOffered at a lower monthly rent per unit than other local complexes.</br> \r\nFully appliance including washer/dryer.</br>\r\nCentral air conditioning</br> \r\nSuperior Soundproofing</br>\r\nAll units designed townhouse style</br>\r\nDecks with storage areas</br>\r\n9-foot high ceilings on first floor</br>\r\nMini-blinds/verticals on all windows</br></p>\r\n\r\n<p>Visit our website for floor plans and leasing applications', '2211 Sweet Home Road-Alexander Estates\r\n\r\n<p>Business Address:\r\n\r\nP.O. Box 564\r\n\r\nGetzville, NY  14068', NULL, 'http://subboard.com/och/resources/ads/alexander.asp');
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (42, 'Bethune Lofts at The Buffalo Meter Company', NULL, 0, 3521, 'http:// www.bethunelofts.com/', 'Introducing Bethune Lofts at The Buffalo Meter Company -- loft living in an established residential neighborhood in North Buffalo. Located at 2917 Main Street near Hertel Avenue, the 87-unit complex takes full advantage of the area\'s many amenities and provides exceptional style, comfort and convenience to tenants.  The Metro Rail system is within 1 block of Bethune Lofts, the University at Buffalo\'s South Campus, and the Hertel Avenue shopping and dining district are in close proximity\r\n<p>One bedroom apartments are $950 - $1,400/month</p>\r\n\r\n<p>Two bedroom apartments are $1,200 - $2,050/month</p>', NULL, '2917 Main Street', 'http://subboard.com/och/resources/ads/bethunelofts.asp');
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (43, '\tDeer Lakes Apartments', 1, 0, 4142, 'http://www.DeerLakesApartments.com/', 'Newly built 1 & 2 bedroom units are available at Deer Lakes Apartments. Rents start at $1,055. In-unit laundry, all appliances, private patios & storage. Pet friendly! Near UB North Campus.\r\n\r\n<p>Amenities:\r\n•\t24 Hour Emergency Maintenance\r\n•\tAir Conditioner\r\n•\tCable Ready\r\n•\tDishwasher\r\n•\tDisposal\r\n•\tExtra Storage\r\n•\tGarages Available with Fee\r\n•\tLake Views\r\n•\tLarge Closets\r\n•\tMicrowave\r\n•\tNewly Built\r\n•\tOff-Street Parking\r\n•\tOn-Site Management\r\n•\tPatio/Balcony\r\n•\tRange/Oven\r\n•\tRefrigerator\r\n•\tWasher/Dryr', '3416 Deer Lakes Drive\r\nAmherst NY 14228', '716-923-6622', NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (44, 'London Towne Apartments', 1, 0, 4143, 'http://www.LondonTowneApartmentsMJP.com', 'RENT SPECIAL* at London Towne Apartments! Beautiful 1 bedroom units starting at $880! Rent includes heat, water & basic cable. All kitchen appliances, laundry facilities, swimming pool & tennis court. Pet friendly! Near UB North Campus.\r\n\r\n<p>Amenities:\r\n•\t24 Hour Emergency Maintenance\r\n•\tAir Conditioner\r\n•\tCable Ready\r\n•\tCourtyard\r\n•\tDishwasher\r\n•\tDisposal\r\n•\tExtra Storage\r\n•\tGarages Available with Fee\r\n•\tLarge Closets\r\n•\tLaundry Facilities\r\n•\tOff-Street Parking\r\n•\tOn-Site Management\r\n•\tRange/Oven\r\n•\tRefrigerator\r\n•\tSmall Pet Allowed with Pet Fee\r\n•\tSwimming Pool\r\n•\tTennis Court\r\n<p>*Rent Special is available for a limited time only and is based on availability. Restrictions may apply. Contact us for details.', '4453 Chestnut Ridge Road', '716-691-9088', NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (45, 'Peppertree Village Apartments', 1, 0, 4144, 'http://www.PeppertreeVillageApartments.com', 'RENT SPECIAL* at Peppertree Village Apartments! Spacious 1 bedroom units starting at $765! Rent includes heat, water & basic cable. All kitchen appliances, laundry facilities and swimming pool. Pet friendly! Near UB North Campus.\r\n\r\n<p>Amenities:\r\n•\t24 Hour Emergency Maintenance\r\n•\tAir Conditioner\r\n•\tCable Ready\r\n•\tCourtyard\r\n•\tDishwasher\r\n•\tDisposal\r\n•\tExtra Storage\r\n•\tGarages Available with Fee\r\n•\tLarge Closets\r\n•\tLaundry Facilities\r\n•\tOff-Street Parking\r\n•\tOn-Site Management\r\n•\tRange/Oven\r\n•\tRefrigerator\r\n•\tSmall Pet Allowed with Pet Fee\r\n•\tSwimming Pool\r\n<p>*Rent Special is available for a limited time only and is based on availability. Restrictions may apply. Contact us for details.', '153 Peppertree Drive', '716-691-8333', NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (46, '\tLiberty Square Apartments', 1, 0, 4145, 'http://www.LibertySquareApartments.com', 'RENT SPECIAL* at Liberty Square Apartments! Large 1 bedroom units starting at $865! Rent includes heat, water & basic cable. All kitchen appliances, laundry facilities, patios/balconies & central air. Pet friendly! Near UB North Campus.\r\n\r\n<p>Amenities:\r\n•\t24 Hour Emergency Maintenance\r\n•\tCable Ready\r\n•\tCentral Air\r\n•\tCourtyard\r\n•\tDishwasher\r\n•\tDisposal\r\n•\tExtra Storage\r\n•\tGarages Available with Fee\r\n•\tLarge Closets\r\n•\tLaundry Facilities\r\n•\tOff-Street Parking\r\n•\tOn-Site Management\r\n•\tPatio/Balcony\r\n•\tRange/Oven\r\n•\tRefrigerator\r\n•\tSmall Pet Allowed with Pet Fee\r\n<p>*Rent Special is available for a limited time only and is based on availability. Restrictions may apply. Contact us for details.', '4363 Chestnut Ridge Road\r\nAmherst, NY 14228', '716-691-7083', NULL);
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (47, '4) 2091 North Sweet Home Road – Twenty91 North', 1, 1, 6481, 'http://www.Twenty91North.com', 'Twenty91 North – For a limited time, we are offering $639/month for our 4 bedroom units - this is an amazing deal and the lowest rates we\'ve offered.<br>Opened fall of 2016. We offer a free 5-minute shuttle ride to UB North Campus SIX days a week and to Wegman’s on Saturday’s. The shuttle also runs during winter and summer break. The apartments are built with the modern and busy student in mind. All of our amenities are accessible 24 hours a day. We host an array of monthly events for socializing and meeting new people. Sign online today and claim your space! Call for monthly specials and incentive.<br>\r\n Twenty91 North is a luxury, gated community. All apartments include<br>\r\n-Fully furnished apartments with bedroom and living room furniture<br>\r\n-Private bathroom inside your bedroom<br>\r\n-Full size washer/dryer in unit<br>\r\n-All kitchen appliances included<br>\r\n-Spacious kitchens with plenty of storage<br>\r\n-Walk in closets in every bedroom<br>\r\n-Cable/Water/Internet/Gas allowance included with monthly rate<br>\r\n <br>\r\nProperty amenities include: <br>\r\n-24 Hour business center <br>\r\n-24 Hour fitness center <br>\r\n-Private study lounges <br>\r\n-Gaming and billiard room <br>\r\n-40 person hot tub with expansive pool area <br>\r\n-Gas grills and outdoor fire pit <br>\r\n-Pet friendly <br>\r\n-Online Leasing and bill pay <br>\r\n-Able to cater to large groups and International residents <br>\r\n\r\n\r\nWe are a luxury, gated community. All apartments are furnished and individuals can select either 2 or 4 bedroom \r\n\r\nfloorplans. Keep in mind that we’re also pet friendly! Each resident will have exclusive access to our full list of \r\n\r\namenities:\r\n<br>\r\n-Free Shuttle to UB North Campus with expanded weekend service to local shopping and grocery stores <br>\r\n\r\n-Gated Community on wooded lot <br>\r\n\r\n-Heated Pool <br>\r\n\r\n-24-Person Hot Tub <br>\r\n\r\n-Mac Lab (Free Printing) <br>\r\n\r\n-Fitness Center (24 Hours) <br>\r\n\r\n-Outdoor Fire Pits <br>\r\n\r\n-Outdoor Gas Grills <br>\r\n\r\n-Free Tanning <br>\r\n\r\n-4 Study Rooms <br>\r\n\r\n-Free-Wi-Fi and Cable <br>\r\n\r\n-Private Bathroom in each bedroom <br>\r\n\r\n-Walk-in Closets in each bedroom <br>\r\n\r\n-Full-Size washer and dryer in every apartment <br>\r\n\r\n-Fully Furnished <br>\r\n\r\n-Big windows, lots of light <br>\r\n\r\n-Water and Gas Cap included <br>\r\n\r\n-Individual leases <br>\r\n\r\nVisit our website for images and leasing specials!', '2091 North Sweet Home Road, Amherst, NY', '716-748-6267', 'http://subboard.com/och/resources/ads/twenty91north.asp');
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (48, '2) Absolute Location- Axis 360', 1, 0, 5516, 'http://www.liveAXIS360.com', 'Axis 360 – BRAND NEW urban student housing opening August 2016! Located in the University Heights District, 5 blocks away from UB South, 1 block from Hertel Ave, 1 block from the LaSalle Metro Station, and minutes from downtown, you’ll be right in the middle of the action! \r\nOur gated, pet friendly community will feature a huge clubhouse that includes an indoor/outdoor 24 hour fitness center, a pool sized hot tub, and individual study rooms. \r\n<br><br>\r\nAll residents will have exclusive access to our amenities: <br><br>\r\n-Blocks from UB South, The Metro to downtown and all the fun in the University Heights area <br>\r\n-Super High Speed Fiber Internet <br>\r\n-Free Shuttle to UB South Campus <br>\r\n-25 Person Hot Tub with heated edge <br>\r\n-Outdoor entertainment area with fire pits and gas grills <br>\r\n-24 Hour Fitness Center including Fitness on Demand and an outdoor fitness track <br>\r\n-24 Hour Computer Bar with Free Printing <br>\r\n-Juice Bar and lounge area<br>\r\n-Free-Wi-Fi and Cable <br>\r\n-Individual Bathroom in each bedroom <br>\r\n-Full-Size washer and dryer in every apartment <br>\r\n-Fully Furnished High End Finishes – Granite Countertops and Stainless Appliances <br>\r\n-Roommate Matching <br>\r\n-Individual leases <br>\r\nPlus get a $100 when you refer a Friend!<br>', '83 Lasalle Ave, Buffalo, NY', '716-836-2000', 'http://subboard.com/och/resources/ads/axis360.asp');
INSERT INTO `ApartmentComplexes` (`id`, `name`, `listing_id`, `enhanced`, `image_id`, `url`, `description`, `address`, `phone`, `tracker_url`) VALUES (49, '3) Muir Lake Luxury Apartments', 1, 1, 5953, NULL, '<a href=\"http://www.glendaledev.com/muir-lake-apartments\">http://www.glendaledev.com/muir-lake-apartments</a><br>Perfectly located directly behind UB on N Forest Rd you can walk, bike or ride to school.  Be the first to live in these brand new, gorgeous, one-of-a-kind luxury apartments right on Muir Lake.  Amenities include:<br><br>\r\n\r\n \r\n\r\n- Stainless Steel Full-Size Appliances<br>\r\n- In-Unit Laundry with Washer & Dryer<br>\r\n- Quartz Countertops<br>\r\n- Vinyl Plank Flooring<br>\r\n- Spacious Apartments Perfect for Singles or Roommates<br>\r\n- Balconies or Patios<br>\r\n- Elevators<br>\r\n- Off-Street Parking<br>\r\n- Garages Available<br>\r\n- Storage Available<br>\r\n- Access to Walton Pond Center\'s Clubhouse, Pool and Tennis Courts<br>\r\n- Quick Access to the 990 and 290<br>\r\n- Minutes from the Amherst Bike & Walking Trails, UB Campus, Boulevard Mall, Wegmans, Tops & other area shopping, dining & attractions.<br><br>\r\n\r\n1 Bedroom/1 Bathroom and 2 Bedroom/2 Bathroom Apartments are Available for Lease.  Ask about our Move-In Specials!<br>\r\n\r\nCall or Email to take a tour!  716-253-6777 or jgasbarre@glendaledev.com<br>', '2371 North Forest Road<br>\r\nGetzville, NY 14068', '716-253-6777', NULL);
# 47 records

#
# Table structure for table 'Areas'
#

DROP TABLE IF EXISTS `Areas`;

CREATE TABLE `Areas` (
  `id` INTEGER NOT NULL AUTO_INCREMENT, 
  `name` VARCHAR(255), 
  `description` LONGTEXT, 
  INDEX (`id`), 
  PRIMARY KEY (`id`)
) ENGINE=innodb DEFAULT CHARSET=utf8;

SET autocommit=1;

#
# Dumping data for table 'Areas'
#

INSERT INTO `Areas` (`id`, `name`, `description`) VALUES (27, 'University Heights', '<h4>Location</h4>\r\n<p>The northern half of zip code 14214 and 14215, off Main Street, North of Hertel Avenue</p>\r\n\r\n<h4>Rents</h4>\r\n<p>Rents average between $500 and $800 per apartment.  Many landlords in the University Heights will rent space by the bedroom rather than the entire apartment.  In these cases rents is usually between $225 and $400 per person.  Utilities, such as heat, natural gas and electricity are not included in most rent.  Be sure to ask the landlord about this when deciding what is affordable.</p>\r\n\r\n<h4>Safety Tips</h4>\r\n<p>Walk with a friend after dark, and make use of the <a href=\'/he/artf/shuttle.asp\'>ARTF Safety Shuttle</a> whenever possible. Also, try to maintain a well-lit property (inside and out) and lock doors and windows when not at home or while sleeping (including upper floors).</p>\r\n\r\n<h4>Transportation</h4>\r\n<p>It is an easy walk from one end of the University Heights to the other.  Additionally the Main Street bus runs regularly until 6:00 p.m. and hourly after that.  A shuttle bus runs between UB\'s north and south campuses.  There is no cost for UB students to ride this.  In the evenings from 8:00 pm until midnight, during the academic year the Anti-Rape Task force runs a <a href=\'http://subboard.com/he/artf/shuttle.asp\'>shuttle van</a> which will drive students to any address within 1.5 miles of the South Campus.</p>\r\n\r\n<h4>Things to Do</h4>\r\n<p>The University Heights is a vibrant neighborhood with many shops, restaurants and bars.  Additionally, there is a shopping plaza which is home to a movie theater and a large grocery store as well as more shops and restaurants.</p>\r\n\r\n<h4>More information</h4>\r\n<ul>\r\n\t<li><a href=\'http://en.wikipedia.org/wiki/University_Heights,_Buffalo,_New_York\' target=\'_blank\'>Wikipedia article</a></li>\r\n\t<li><a href=\'http://library.buffalo.edu/maps/buffalo-wnymaps/buffalo_neighborhoods.php#universityheights\' target=\'_blank\'>UB Libraries page</a></li>\r\n</ul>');
INSERT INTO `Areas` (`id`, `name`, `description`) VALUES (28, 'Buff State', '<p>Area information to come.</p>');
INSERT INTO `Areas` (`id`, `name`, `description`) VALUES (29, 'Elmwood Village', '<h4>Location</h4>\r\n<p>This Buffalo neighborhood centers on Elmwood Avenue from Forest Avenue south to North Street.  The Elmwood \"strip\" commercial district includes a variety of cafes, ethnic restaurants, nightclubs and specialty shops catering to neighborhood residents and the students of nearby Buffalo State College.</p>\r\n<h4>Rents</h4>\r\n<p>Rent in this neighborhood averages $500 per person.  Many students that attend Buffalo State College live in this neighborhood.  The blocks running off of Elmwood Avenue are lined with fine examples of residential Victorian architecture and are more expensive properties. Leaded glass windows and wooden \"tracery\" embellish these older single and multiple family houses. </p>\r\n<h4>Things to Do</h4>\r\n<p>According to the American Planning Association the Elmwood Village neighborhood in Buffalo is ranked the third best neighborhood in America.  Elmwood Village is a mixed use neighborhood with hundreds of small, home grown stores and restaurants.  If looking for something out of the ordinary, the Elmwood Village Association hosts events regularly throughout the year that range from festivals to candy giveaways.</p>\r\n\r\n\r\n<h4>More information</h4>\r\n<ul>\r\n<li><a href=\'http://www.foreverelmwood.org/\' target=\'_blank\'>Elmwood Village Association</a></li>\r\n\t<li><a href=\'http://library.buffalo.edu/maps/buffalo-wnymaps/buffalo_neighborhoods.php#elmwoodvillage\' target=\'_blank\'>UB Libraries page</a></li>\r\n</ul>');
INSERT INTO `Areas` (`id`, `name`, `description`) VALUES (30, 'Downtown', '<h4>Location</h4>\r\n<p>Downtown Buffalo centers on Main Street from Goodell Street to the Buffalo River. Within this two mile area are three separate centers: the Theater District in the north, the civic and commercial center around Niagara and Lafayette Squares, and the Waterfront area at the south end.</p>\r\n<h4>Rents</h4>\r\n<p>Rent for apartments in the downtown area greatly vary and range from $400-$2,000.  The rent depends on the location of the apartment, and the amenities and luxuries included.  Go to http://www.buffaloplace.com/live/apartmentcondo.html for a list of available apartments in the Downtown area.</p>\r\n<h4>Transportation</h4>\r\n<p>The Metro Rail is a single line rail that extends from downtown Buffalo to the University Heights district in north Buffalo.  The downtown section of the line is operated above ground and is free of charge to passengers. Outside the downtown area the line transitions to an underground system until it reaches the end of the line at University Heights. Passengers pay a fee to ride this section of the rail.  The NFTA operates a public transit system of busses throughout the Downtown and greater Buffalo region.</p>\r\n<h4>Things to Do</h4>\r\n<p>Downtown Buffalo is the center of entertainment.  It contains a successful Theatre District that possesses everything from Broadway Theater to local productions.  Chippewa street is Buffalo’s entertainment strip that contains a variety of bars, restaurants, and dance clubs.  Buffalo is also home to the NHL Buffalo Sabres and the AAA baseball team, the Buffalo Bisons.  Whether catching a hockey match in the HSBC arena or sharing a drink at Sky Bar, Downtown Buffalo is always thriving with excitement.</p>\r\n<h4>More information</h4>\r\n<ul>\r\n\t<li><a href=\'http://www.buffaloplace.com/\' target=\'_blank\'>Downtown Buffalo Webpage</a></li>\r\n\t<li><a href=\'http://en.wikipedia.org/wiki/Buffalo%2C_New_York\' target=\'_blank\'>Wikipedia article</a></li>\r\n        <li><a href=\'http://library.buffalo.edu/maps/buffalo-wnymaps/buffalo_neighborhoods.php#downtown\' target=\'_blank\'>UB Libraries page</a></li>\r\n</ul>');
INSERT INTO `Areas` (`id`, `name`, `description`) VALUES (31, 'Allentown', '<h4>Location</h4>\r\n<p>Allentown is the first neighborhood north of the Downtown Buffalo core. It borders the downtown theater and entertainment district to its south, and runs north to North Street at its northern edge. Running from Orton Place on the west, to Main Street on the east, the neighborhood is generally centered around Allen Street and Elmwood Avenue.</p>\r\n<h4>Rents</h4>\r\n<p>A one-bedroom apartment ranges from $300-$600 per month.</p>\r\n<h4>Transportation</h4>\r\n<p>Allentown was built before the automobile, and thankfully is more convenient for pedestrians than cars. Almost everything is within walking distance.  The downtown center is just a 20-minute walk south, or a 5-minute subway ride down Main Street.  Allentown is in very close proximity to NFTA bus and subway lines.</p>\r\n<h4>Things to Do</h4>\r\n<p>Allentown is one of Buffalo’s busiest and most cozy urban neighborhoods.  It is one of Buffalo’s premier areas for nightlife, dining, and antique shopping.  During the summer, there is an annual tour of gardens, tour of the beautiful old homes, and a giant art festival.  The are also plenty of restaurants and bars, diners and eateries, as well as some coffee shops, beer and wine cafes, and small art galleries.  Not to mention Kleinhans Music Hall, which is the premier music venue in Buffalo and home of the Buffalo Philharmonic Orchestra.</p>\r\n\r\n<h4>More information</h4>\r\n<ul>\r\n\t<li><a href=\'http://www.allentown.org/\' target=\'_blank\'>Allentown Webpage</a></li>\r\n\t<li><a href=\'http://en.wikipedia.org/wiki/Allentown%2C_Buffalo%2C_New_York\' target=\'_blank\'>Wikipedia article</a></li>\r\n        <li><a href=\'http://library.buffalo.edu/maps/buffalo-wnymaps/buffalo_neighborhoods.php#allentown\' target=\'_blank\'>UB Libraries page</a></li>\r\n</ul>');
INSERT INTO `Areas` (`id`, `name`, `description`) VALUES (32, 'North Buffalo/Hertel', '<h4>Location</h4>\r\n<p>North Buffalo stretches north from Amherst Street to Kenmore Avenue and East from Elmwood Avenue to Main Street. It is bounded to the north by Kenmore, to the south by North Park, Parkside, and Central Park. To the west is Black Rock and to the east is University Heights.\r\n</p>\r\n<h4>Rents</h4>\r\n<p>Because of its pedestrian-oriented environment; proximity to downtown Buffalo, the University at Buffalo, and suburban office parks; and high-quality 1920s-era housing stock, North Buffalo is experiencing an influx of young professional homebuyers.  For this reason, rent in North Buffalo is more expensive than the typical college student neighborhoods.</p>\r\n<h4>Transportation</h4>\r\n<p>The area is serviced by the <a href=\'http://www.nfta.com/metro/\' target=\'_blank\'>NFTA</a> buses on Delaware Ave. (No. 25), Colvin (No. 11) and Elmwood (No. 20).</p>\r\n<h4>Things to Do</h4>\r\n<p>The neighborhood is a stable residential community typified by older duplex homes. Hertel Avenue has always supported a healthy commercial district and today is lined with antique shops, restaurants and Buffalo\'s most noted movie theater for independent films, the North Park Theatre.</p>');
INSERT INTO `Areas` (`id`, `name`, `description`) VALUES (33, 'Parkside', '<h4>Location</h4>\r\n<p>This Buffalo neighborhood centers on Jewett Avenue from Parkside to Main Street.</p>\r\n<h4>Rents</h4>\r\n<p>Rent in Parkside is typically more expensive than the average student neighborhood.  Due to its proximity with Delaware Park and the fact that it is mainly a family oriented area, you can expect to pay $500-$1000 per person.</p>\r\n<h4>Transportation</h4>\r\n<p>Whether walking, driving, or biking, the Parkside district is conveniently located. The Scajaquada Expressway (Route 198, or \"the 198\") links the neighborhood to the major highways of Buffalo and the bridges to Canada. Under Main Street, at the eastern boundary of the neighborhood, the Metro Rail subway system connects the neighborhood with both downtown Buffalo to the west, and with the University at Buffalo\'s South Campus to the east. Parkside is also served by several bus lines. For more information about Metro Rail and bus schedules, visit the Niagara Frontier Transportation Authority.</p>\r\n<h4>Things to Do</h4>\r\n<p>Parkside is located in the heart of Buffalo\'s cultural corridor, and contains such attractions as Delaware Park, the Buffalo Zoo, and the Darwin D. Martin House within its boundaries. In addition, the Albright-Knox Art Gallery, the Buffalo and Erie County Historical Society, and Forest Lawn Cemetery are just outside Parkside.</p>\r\n<h4>More information</h4>\r\n<ul>\r\n        <li><a href=\'http://www.parksidebuffalo.org/\' target=\'_blank\'>UB Libraries page</a></li>\r\n\t<li><a href=\'http://library.buffalo.edu/maps/buffalo-wnymaps/buffalo_neighborhoods.php#parkside\' target=\'_blank\'>UB Libraries page</a></li>\r\n</ul>');
INSERT INTO `Areas` (`id`, `name`, `description`) VALUES (35, 'Amherst', '<h4>Location</h4>\r\n<p>Amherst is a town in Erie County and is located northeast of the City of Buffalo.  It is the largest and most populated suburb of Buffalo.  Amherst is bordered on the north by Tonawanda Creek and Niagara County.  University at Buffalo’s North Campus is located in Amherst.</p>\r\n<h4>Rents</h4>\r\n<p>The average rent for one bedroom apartments in Amherst is $700.  However, there are many apartment building complexes specifically for college students that have individual rents starting from $400.  Check the <a href=\'http://subboard.com/och/apartments.asp\' target=\'_blank\'>apartment buildings page</a> for more info on apartment buildings.</p>\r\n<h4>Safety Information</h4>\r\n<p>Based on statistics reported to the Federal Bureau of Investigation, Amherst is the second safest city in the United States. The designation is based on crime statistics for the year 2005 in six categories: murder, rape, robbery, aggravated assault, burglary, and auto theft. Recently, it dropped to 4th place in the safest cities of USA in the 2007 study.</p>\r\n<h4>Transportation</h4>\r\n<p>A free shuttle bus between North Campus and South Campus is offered by the University at Buffalo for UB students.  Once on south campus, Buffalo’s NFTA transportation system is within seconds of the shuttle bus stop.   Public bus transportation in Amherst is limited and having a car would be highly recommended.</p>\r\n<h4>Things to Do</h4>\r\n<p>Amherst is filled with many dining, shopping, and entertainment locations.  The town’s major mall, Boulevard mall, is filled with many useful stores and is a popular location for students.  Strip malls are very prevalent as well as steakhouse type restaurants.  Movie theaters are scattered throughout the town and are another popular activity for students. Unfortunately, many places of interest are relatively far apart and would require a car for transportation.</p>\r\n<h4>More information</h4>\r\n<ul>\r\n\t<li><a href=\'http://www.amherst.ny.us/default.asp\' target=\'_blank\'>Town of Amherst Webpage</a></li>\r\n\t<li><a href=\'http://en.wikipedia.org/wiki/Amherst,_New_York\' target=\'_blank\'>Wikipedia Article</a></li>\r\n</ul>');
INSERT INTO `Areas` (`id`, `name`, `description`) VALUES (36, 'Tonawanda', '<h4>Location</h4>\r\n<p>The City of Tonawanda is located in Western New York alongside the Niagara River and Erie Canal, and boarders the Town of Tonawanda to the south, east, and west, with borders to North Tonawanda north of the Erie Canal.</p>\r\n<h4>Rents</h4>\r\n<p>Rent is typically $500-$800 in this area.</p>\r\n<h4>Transportation</h4>\r\n<p>Public transportation is very limited in Tonawanda.  It is necessary to have a car if living in this neighborhood.</p>\r\n<h4>Things to Do</h4>\r\n<p>Tonawanda hosts a variety of fun events to keep you entertained.  The biggest event is the annual Canal Festival, which celebrates Tonawanda’s historic location on the end of the Erie Canal.  Gateway Harbor is a popular place to go during the summer with many free concerts and public parks. If looking for something more extreme, there is a local skate park, hockey rink, soccer field, and tennis court in the middle of Tonawanda.</p>\r\n<h4>More information</h4>\r\n<ul>\r\n\t<li><a href=\'http://www.ci.tonawanda.ny.us/\' target=\'_blank\'>City of Tonawanda Webpage</a></li>\r\n\t<li><a href=\'http://en.wikipedia.org/wiki/Tonawanda_%28city%29,_New_York\' target=\'_blank\'>Wikipedia article</a></li>\r\n</ul>');
# 9 records

#
# Table structure for table 'Areas_PostalCodes'
#

DROP TABLE IF EXISTS `Areas_PostalCodes`;

CREATE TABLE `Areas_PostalCodes` (
  `postal_code` INTEGER, 
  `area_id` INTEGER, 
  INDEX (`area_id`)
) ENGINE=innodb DEFAULT CHARSET=utf8;

SET autocommit=1;

#
# Dumping data for table 'Areas_PostalCodes'
#

INSERT INTO `Areas_PostalCodes` (`postal_code`, `area_id`) VALUES (14214, 27);
INSERT INTO `Areas_PostalCodes` (`postal_code`, `area_id`) VALUES (14215, 27);
INSERT INTO `Areas_PostalCodes` (`postal_code`, `area_id`) VALUES (14226, 27);
INSERT INTO `Areas_PostalCodes` (`postal_code`, `area_id`) VALUES (14216, 28);
INSERT INTO `Areas_PostalCodes` (`postal_code`, `area_id`) VALUES (14213, 28);
INSERT INTO `Areas_PostalCodes` (`postal_code`, `area_id`) VALUES (14222, 28);
INSERT INTO `Areas_PostalCodes` (`postal_code`, `area_id`) VALUES (14207, 28);
INSERT INTO `Areas_PostalCodes` (`postal_code`, `area_id`) VALUES (14216, 29);
INSERT INTO `Areas_PostalCodes` (`postal_code`, `area_id`) VALUES (14213, 29);
INSERT INTO `Areas_PostalCodes` (`postal_code`, `area_id`) VALUES (14222, 29);
INSERT INTO `Areas_PostalCodes` (`postal_code`, `area_id`) VALUES (14207, 29);
INSERT INTO `Areas_PostalCodes` (`postal_code`, `area_id`) VALUES (14203, 30);
INSERT INTO `Areas_PostalCodes` (`postal_code`, `area_id`) VALUES (14201, 31);
INSERT INTO `Areas_PostalCodes` (`postal_code`, `area_id`) VALUES (14216, 32);
INSERT INTO `Areas_PostalCodes` (`postal_code`, `area_id`) VALUES (14216, 33);
# 15 records

#
# Table structure for table 'Flags'
#

DROP TABLE IF EXISTS `Flags`;

CREATE TABLE `Flags` (
  `id` INTEGER NOT NULL AUTO_INCREMENT, 
  `listing_id` INTEGER, 
  `date_flagged` DATETIME, 
  `ip_address` VARCHAR(255), 
  `reason` LONGTEXT, 
  INDEX (`listing_id`), 
  PRIMARY KEY (`id`)
) ENGINE=innodb DEFAULT CHARSET=utf8;

SET autocommit=1;

#
# Dumping data for table 'Flags'
#

# 0 records

#
# Table structure for table 'Images'
#

DROP TABLE IF EXISTS `Images`;

CREATE TABLE `Images` (
  `id` INTEGER NOT NULL AUTO_INCREMENT, 
  `listing_id` INTEGER, 
  INDEX (`listing_id`), 
  PRIMARY KEY (`id`)
) ENGINE=innodb DEFAULT CHARSET=utf8;

SET autocommit=1;

#
# Dumping data for table 'Images'
#

# 6746 records

#
# Table structure for table 'Listings'
#

DROP TABLE IF EXISTS `Listings`;

CREATE TABLE `Listings` (
  `id` INTEGER NOT NULL AUTO_INCREMENT, 
  `status` VARCHAR(255), 
  `street_number` INTEGER DEFAULT 0, 
  `street` VARCHAR(35), 
  `locality` VARCHAR(255), 
  `Field1` TINYINT(1) DEFAULT 0, 
  `postal_code` INTEGER, 
  `area` VARCHAR(255), 
  `apartment_number` VARCHAR(8), 
  `unit_type_id` INTEGER, 
  `total_bedrooms` INTEGER DEFAULT 0, 
  `available_bedrooms` INTEGER DEFAULT 0, 
  `date_available` DATETIME, 
  `date_listed` DATETIME, 
  `lease` VARCHAR(10), 
  `term` DOUBLE NULL DEFAULT 0, 
  `rent` DECIMAL(19,4) DEFAULT 0, 
  `security_deposit` DECIMAL(19,4) DEFAULT 0, 
  `heat` TINYINT(1), 
  `gas` TINYINT(1), 
  `water` TINYINT(1), 
  `electric` TINYINT(1), 
  `internet` TINYINT(1), 
  `cable` TINYINT(1), 
  `stove` TINYINT(1), 
  `refrigerator` TINYINT(1), 
  `off-street_parking` TINYINT(1), 
  `garage` TINYINT(1), 
  `dogs_allowed` TINYINT(1) DEFAULT 0, 
  `cats_allowed` TINYINT(1), 
  `insulated` TINYINT(1), 
  `garbage_collection` TINYINT(1) DEFAULT 0, 
  `central_air` TINYINT(1) DEFAULT 0, 
  `storm_windows` TINYINT(1), 
  `furnished` TINYINT(1), 
  `washer` TINYINT(1), 
  `dryer` TINYINT(1), 
  `males_preferred` TINYINT(1), 
  `females_preferred` TINYINT(1), 
  `grad_students_preferred` TINYINT(1), 
  `med_students_preferred` TINYINT(1), 
  `dental_students_preferred` TINYINT(1), 
  `law_students_preferred` TINYINT(1), 
  `non-smoker_preferred` TINYINT(1), 
  `any` TINYINT(1), 
  `other_preferred` VARCHAR(50), 
  `comments` LONGTEXT, 
  `rent_for` VARCHAR(50) DEFAULT 'per person', 
  `picture` TINYINT(1), 
  `contact_name` VARCHAR(50), 
  `contact` VARCHAR(50) DEFAULT 'Landlord', 
  `email` VARCHAR(100), 
  `primary_phone` VARCHAR(15), 
  `secondary_phone` VARCHAR(15), 
  `best_time_to_call` VARCHAR(80), 
  `building_and_surrounding_area_illuminated_at_night` TINYINT(1) DEFAULT 0, 
  `working_smoke_detectors` TINYINT(1) DEFAULT 0, 
  `security_system` TINYINT(1) DEFAULT 0, 
  `exterior_door_has_deadbolt` TINYINT(1) DEFAULT 0, 
  `doors,_stairs,_fire_escapes_unobstructed_and_in_good_condition` TINYINT(1) DEFAULT 0, 
  `carbon_monoxide_detectors` TINYINT(1) DEFAULT 0, 
  `fire_extinguishers` TINYINT(1) DEFAULT 0, 
  `electrical_wiring_up_to_code` TINYINT(1) DEFAULT 0, 
  `bedrooms_not_located_in_basement_or_attic` TINYINT(1) DEFAULT 0, 
  `Verified` TINYINT(1) DEFAULT 0, 
  `Not_Verified` TINYINT(1) DEFAULT 0, 
  `No_Certificate` TINYINT(1) DEFAULT 0, 
  `N/A` TINYINT(1) DEFAULT 0, 
  `preferred_gender` VARCHAR(255), 
  `registered_with_rental_registry` TINYINT(1) DEFAULT 0, 
  INDEX (`street_number`), 
  INDEX (`postal_code`), 
  PRIMARY KEY (`id`)
) ENGINE=innodb DEFAULT CHARSET=utf8;

SET autocommit=1;

#
# Dumping data for table 'Listings'
#

INSERT INTO `Listings` (`id`, `status`, `street_number`, `street`, `locality`, `Field1`, `postal_code`, `area`, `apartment_number`, `unit_type_id`, `total_bedrooms`, `available_bedrooms`, `date_available`, `date_listed`, `lease`, `term`, `rent`, `security_deposit`, `heat`, `gas`, `water`, `electric`, `internet`, `cable`, `stove`, `refrigerator`, `off-street_parking`, `garage`, `dogs_allowed`, `cats_allowed`, `insulated`, `garbage_collection`, `central_air`, `storm_windows`, `furnished`, `washer`, `dryer`, `males_preferred`, `females_preferred`, `grad_students_preferred`, `med_students_preferred`, `dental_students_preferred`, `law_students_preferred`, `non-smoker_preferred`, `any`, `other_preferred`, `comments`, `rent_for`, `picture`, `contact_name`, `contact`, `email`, `primary_phone`, `secondary_phone`, `best_time_to_call`, `building_and_surrounding_area_illuminated_at_night`, `working_smoke_detectors`, `security_system`, `exterior_door_has_deadbolt`, `doors,_stairs,_fire_escapes_unobstructed_and_in_good_condition`, `carbon_monoxide_detectors`, `fire_extinguishers`, `electrical_wiring_up_to_code`, `bedrooms_not_located_in_basement_or_attic`, `Verified`, `Not_Verified`, `No_Certificate`, `N/A`, `preferred_gender`, `registered_with_rental_registry`) VALUES (9569, 'Active', 21, 'Englewood', 'Buffalo', 0, 14214, 'University Heights', 'Upper', 3, 5, 5, '2019-08-01 00:00:00', '2019-02-14 00:00:00', 'Yes', 12, 375, 475, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 'Across the street from south campus! Extra large two floor apartment with 5 bedrooms and two bathrooms, fully renovated kitchen with all appliances included.  Renovated bathrooms.  Fully furnished with all furniture.  Free laundry and off street parking.', 'per room', 0, 'Marie', 'Landlord seeking tenants', 'mak318@hotmail.com', '716-570-8867', NULL, '8am-8pm', 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, NULL, 0);
INSERT INTO `Listings` (`id`, `status`, `street_number`, `street`, `locality`, `Field1`, `postal_code`, `area`, `apartment_number`, `unit_type_id`, `total_bedrooms`, `available_bedrooms`, `date_available`, `date_listed`, `lease`, `term`, `rent`, `security_deposit`, `heat`, `gas`, `water`, `electric`, `internet`, `cable`, `stove`, `refrigerator`, `off-street_parking`, `garage`, `dogs_allowed`, `cats_allowed`, `insulated`, `garbage_collection`, `central_air`, `storm_windows`, `furnished`, `washer`, `dryer`, `males_preferred`, `females_preferred`, `grad_students_preferred`, `med_students_preferred`, `dental_students_preferred`, `law_students_preferred`, `non-smoker_preferred`, `any`, `other_preferred`, `comments`, `rent_for`, `picture`, `contact_name`, `contact`, `email`, `primary_phone`, `secondary_phone`, `best_time_to_call`, `building_and_surrounding_area_illuminated_at_night`, `working_smoke_detectors`, `security_system`, `exterior_door_has_deadbolt`, `doors,_stairs,_fire_escapes_unobstructed_and_in_good_condition`, `carbon_monoxide_detectors`, `fire_extinguishers`, `electrical_wiring_up_to_code`, `bedrooms_not_located_in_basement_or_attic`, `Verified`, `Not_Verified`, `No_Certificate`, `N/A`, `preferred_gender`, `registered_with_rental_registry`) VALUES (9570, 'Active', 88, 'Minnesota', 'Buffalo', 0, 14214, 'University Heights', 'Lower', 2, 4, 4, '2019-08-01 00:00:00', '2019-02-14 00:00:00', 'Yes', 12, 300, 400, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 'Walk to south campus, extra large rooms, fully furnished, new flooring, all appliances and laundry included. Off street parking, water garbage and snow plowing included', 'per room', 0, 'Marie', 'Landlord seeking tenants', 'mak318@hotmail.com', '716-570-8867', NULL, '8am-8pm', 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, NULL, 0);
INSERT INTO `Listings` (`id`, `status`, `street_number`, `street`, `locality`, `Field1`, `postal_code`, `area`, `apartment_number`, `unit_type_id`, `total_bedrooms`, `available_bedrooms`, `date_available`, `date_listed`, `lease`, `term`, `rent`, `security_deposit`, `heat`, `gas`, `water`, `electric`, `internet`, `cable`, `stove`, `refrigerator`, `off-street_parking`, `garage`, `dogs_allowed`, `cats_allowed`, `insulated`, `garbage_collection`, `central_air`, `storm_windows`, `furnished`, `washer`, `dryer`, `males_preferred`, `females_preferred`, `grad_students_preferred`, `med_students_preferred`, `dental_students_preferred`, `law_students_preferred`, `non-smoker_preferred`, `any`, `other_preferred`, `comments`, `rent_for`, `picture`, `contact_name`, `contact`, `email`, `primary_phone`, `secondary_phone`, `best_time_to_call`, `building_and_surrounding_area_illuminated_at_night`, `working_smoke_detectors`, `security_system`, `exterior_door_has_deadbolt`, `doors,_stairs,_fire_escapes_unobstructed_and_in_good_condition`, `carbon_monoxide_detectors`, `fire_extinguishers`, `electrical_wiring_up_to_code`, `bedrooms_not_located_in_basement_or_attic`, `Verified`, `Not_Verified`, `No_Certificate`, `N/A`, `preferred_gender`, `registered_with_rental_registry`) VALUES (9571, 'Active', 235, 'Heath St', 'Buffalo', 0, 14214, 'University Heights', 'Lower', 2, 3, 3, '2019-06-01 00:00:00', '2019-02-18 00:00:00', 'Yes', 12, 325, 425, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 'Walk to south campus, fully furnished, all appliances and laundry included. Three bedrooms, living room, dining room kitchen and bath. Rent includes water, garbage and snow plowing. Upper unit is also available with another 3 bedrooms.', 'per room', 0, 'Marie', 'Landlord seeking tenants', 'mak318@hotmail.com', '716-570-8867', NULL, '8am-8pm', 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, NULL, 0);
# 6720 records

#
# Table structure for table 'Localities'
#

DROP TABLE IF EXISTS `Localities`;

CREATE TABLE `Localities` (
  `postal_code` INTEGER DEFAULT 0, 
  `name` VARCHAR(255), 
  INDEX (`postal_code`)
) ENGINE=innodb DEFAULT CHARSET=utf8;

SET autocommit=1;

#
# Dumping data for table 'Localities'
#

INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14001, 'Akron');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14031, 'Clarence');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14032, 'Clarence Center');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14043, 'Depew');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14051, 'East Amherst');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14052, 'East Aurora');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14057, 'Eden');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14068, 'Getzville');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14072, 'Grand Island');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14075, 'Hamburg');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14086, 'Lancaster');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14094, 'Lockport');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14095, 'Lockport');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14120, 'North Tonawanda');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14127, 'Orchard Park');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14150, 'Tonawanda');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14151, 'Tonawanda');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14201, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14202, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14203, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14204, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14205, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14206, 'Cheektowaga');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14207, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14208, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14209, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14210, 'West Seneca');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14211, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14212, 'Sloan');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14213, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14214, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14215, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14216, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14217, 'Kenmore');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14218, 'Lackawanna');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14219, 'Blasdell');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14220, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14221, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14222, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14223, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14224, 'West Seneca');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14225, 'Cheektowaga');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14226, 'Amherst');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14227, 'Cheektowaga');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14228, 'Amherst');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14231, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14233, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14240, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14241, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14260, 'Amherst');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14261, 'Amherst');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14263, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14280, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14301, 'Niagara Falls');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14302, 'Niagara Falls');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14303, 'Niagara Falls');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14304, 'Niagara Falls');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14305, 'Niagara Falls');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14025, 'Boston');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14051, 'Swormville');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14206, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14206, 'West Seneca');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14210, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14212, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14215, 'Cheektowaga');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14214, 'Snyder');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14217, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14217, 'Tonawanda');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14218, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14218, 'West Seneca');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14219, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14221, 'Amherst');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14221, 'Williamsville');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14223, 'Kenmore');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14223, 'Tonawanda');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14224, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14225, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14226, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14226, 'Eggertsville');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14226, 'Snyder');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14227, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14227, 'South Cheektowaga');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14228, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14231, 'Williamsville');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14260, 'Buffalo');
INSERT INTO `Localities` (`postal_code`, `name`) VALUES (14261, 'Buffalo');
# 86 records

#
# Table structure for table 'Streets'
#

DROP TABLE IF EXISTS `Streets`;

CREATE TABLE `Streets` (
  `id` INTEGER NOT NULL AUTO_INCREMENT, 
  `street` VARCHAR(50), 
  INDEX (`id`), 
  PRIMARY KEY (`id`)
) ENGINE=innodb DEFAULT CHARSET=utf8;

SET autocommit=1;

#
# Dumping data for table 'Streets'
#

INSERT INTO `Streets` (`id`, `street`) VALUES (1, 'Addison');
INSERT INTO `Streets` (`id`, `street`) VALUES (2, 'Albert dr.');
INSERT INTO `Streets` (`id`, `street`) VALUES (3, 'Allenhurst');
INSERT INTO `Streets` (`id`, `street`) VALUES (4, 'Amherst st.');
INSERT INTO `Streets` (`id`, `street`) VALUES (5, 'Ashland ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (6, 'Auburn ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (7, 'Bailey ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (8, 'Barton');
INSERT INTO `Streets` (`id`, `street`) VALUES (9, 'Bathhurst dr.');
INSERT INTO `Streets` (`id`, `street`) VALUES (10, 'Beach rd.');
INSERT INTO `Streets` (`id`, `street`) VALUES (11, 'Beach ridge rd.');
INSERT INTO `Streets` (`id`, `street`) VALUES (12, 'Beard ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (13, 'Beaumont dr.');
INSERT INTO `Streets` (`id`, `street`) VALUES (14, 'Beech meadow ct.');
INSERT INTO `Streets` (`id`, `street`) VALUES (15, 'Belmont ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (16, 'Blackmore');
INSERT INTO `Streets` (`id`, `street`) VALUES (17, 'Blvd towers');
INSERT INTO `Streets` (`id`, `street`) VALUES (18, 'Brantwood rd.');
INSERT INTO `Streets` (`id`, `street`) VALUES (19, 'Brittany');
INSERT INTO `Streets` (`id`, `street`) VALUES (20, 'Broad st.');
INSERT INTO `Streets` (`id`, `street`) VALUES (21, 'Brooklane');
INSERT INTO `Streets` (`id`, `street`) VALUES (22, 'Bruce');
INSERT INTO `Streets` (`id`, `street`) VALUES (23, 'Brush creek');
INSERT INTO `Streets` (`id`, `street`) VALUES (24, 'Bryant');
INSERT INTO `Streets` (`id`, `street`) VALUES (25, 'Burroughs dr.');
INSERT INTO `Streets` (`id`, `street`) VALUES (26, 'Busti ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (27, 'Callodine ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (28, 'Capen blvd.');
INSERT INTO `Streets` (`id`, `street`) VALUES (29, 'Carmel rd.');
INSERT INTO `Streets` (`id`, `street`) VALUES (30, 'Cascade dr.');
INSERT INTO `Streets` (`id`, `street`) VALUES (31, 'Chestnut ridge rd.');
INSERT INTO `Streets` (`id`, `street`) VALUES (32, 'Claremont');
INSERT INTO `Streets` (`id`, `street`) VALUES (33, 'Colvin ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (34, 'Comstock');
INSERT INTO `Streets` (`id`, `street`) VALUES (35, 'Concord dr.');
INSERT INTO `Streets` (`id`, `street`) VALUES (36, 'Cordova');
INSERT INTO `Streets` (`id`, `street`) VALUES (37, 'Cramer st.');
INSERT INTO `Streets` (`id`, `street`) VALUES (38, 'Crescent ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (39, 'Crestwood ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (40, 'Custer st.');
INSERT INTO `Streets` (`id`, `street`) VALUES (41, 'Dale dr.');
INSERT INTO `Streets` (`id`, `street`) VALUES (42, 'Dartmouth ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (43, 'Delaware ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (44, 'Draden ln.');
INSERT INTO `Streets` (`id`, `street`) VALUES (45, 'Durham dr.');
INSERT INTO `Streets` (`id`, `street`) VALUES (46, 'East northrup pl.');
INSERT INTO `Streets` (`id`, `street`) VALUES (47, 'Eggert rd.');
INSERT INTO `Streets` (`id`, `street`) VALUES (48, 'Eiss pl.');
INSERT INTO `Streets` (`id`, `street`) VALUES (49, 'Elmwood ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (50, 'Englewood ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (51, 'Flickinger ct.');
INSERT INTO `Streets` (`id`, `street`) VALUES (52, 'Flower st.');
INSERT INTO `Streets` (`id`, `street`) VALUES (53, 'Fordham dr.');
INSERT INTO `Streets` (`id`, `street`) VALUES (54, 'Foxberry dr.');
INSERT INTO `Streets` (`id`, `street`) VALUES (55, 'Franklin');
INSERT INTO `Streets` (`id`, `street`) VALUES (56, 'Freeman');
INSERT INTO `Streets` (`id`, `street`) VALUES (57, 'Getzville');
INSERT INTO `Streets` (`id`, `street`) VALUES (58, 'Ginger ct.');
INSERT INTO `Streets` (`id`, `street`) VALUES (59, 'Glenhaven');
INSERT INTO `Streets` (`id`, `street`) VALUES (60, 'Glenhurst');
INSERT INTO `Streets` (`id`, `street`) VALUES (61, 'Grand island blvd.');
INSERT INTO `Streets` (`id`, `street`) VALUES (62, 'Grandview');
INSERT INTO `Streets` (`id`, `street`) VALUES (63, 'Greenfield');
INSERT INTO `Streets` (`id`, `street`) VALUES (64, 'Greenfield st.');
INSERT INTO `Streets` (`id`, `street`) VALUES (65, 'Greenwich');
INSERT INTO `Streets` (`id`, `street`) VALUES (66, 'Hamilton');
INSERT INTO `Streets` (`id`, `street`) VALUES (67, 'Hartford');
INSERT INTO `Streets` (`id`, `street`) VALUES (68, 'Hawthorne');
INSERT INTO `Streets` (`id`, `street`) VALUES (69, 'Hawthrone ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (70, 'Heath st.');
INSERT INTO `Streets` (`id`, `street`) VALUES (71, 'Henel');
INSERT INTO `Streets` (`id`, `street`) VALUES (72, 'Hertel ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (73, 'Hewitt ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (74, 'Highgate ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (75, 'Hodge ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (76, 'Holmes ave');
INSERT INTO `Streets` (`id`, `street`) VALUES (77, 'Homecrest dr.');
INSERT INTO `Streets` (`id`, `street`) VALUES (78, 'Homer lane');
INSERT INTO `Streets` (`id`, `street`) VALUES (79, 'Homewood ct.');
INSERT INTO `Streets` (`id`, `street`) VALUES (80, 'Huntington');
INSERT INTO `Streets` (`id`, `street`) VALUES (81, 'Ivyhurst cir.');
INSERT INTO `Streets` (`id`, `street`) VALUES (82, 'Joe mccarthy dr.');
INSERT INTO `Streets` (`id`, `street`) VALUES (83, 'Kenmore ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (84, 'Kensington ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (85, 'Lafayette');
INSERT INTO `Streets` (`id`, `street`) VALUES (86, 'Lardner');
INSERT INTO `Streets` (`id`, `street`) VALUES (87, 'Lasalle ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (88, 'Lawnwood dr.');
INSERT INTO `Streets` (`id`, `street`) VALUES (89, 'Layton ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (91, 'Lisbon ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (92, 'Lockport area');
INSERT INTO `Streets` (`id`, `street`) VALUES (93, 'Longmeadow');
INSERT INTO `Streets` (`id`, `street`) VALUES (94, 'Lovering ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (95, 'Main st.');
INSERT INTO `Streets` (`id`, `street`) VALUES (96, 'Mallony');
INSERT INTO `Streets` (`id`, `street`) VALUES (97, 'Mallory');
INSERT INTO `Streets` (`id`, `street`) VALUES (98, 'Manor oak dr.');
INSERT INTO `Streets` (`id`, `street`) VALUES (99, 'Maple rd.');
INSERT INTO `Streets` (`id`, `street`) VALUES (100, 'Marine');
INSERT INTO `Streets` (`id`, `street`) VALUES (101, 'Mariner st.');
INSERT INTO `Streets` (`id`, `street`) VALUES (102, 'Mckinley pkwy.');
INSERT INTO `Streets` (`id`, `street`) VALUES (103, 'Meadow lea');
INSERT INTO `Streets` (`id`, `street`) VALUES (104, 'Merrimac');
INSERT INTO `Streets` (`id`, `street`) VALUES (105, 'Midland ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (106, 'Millersport hwy.');
INSERT INTO `Streets` (`id`, `street`) VALUES (107, 'Minnesota ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (108, 'Montrose');
INSERT INTO `Streets` (`id`, `street`) VALUES (109, 'Mullen');
INSERT INTO `Streets` (`id`, `street`) VALUES (110, 'New rd.');
INSERT INTO `Streets` (`id`, `street`) VALUES (111, 'Niagara falls blvd.');
INSERT INTO `Streets` (`id`, `street`) VALUES (112, 'Nicholson');
INSERT INTO `Streets` (`id`, `street`) VALUES (113, 'Noel');
INSERT INTO `Streets` (`id`, `street`) VALUES (114, 'North bailey ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (115, 'North forest rd.');
INSERT INTO `Streets` (`id`, `street`) VALUES (116, 'North long');
INSERT INTO `Streets` (`id`, `street`) VALUES (117, 'North maplemere');
INSERT INTO `Streets` (`id`, `street`) VALUES (118, 'North park');
INSERT INTO `Streets` (`id`, `street`) VALUES (119, 'North pearl');
INSERT INTO `Streets` (`id`, `street`) VALUES (120, 'North st.');
INSERT INTO `Streets` (`id`, `street`) VALUES (121, 'Northrup pl.');
INSERT INTO `Streets` (`id`, `street`) VALUES (122, 'Old lyme dr.');
INSERT INTO `Streets` (`id`, `street`) VALUES (123, 'other street');
INSERT INTO `Streets` (`id`, `street`) VALUES (124, 'Paige ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (125, 'Palmdale dr.');
INSERT INTO `Streets` (`id`, `street`) VALUES (126, 'Parkridge');
INSERT INTO `Streets` (`id`, `street`) VALUES (127, 'Patricia dr.');
INSERT INTO `Streets` (`id`, `street`) VALUES (128, 'Phylliss ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (129, 'Princeton ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (130, 'Raintree apt.');
INSERT INTO `Streets` (`id`, `street`) VALUES (131, 'Red fern ct.');
INSERT INTO `Streets` (`id`, `street`) VALUES (132, 'Richmond');
INSERT INTO `Streets` (`id`, `street`) VALUES (133, 'Riverdale');
INSERT INTO `Streets` (`id`, `street`) VALUES (134, 'Rounds ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (135, 'Rugby');
INSERT INTO `Streets` (`id`, `street`) VALUES (136, 'Saint florian');
INSERT INTO `Streets` (`id`, `street`) VALUES (137, 'Sanders rd.');
INSERT INTO `Streets` (`id`, `street`) VALUES (138, 'Saranac ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (139, 'Sedgemoor ct.');
INSERT INTO `Streets` (`id`, `street`) VALUES (140, 'Sheridan dr.');
INSERT INTO `Streets` (`id`, `street`) VALUES (141, 'Shirley ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (142, 'Shoshone');
INSERT INTO `Streets` (`id`, `street`) VALUES (143, 'Shoshone st.');
INSERT INTO `Streets` (`id`, `street`) VALUES (144, 'Smith rd.');
INSERT INTO `Streets` (`id`, `street`) VALUES (145, 'Springville ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (146, 'Starin ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (147, 'Stillwell');
INSERT INTO `Streets` (`id`, `street`) VALUES (148, 'SUMMER');
INSERT INTO `Streets` (`id`, `street`) VALUES (149, 'Sunmist sq.');
INSERT INTO `Streets` (`id`, `street`) VALUES (150, 'Sunshine dr.');
INSERT INTO `Streets` (`id`, `street`) VALUES (151, 'Tioga st.');
INSERT INTO `Streets` (`id`, `street`) VALUES (152, 'Traymore st.');
INSERT INTO `Streets` (`id`, `street`) VALUES (153, 'Tyler st.');
INSERT INTO `Streets` (`id`, `street`) VALUES (154, 'University ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (155, 'Villa');
INSERT INTO `Streets` (`id`, `street`) VALUES (156, 'Walton ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (157, 'Washington');
INSERT INTO `Streets` (`id`, `street`) VALUES (158, 'West ferry');
INSERT INTO `Streets` (`id`, `street`) VALUES (159, 'West ferry st');
INSERT INTO `Streets` (`id`, `street`) VALUES (160, 'West northrup');
INSERT INTO `Streets` (`id`, `street`) VALUES (161, 'West northrup');
INSERT INTO `Streets` (`id`, `street`) VALUES (162, 'West summerset');
INSERT INTO `Streets` (`id`, `street`) VALUES (163, 'West utica');
INSERT INTO `Streets` (`id`, `street`) VALUES (164, 'West winspear ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (165, 'Westminster ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (166, 'Willow dr.');
INSERT INTO `Streets` (`id`, `street`) VALUES (167, 'Willow ridge dr.');
INSERT INTO `Streets` (`id`, `street`) VALUES (168, 'Windermere');
INSERT INTO `Streets` (`id`, `street`) VALUES (169, 'Winspear ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (170, 'Winston');
INSERT INTO `Streets` (`id`, `street`) VALUES (171, 'Woodcrest dr.');
INSERT INTO `Streets` (`id`, `street`) VALUES (172, 'Woodette  pl.');
INSERT INTO `Streets` (`id`, `street`) VALUES (173, 'Woodpointe run');
INSERT INTO `Streets` (`id`, `street`) VALUES (174, 'Woodward ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (177, 'HOWARD AVE');
INSERT INTO `Streets` (`id`, `street`) VALUES (178, 'AMHERST MANOR Dr.');
INSERT INTO `Streets` (`id`, `street`) VALUES (179, 'FAIRFIELD AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (180, 'WELLINGTON');
INSERT INTO `Streets` (`id`, `street`) VALUES (181, 'SUN DOWN TRAIL');
INSERT INTO `Streets` (`id`, `street`) VALUES (182, 'LEXINGTON AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (183, 'CLEVELAND');
INSERT INTO `Streets` (`id`, `street`) VALUES (184, 'KLAUDER');
INSERT INTO `Streets` (`id`, `street`) VALUES (185, 'FALCONER');
INSERT INTO `Streets` (`id`, `street`) VALUES (186, 'SCHOOL ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (187, 'GRANGER PLACE');
INSERT INTO `Streets` (`id`, `street`) VALUES (188, 'LINWOOD');
INSERT INTO `Streets` (`id`, `street`) VALUES (189, 'CHERRYWOOD');
INSERT INTO `Streets` (`id`, `street`) VALUES (190, 'MARIAN DRIVE');
INSERT INTO `Streets` (`id`, `street`) VALUES (191, 'BEECH RD');
INSERT INTO `Streets` (`id`, `street`) VALUES (192, 'FRANKHAUSER');
INSERT INTO `Streets` (`id`, `street`) VALUES (193, 'FRANKHAUSER RD');
INSERT INTO `Streets` (`id`, `street`) VALUES (194, 'NORTH FRENCH RD');
INSERT INTO `Streets` (`id`, `street`) VALUES (195, 'SOUTH CENTURY AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (196, 'BRANTFORD PLACE');
INSERT INTO `Streets` (`id`, `street`) VALUES (197, 'NORWALK AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (198, 'OAKBROOK DR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (199, 'COLONIAL DR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (200, 'ST. MARGARETS CT.');
INSERT INTO `Streets` (`id`, `street`) VALUES (201, 'WEST DELAVAN');
INSERT INTO `Streets` (`id`, `street`) VALUES (202, 'HALLER AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (203, 'DYSINGER RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (204, 'MAPLEVIEW RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (205, 'GOUNDRY STREET');
INSERT INTO `Streets` (`id`, `street`) VALUES (206, 'RIDGE ROAD');
INSERT INTO `Streets` (`id`, `street`) VALUES (207, 'PAYNE AVE,');
INSERT INTO `Streets` (`id`, `street`) VALUES (208, 'PAYNE AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (209, 'NORWOOD AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (210, 'WHITNEY PL');
INSERT INTO `Streets` (`id`, `street`) VALUES (211, 'SPRINGMEADOW DRIVE');
INSERT INTO `Streets` (`id`, `street`) VALUES (212, 'ARIELLECOURT');
INSERT INTO `Streets` (`id`, `street`) VALUES (213, 'ST. JAMES PLACE');
INSERT INTO `Streets` (`id`, `street`) VALUES (214, 'TUXEDO');
INSERT INTO `Streets` (`id`, `street`) VALUES (215, 'HINDS');
INSERT INTO `Streets` (`id`, `street`) VALUES (216, 'SEGSBURY RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (217, 'COLONIAL CIR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (218, 'SWEENY');
INSERT INTO `Streets` (`id`, `street`) VALUES (219, 'KENOVA');
INSERT INTO `Streets` (`id`, `street`) VALUES (220, 'PARKSIDE CT.');
INSERT INTO `Streets` (`id`, `street`) VALUES (221, 'PARK STREET');
INSERT INTO `Streets` (`id`, `street`) VALUES (222, 'LAKEWOOD');
INSERT INTO `Streets` (`id`, `street`) VALUES (223, 'LAKEWOOD PKWY');
INSERT INTO `Streets` (`id`, `street`) VALUES (224, 'EDWARD ST');
INSERT INTO `Streets` (`id`, `street`) VALUES (225, 'CHARELSGATE');
INSERT INTO `Streets` (`id`, `street`) VALUES (226, 'RENSCH RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (227, 'KELVIN DR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (228, 'GLEN OAK DRIVE');
INSERT INTO `Streets` (`id`, `street`) VALUES (229, 'NEWELL AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (230, 'TAUNTON PL.');
INSERT INTO `Streets` (`id`, `street`) VALUES (231, 'WALNUT ST');
INSERT INTO `Streets` (`id`, `street`) VALUES (232, 'SUNRIDGE DR');
INSERT INTO `Streets` (`id`, `street`) VALUES (233, 'SUNDRIDGE DR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (234, 'MARION DRIVE');
INSERT INTO `Streets` (`id`, `street`) VALUES (235, 'EVANS ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (236, 'GROTON DR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (237, 'HICKORY HILL RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (238, 'JEWETT PARKWAY');
INSERT INTO `Streets` (`id`, `street`) VALUES (239, 'CUNARD ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (240, 'CUNARD RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (241, 'VIA PINTO DRIVE');
INSERT INTO `Streets` (`id`, `street`) VALUES (242, 'PHEASENT RUN');
INSERT INTO `Streets` (`id`, `street`) VALUES (243, 'MEYER');
INSERT INTO `Streets` (`id`, `street`) VALUES (244, 'ROBINHILL DR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (245, 'AREND AVENUE');
INSERT INTO `Streets` (`id`, `street`) VALUES (246, 'ALCONA');
INSERT INTO `Streets` (`id`, `street`) VALUES (247, 'PARKSIDE AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (248, 'MAPLE COURT');
INSERT INTO `Streets` (`id`, `street`) VALUES (249, 'HEMLOCK RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (250, 'ALCONA AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (251, 'KAYMAR DR');
INSERT INTO `Streets` (`id`, `street`) VALUES (252, 'GENESEE ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (253, 'BROOKFIELD LANE');
INSERT INTO `Streets` (`id`, `street`) VALUES (254, 'VILLA AVENUE');
INSERT INTO `Streets` (`id`, `street`) VALUES (255, 'STERLING RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (256, 'KINGS HIGHWAY');
INSERT INTO `Streets` (`id`, `street`) VALUES (257, 'COMMONWEALTH AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (258, 'CREEKSIDE VILLAGE');
INSERT INTO `Streets` (`id`, `street`) VALUES (259, 'KNOX');
INSERT INTO `Streets` (`id`, `street`) VALUES (260, 'MILITARY RD');
INSERT INTO `Streets` (`id`, `street`) VALUES (261, 'GOUNDRY');
INSERT INTO `Streets` (`id`, `street`) VALUES (262, 'HOLMES ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (263, 'CHRISTIANA');
INSERT INTO `Streets` (`id`, `street`) VALUES (264, 'LAMARCK');
INSERT INTO `Streets` (`id`, `street`) VALUES (265, 'BLUEBIRD LANE');
INSERT INTO `Streets` (`id`, `street`) VALUES (266, 'WM. PRICE PKWY');
INSERT INTO `Streets` (`id`, `street`) VALUES (267, 'LANCASTER AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (268, 'W. WINSPEAR');
INSERT INTO `Streets` (`id`, `street`) VALUES (269, 'ATWOOD');
INSERT INTO `Streets` (`id`, `street`) VALUES (270, 'SOUTHWIND TR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (271, 'E End Ave.');
INSERT INTO `Streets` (`id`, `street`) VALUES (272, 'ROYCROFT');
INSERT INTO `Streets` (`id`, `street`) VALUES (273, 'LEBRUN RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (274, 'MEADOWBROOK');
INSERT INTO `Streets` (`id`, `street`) VALUES (275, 'POTOMAC AVE');
INSERT INTO `Streets` (`id`, `street`) VALUES (276, 'ST. FLORIAN');
INSERT INTO `Streets` (`id`, `street`) VALUES (277, 'WEST OAKFIELD ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (278, 'KENNEDY');
INSERT INTO `Streets` (`id`, `street`) VALUES (279, 'OAKRIDGE AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (280, 'ALBERTA DR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (281, 'PROSPECT AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (282, 'VANDERVOORT ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (283, 'KENVILLE RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (284, 'MEAD ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (285, 'CARMEN AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (286, 'SWEET HOME RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (287, 'RADCLIFFE RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (288, 'BARON CT.');
INSERT INTO `Streets` (`id`, `street`) VALUES (289, 'BERKSHIRE AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (290, 'HOLLY LN.');
INSERT INTO `Streets` (`id`, `street`) VALUES (291, 'TACOMA');
INSERT INTO `Streets` (`id`, `street`) VALUES (292, 'CAMDEN AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (293, 'GLENDALE RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (294, 'BEECH RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (295, 'WESTFIELD RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (296, 'RAMSDELL AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (297, 'HAYMARKET SQUARE');
INSERT INTO `Streets` (`id`, `street`) VALUES (298, 'CHARTER OAKS');
INSERT INTO `Streets` (`id`, `street`) VALUES (299, 'REDMOND ST');
INSERT INTO `Streets` (`id`, `street`) VALUES (300, 'WALTON DR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (301, 'HARTWELL RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (302, 'FOREST');
INSERT INTO `Streets` (`id`, `street`) VALUES (303, 'EAST ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (304, 'HARROGATE SQ.');
INSERT INTO `Streets` (`id`, `street`) VALUES (305, 'NORTH ELLICOTT CREEK RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (306, 'SOUTH ELLICOTT CREEK RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (307, 'LEBRUN');
INSERT INTO `Streets` (`id`, `street`) VALUES (308, 'TULANE RD');
INSERT INTO `Streets` (`id`, `street`) VALUES (309, 'AGYLE');
INSERT INTO `Streets` (`id`, `street`) VALUES (310, 'PARADISE LN.');
INSERT INTO `Streets` (`id`, `street`) VALUES (311, 'STOCKBRIDGE AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (312, 'RANCH TRAIL WEST');
INSERT INTO `Streets` (`id`, `street`) VALUES (313, 'HARLEM RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (314, 'OLD ORCHARD');
INSERT INTO `Streets` (`id`, `street`) VALUES (315, 'MARINE DR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (316, 'SUGNET');
INSERT INTO `Streets` (`id`, `street`) VALUES (317, 'EAGLE ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (318, 'WEST CLEVELAND DR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (319, 'CAYUGA');
INSERT INTO `Streets` (`id`, `street`) VALUES (320, 'HENDRICKS BLVD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (321, 'VIRGINIA ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (322, 'MARIEMONT');
INSERT INTO `Streets` (`id`, `street`) VALUES (323, 'NIAGRA FALLS BLVD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (324, 'JEFFREY ST');
INSERT INTO `Streets` (`id`, `street`) VALUES (325, 'WEHRLE ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (326, 'TRAVERSE ST');
INSERT INTO `Streets` (`id`, `street`) VALUES (327, 'CHASEWOOD LANE');
INSERT INTO `Streets` (`id`, `street`) VALUES (328, 'CALIFORNIA RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (329, 'ROWLEY RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (330, 'CAMPUS DR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (331, 'BEEHUNTER CT.');
INSERT INTO `Streets` (`id`, `street`) VALUES (332, 'EXCHANGE ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (333, 'LOCUST ST');
INSERT INTO `Streets` (`id`, `street`) VALUES (334, 'SOUTH ELLICOT ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (335, 'BRINTON ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (336, 'HEDGE CT.');
INSERT INTO `Streets` (`id`, `street`) VALUES (337, 'SPRUCE');
INSERT INTO `Streets` (`id`, `street`) VALUES (338, 'BRIDLEWOOD DR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (339, 'PINETREE COURT');
INSERT INTO `Streets` (`id`, `street`) VALUES (340, 'FAIRELN LANE');
INSERT INTO `Streets` (`id`, `street`) VALUES (341, 'FAIRELM');
INSERT INTO `Streets` (`id`, `street`) VALUES (342, 'FAIRELM LANE');
INSERT INTO `Streets` (`id`, `street`) VALUES (343, 'BIDWELL PARKWAY');
INSERT INTO `Streets` (`id`, `street`) VALUES (344, 'CONNECTION DRIVE');
INSERT INTO `Streets` (`id`, `street`) VALUES (345, 'GORDON');
INSERT INTO `Streets` (`id`, `street`) VALUES (346, 'STRATFORD');
INSERT INTO `Streets` (`id`, `street`) VALUES (347, 'CAMBRIDGE');
INSERT INTO `Streets` (`id`, `street`) VALUES (348, 'GARNET ROAD');
INSERT INTO `Streets` (`id`, `street`) VALUES (349, 'BODINE RD');
INSERT INTO `Streets` (`id`, `street`) VALUES (350, 'RINEWALT ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (351, 'CORNELL AVE');
INSERT INTO `Streets` (`id`, `street`) VALUES (352, 'W. TOULON DR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (353, 'BRIGHTON RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (354, 'CROSBY AVE');
INSERT INTO `Streets` (`id`, `street`) VALUES (355, 'GEORGE URBAN BLVD');
INSERT INTO `Streets` (`id`, `street`) VALUES (356, 'COVENTRY GREEN CIRCLE');
INSERT INTO `Streets` (`id`, `street`) VALUES (357, 'LEONORE RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (358, 'RAND');
INSERT INTO `Streets` (`id`, `street`) VALUES (359, 'RAND AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (360, 'DENROSE');
INSERT INTO `Streets` (`id`, `street`) VALUES (361, 'CRESTMOUNT');
INSERT INTO `Streets` (`id`, `street`) VALUES (362, 'PARKDALE AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (363, 'ANDOVER');
INSERT INTO `Streets` (`id`, `street`) VALUES (364, 'FLORIDA ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (365, 'THORNCLIFFE');
INSERT INTO `Streets` (`id`, `street`) VALUES (366, 'NORTH ELLICOT');
INSERT INTO `Streets` (`id`, `street`) VALUES (367, 'SOUTH SEINE');
INSERT INTO `Streets` (`id`, `street`) VALUES (368, 'ESTHER');
INSERT INTO `Streets` (`id`, `street`) VALUES (369, 'BERKLEY');
INSERT INTO `Streets` (`id`, `street`) VALUES (370, 'GARDEN CT.');
INSERT INTO `Streets` (`id`, `street`) VALUES (371, 'FLORENCE AVE');
INSERT INTO `Streets` (`id`, `street`) VALUES (372, 'KOSTER ROW');
INSERT INTO `Streets` (`id`, `street`) VALUES (373, 'DEVEREAUX');
INSERT INTO `Streets` (`id`, `street`) VALUES (374, 'LEMANS DR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (375, 'LINDEN AVE');
INSERT INTO `Streets` (`id`, `street`) VALUES (376, 'MEYER RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (377, 'THE COURTYARDS');
INSERT INTO `Streets` (`id`, `street`) VALUES (378, 'VIRGIL AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (379, 'BEREHAVEN');
INSERT INTO `Streets` (`id`, `street`) VALUES (380, 'CHAPEL AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (381, 'BELVOIR RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (382, 'SUFFOLK');
INSERT INTO `Streets` (`id`, `street`) VALUES (383, 'REIMAN ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (384, 'ELMHURST PL.');
INSERT INTO `Streets` (`id`, `street`) VALUES (385, 'WHEATON AVE');
INSERT INTO `Streets` (`id`, `street`) VALUES (386, 'KNOWLTON');
INSERT INTO `Streets` (`id`, `street`) VALUES (387, 'KNOWLTON AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (388, 'WHEATON DR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (389, 'EAST AMHERST');
INSERT INTO `Streets` (`id`, `street`) VALUES (390, 'HALWILL');
INSERT INTO `Streets` (`id`, `street`) VALUES (391, 'PEPPERTREE DR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (392, 'VINE LA.');
INSERT INTO `Streets` (`id`, `street`) VALUES (393, 'SCAM RIDGE CURVE');
INSERT INTO `Streets` (`id`, `street`) VALUES (394, 'BLAKE ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (395, 'PARADISE RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (396, 'GLEN AVE');
INSERT INTO `Streets` (`id`, `street`) VALUES (397, 'FLETCHER ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (398, 'GREEN ACRES RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (399, '17TH ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (400, 'SOUTHWEDGE');
INSERT INTO `Streets` (`id`, `street`) VALUES (401, 'N. MAPLEMERE');
INSERT INTO `Streets` (`id`, `street`) VALUES (402, 'GARDENWOOD LN.');
INSERT INTO `Streets` (`id`, `street`) VALUES (403, 'HENLEY RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (404, 'CREEKSIDE DR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (405, 'GARRISON RD');
INSERT INTO `Streets` (`id`, `street`) VALUES (406, 'MANCHESTER PLACE');
INSERT INTO `Streets` (`id`, `street`) VALUES (407, 'STERLING AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (408, 'PRARIE AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (409, 'EMERSON DR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (410, 'PENNSYLVANIA AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (411, 'GRANDVIEW AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (412, 'EAST DEPEW AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (413, 'HAWTHORNE AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (414, 'ROCKDALE DRIVE');
INSERT INTO `Streets` (`id`, `street`) VALUES (415, 'DELAMERE RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (416, 'LOS ROBLES AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (417, 'CLEARFIELD DR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (418, 'PRYOR AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (419, 'MELROSE RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (420, 'ZELMER ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (421, 'MT. VERNON');
INSERT INTO `Streets` (`id`, `street`) VALUES (422, 'MOUNT VERNON');
INSERT INTO `Streets` (`id`, `street`) VALUES (423, 'MT. VENON');
INSERT INTO `Streets` (`id`, `street`) VALUES (424, 'BERRYMAN RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (425, 'LINVIEW TERRACE');
INSERT INTO `Streets` (`id`, `street`) VALUES (426, 'BYRON AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (427, 'NORTHWOOD DR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (428, 'BIRD AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (429, 'HUNTING VALLEY');
INSERT INTO `Streets` (`id`, `street`) VALUES (430, 'DEWEY AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (431, 'LYNDALE AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (432, 'NORTHUMBERLAND');
INSERT INTO `Streets` (`id`, `street`) VALUES (433, 'NADON PL.');
INSERT INTO `Streets` (`id`, `street`) VALUES (434, 'HOPKIN RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (435, 'WEST OAKFIELD RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (436, 'AMERICAN CAMPUS DRIVE');
INSERT INTO `Streets` (`id`, `street`) VALUES (437, 'EAST SUMMERSET');
INSERT INTO `Streets` (`id`, `street`) VALUES (438, 'SWEETHOME ROAD');
INSERT INTO `Streets` (`id`, `street`) VALUES (439, 'NORTH DAVIS RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (440, 'HIGHLAND AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (441, 'BOSTON CROSS RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (442, 'ROBIN ROAD');
INSERT INTO `Streets` (`id`, `street`) VALUES (443, 'GROVER CLEVELAND HIGHWAY');
INSERT INTO `Streets` (`id`, `street`) VALUES (444, 'LEMANS');
INSERT INTO `Streets` (`id`, `street`) VALUES (445, 'CHARLESGATE CIRCLE');
INSERT INTO `Streets` (`id`, `street`) VALUES (446, 'LEMONTREE COURT');
INSERT INTO `Streets` (`id`, `street`) VALUES (447, 'ASHBURN AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (448, 'HUMBOLDT PARKWAY');
INSERT INTO `Streets` (`id`, `street`) VALUES (449, 'CHERRYWOOD DR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (450, 'E. LOVEJOY ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (451, 'LONGNECKER ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (452, 'BOBBEN');
INSERT INTO `Streets` (`id`, `street`) VALUES (453, 'WILEY ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (454, 'BERNHADT DR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (455, 'BERNHARDT DR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (456, 'CAMPBELL BLVD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (457, 'ALLEN ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (458, 'KAY ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (459, 'BLUEBIRD LN.');
INSERT INTO `Streets` (`id`, `street`) VALUES (460, 'BEAR RIDGE RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (461, 'TRANSIT RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (462, 'TRALEE TERRACE');
INSERT INTO `Streets` (`id`, `street`) VALUES (463, 'DOCKSIDE DR');
INSERT INTO `Streets` (`id`, `street`) VALUES (464, 'GEORGE');
INSERT INTO `Streets` (`id`, `street`) VALUES (465, 'KEPH RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (466, 'KEPH DRIVE');
INSERT INTO `Streets` (`id`, `street`) VALUES (467, 'PINE RIDGE RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (468, 'REIST ST');
INSERT INTO `Streets` (`id`, `street`) VALUES (469, 'GROVELAND');
INSERT INTO `Streets` (`id`, `street`) VALUES (470, 'WILLET');
INSERT INTO `Streets` (`id`, `street`) VALUES (471, 'NOTTINGHAM');
INSERT INTO `Streets` (`id`, `street`) VALUES (472, 'HEIM RD');
INSERT INTO `Streets` (`id`, `street`) VALUES (473, 'STEVENSON BLVD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (474, 'WILLETT ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (475, 'SEAN RILEY COURT');
INSERT INTO `Streets` (`id`, `street`) VALUES (476, 'GASPE DRIVE');
INSERT INTO `Streets` (`id`, `street`) VALUES (477, 'HINMAN AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (478, 'GROVE');
INSERT INTO `Streets` (`id`, `street`) VALUES (479, 'ROBERT DR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (480, 'PIERPONT AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (481, 'LAMONT ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (482, 'NIAGARA ST');
INSERT INTO `Streets` (`id`, `street`) VALUES (483, 'VERNON PLACE');
INSERT INTO `Streets` (`id`, `street`) VALUES (484, 'MORGAN ST');
INSERT INTO `Streets` (`id`, `street`) VALUES (485, 'SPRUCE ROAD');
INSERT INTO `Streets` (`id`, `street`) VALUES (486, 'ELLEN DRIVE');
INSERT INTO `Streets` (`id`, `street`) VALUES (487, 'PIN OAK DRIVE');
INSERT INTO `Streets` (`id`, `street`) VALUES (488, 'HEDLEY ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (489, 'ST. AMELIA DR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (490, 'PARKWOOD AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (491, 'MILL STREET');
INSERT INTO `Streets` (`id`, `street`) VALUES (492, 'SKINNERVILLE RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (493, 'ORCHARD PL.');
INSERT INTO `Streets` (`id`, `street`) VALUES (494, 'RUMSON ROAD');
INSERT INTO `Streets` (`id`, `street`) VALUES (495, 'CLINTON ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (496, 'ASTOR RIDGE DRIVE');
INSERT INTO `Streets` (`id`, `street`) VALUES (497, 'VIRGINIA PLACE');
INSERT INTO `Streets` (`id`, `street`) VALUES (498, 'RANDWOOD CT.');
INSERT INTO `Streets` (`id`, `street`) VALUES (499, 'UNION ROAD');
INSERT INTO `Streets` (`id`, `street`) VALUES (500, 'DODGE ROAD');
INSERT INTO `Streets` (`id`, `street`) VALUES (501, 'ARLINGTON ROAD');
INSERT INTO `Streets` (`id`, `street`) VALUES (502, 'SUNDOWN TRAIL');
INSERT INTO `Streets` (`id`, `street`) VALUES (503, 'TONAWANDA CREEK ROAD');
INSERT INTO `Streets` (`id`, `street`) VALUES (504, 'ORCHARD DRIVE');
INSERT INTO `Streets` (`id`, `street`) VALUES (505, 'CANTERBURY CT.');
INSERT INTO `Streets` (`id`, `street`) VALUES (506, 'LOS ROBLES');
INSERT INTO `Streets` (`id`, `street`) VALUES (507, 'NORTH DR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (508, 'LARCH ROAD');
INSERT INTO `Streets` (`id`, `street`) VALUES (509, 'HENNEPIN ROAD');
INSERT INTO `Streets` (`id`, `street`) VALUES (510, 'KENVIEW BOULEVARD');
INSERT INTO `Streets` (`id`, `street`) VALUES (511, 'HIGHLAND DR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (512, 'DEWITT');
INSERT INTO `Streets` (`id`, `street`) VALUES (513, 'ATLANTIC AVE.');
INSERT INTO `Streets` (`id`, `street`) VALUES (514, 'HENNIPIN RD.');
INSERT INTO `Streets` (`id`, `street`) VALUES (515, 'NORTH ELLICOTT');
INSERT INTO `Streets` (`id`, `street`) VALUES (516, 'COUNTRYSIDE LN.');
INSERT INTO `Streets` (`id`, `street`) VALUES (517, 'ELM ROAD');
INSERT INTO `Streets` (`id`, `street`) VALUES (518, 'COURTLAND');
INSERT INTO `Streets` (`id`, `street`) VALUES (519, 'ZIMMERMAN ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (520, 'SENECA STREET');
INSERT INTO `Streets` (`id`, `street`) VALUES (521, 'SENECA ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (522, 'PONDEROSA DRIVE');
INSERT INTO `Streets` (`id`, `street`) VALUES (523, 'MILLRACE NORTH');
INSERT INTO `Streets` (`id`, `street`) VALUES (524, 'HIRSCHFIELD DR.');
INSERT INTO `Streets` (`id`, `street`) VALUES (525, 'TRISTAN LANE');
INSERT INTO `Streets` (`id`, `street`) VALUES (526, 'GERMAIN ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (527, 'OLIVER ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (528, 'DOCKSIDE PARKWAY');
INSERT INTO `Streets` (`id`, `street`) VALUES (529, 'DOCKSIDE PKWY');
INSERT INTO `Streets` (`id`, `street`) VALUES (530, 'BELCOURT');
INSERT INTO `Streets` (`id`, `street`) VALUES (531, 'OAK COURT');
INSERT INTO `Streets` (`id`, `street`) VALUES (532, 'BRUCE ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (533, 'ABBOTTS FORD PL.');
INSERT INTO `Streets` (`id`, `street`) VALUES (534, 'RESERVATION ST.');
INSERT INTO `Streets` (`id`, `street`) VALUES (535, 'VOORHEES');
INSERT INTO `Streets` (`id`, `street`) VALUES (536, 'CAMPUS DRIVE NORTH');
INSERT INTO `Streets` (`id`, `street`) VALUES (537, 'EAST DEPEW AVE');
INSERT INTO `Streets` (`id`, `street`) VALUES (538, 'HENRIETTA ST.');
# 535 records

#
# Table structure for table 'Streets_New'
#

DROP TABLE IF EXISTS `Streets_New`;

CREATE TABLE `Streets_New` (
  `id` INTEGER NOT NULL AUTO_INCREMENT, 
  `street` VARCHAR(50), 
  `suffix` INTEGER, 
  INDEX (`id`), 
  PRIMARY KEY (`id`)
) ENGINE=innodb DEFAULT CHARSET=utf8;

SET autocommit=1;

#
# Dumping data for table 'Streets_New'
#

# 0 records

#
# Table structure for table 'StreetSuffixes'
#

DROP TABLE IF EXISTS `StreetSuffixes`;

CREATE TABLE `StreetSuffixes` (
  `id` INTEGER NOT NULL AUTO_INCREMENT, 
  `suffix` VARCHAR(255), 
  `abbreviation` VARCHAR(255), 
  PRIMARY KEY (`id`)
) ENGINE=innodb DEFAULT CHARSET=utf8;

SET autocommit=1;

#
# Dumping data for table 'StreetSuffixes'
#

# 0 records

#
# Table structure for table 'UnitTypes'
#

DROP TABLE IF EXISTS `UnitTypes`;

CREATE TABLE `UnitTypes` (
  `id` INTEGER NOT NULL AUTO_INCREMENT, 
  `unit_type` VARCHAR(50), 
  INDEX (`id`), 
  PRIMARY KEY (`id`)
) ENGINE=innodb DEFAULT CHARSET=utf8;

SET autocommit=1;

#
# Dumping data for table 'UnitTypes'
#

INSERT INTO `UnitTypes` (`id`, `unit_type`) VALUES (1, 'Apartment');
INSERT INTO `UnitTypes` (`id`, `unit_type`) VALUES (2, 'Lower flat');
INSERT INTO `UnitTypes` (`id`, `unit_type`) VALUES (3, 'Upper flat');
INSERT INTO `UnitTypes` (`id`, `unit_type`) VALUES (4, 'Room in private house');
INSERT INTO `UnitTypes` (`id`, `unit_type`) VALUES (5, 'Whole house');
INSERT INTO `UnitTypes` (`id`, `unit_type`) VALUES (6, 'Studio');
INSERT INTO `UnitTypes` (`id`, `unit_type`) VALUES (7, 'Duplex');
INSERT INTO `UnitTypes` (`id`, `unit_type`) VALUES (8, 'Sublet');
# 8 records

