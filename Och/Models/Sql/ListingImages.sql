CREATE TABLE IF NOT EXISTS `listing_images`
(
  `image_id`          INT          NOT NULL AUTO_INCREMENT,
  `listing_id`        INT          NOT NULL,
  `image_url`         VARCHAR(200) NOT NULL,
  `image_description` VARCHAR(300) NULL,
  PRIMARY KEY (`image_id`),
  INDEX (`listing_id`)
);
