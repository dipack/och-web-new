using System.Collections.Generic;

namespace Och.Models.Db
{
    public class Springboard
    {
        public List<UnitType> UnitTypes { get; set; }
        public List<Locality> Localities { get; set; }
    }
}