using System.Data;
using Newtonsoft.Json;

namespace Och.Models.Db
{
    public class ApartmentComplex
    {
        public int? ApartmentId { get; set; }
        public string ApartmentName { get; set; }
        public int? ListingId { get; set; }
        public string ApartmentAddress { get; set; }
        public int? ContactDetailsId { get; set; }
        public string ApartmentDescription { get; set; }
        public string ApartmentUrl { get; set; }
        public int? DisplayOrder { get; set; }
        public bool? ListingActive { get; set; }

        public static ApartmentComplex PopulateFromRecord(IDataRecord reader)
        {
            return new ApartmentComplex
            {
                ApartmentId = reader.GetInt32(0),
                ApartmentName = reader.GetString(1),
                ListingId = reader.GetInt32(2),
                ApartmentAddress = reader.GetString(3),
                ContactDetailsId = reader.GetInt32(4),
                ApartmentDescription = reader.GetString(5),
                ApartmentUrl = reader.GetString(6),
                DisplayOrder = reader.GetInt32(7),
                ListingActive = reader.GetInt32(8).Equals(1)
            };
        }
    }
}