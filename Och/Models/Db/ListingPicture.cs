using System.Data;

namespace Och.Models.Db
{
    public class ListingPicture
    {
        public int ImageId { get; set; }
        public int ListingId { get; set; }
        public string ImageUrl { get; set; }
        public string ImageDescription { get; set; }

        public static ListingPicture PopulateFromRecord(IDataRecord reader)
        {
            return new ListingPicture
            {
                ImageId = reader.GetInt32(0),
                ListingId = reader.GetInt32(1),
                ImageUrl = reader.GetString(2),
                ImageDescription = reader.GetString(3)
            };
        }
    }
}