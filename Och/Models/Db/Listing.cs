using System;
using System.Data;

namespace Och.Models.Db
{
    public class Listing
    {
        public int? ListingId { get; set; } = -1;
        public bool? ListingActive { get; set; } = false;
        public int? StreetNumber { get; set; } = -1;
        public string StreetName { get; set; } = "";
        public string Locality { get; set; } = "";
        public int? PostalCode { get; set; } = -1;
        public int? ApartmentNumber { get; set; } = -1;
        public int? UnitType { get; set; } = -1;
        public int? BedroomsTotal { get; set; } = -1;
        public int? BedroomsAvailable { get; set; } = -1;
        public DateTime DateAvailable { get; set; } = DateTime.Now;
        public DateTime DateListed { get; set; } = DateTime.Now;
        public DateTime DateExpiry { get; set; } = DateTime.Now;
        public int? LeaseLengthMonths { get; set; } = -1;
        public float? AmtRent { get; set; } = (float?) 0.0;
        public float? AmtSecurity { get; set; } = (float?) 0.0;
        public bool? AmenityHeating { get; set; } = false;
        public bool? AmenityGas { get; set; } = false;
        public bool? AmenityWater { get; set; } = false;
        public bool? AmenityElectricity { get; set; } = false;
        public bool? AmenityInternet { get; set; } = false;
        public bool? AmenityCable { get; set; } = false;
        public bool? AmenityStove { get; set; } = false;
        public bool? AmenityRefrigerator { get; set; } = false;
        public bool? AmenityOffStreetParking { get; set; } = false;
        public bool? AmenityGarage { get; set; } = false;
        public bool? AmenityInsulated { get; set; } = false;
        public bool? AmenityGarbageCollection { get; set; } = false;
        public bool? AmenityCentralAc { get; set; } = false;
        public bool? AmenityStormWindows { get; set; } = false;
        public bool? AmenityFurnished { get; set; } = false;
        public bool? AmenityWasher { get; set; } = false;
        public bool? AmenityDryer { get; set; } = false;
        public bool? AmenitySmokeDetectors { get; set; } = false;
        public bool? AmenitySecuritySystem { get; set; } = false;
        public bool? AmenityExteriorDoorDeadbolt { get; set; } = false;
        public bool? AmenityFireSafetyCode { get; set; } = false;
        public bool? AmenityCoDetectors { get; set; } = false;
        public bool? AmenityElectricalSafetyCode { get; set; } = false;
        public bool? RulesPetsAllowed { get; set; } = false;
        public bool? BedroomsLocatedBasementAttic { get; set; } = false;
        public bool? VerifiedProperty { get; set; } = false;
        public bool? CertificateOfOccupancy { get; set; } = false;
        public bool? RegisteredRentalRegistry { get; set; } = false;
        public int? ContactDetailsId { get; set; } = -1;
        public string Comments { get; set; } = "";


        public static Listing PopulateFromRecord(IDataRecord reader)
        {
            return new Listing
            {
                ListingId = reader.GetInt32(0),
                ListingActive = reader.GetInt32(1).Equals(1),
                StreetNumber = reader.GetInt32(2),
                StreetName = reader.GetString(3),
                Locality = reader.GetString(4),
                PostalCode = reader.GetInt32(5),
                ApartmentNumber = reader.GetInt32(6),
                UnitType = reader.GetInt32(7),
                BedroomsTotal = reader.GetInt32(8),
                BedroomsAvailable = reader.GetInt32(9),
                DateAvailable = reader.GetDateTime(10),
                DateListed = reader.GetDateTime(11),
                DateExpiry = reader.GetDateTime(12),
                LeaseLengthMonths = reader.GetInt32(13),
                AmtRent = (float) reader.GetDecimal(14),
                AmtSecurity = (float) reader.GetDecimal(15),
                AmenityHeating = reader.GetInt32(16).Equals(1),
                AmenityGas = reader.GetInt32(17).Equals(1),
                AmenityWater = reader.GetInt32(18).Equals(1),
                AmenityElectricity = reader.GetInt32(19).Equals(1),
                AmenityInternet = reader.GetInt32(20).Equals(1),
                AmenityCable = reader.GetInt32(21).Equals(1),
                AmenityStove = reader.GetInt32(22).Equals(1),
                AmenityRefrigerator = reader.GetInt32(23).Equals(1),
                AmenityOffStreetParking = reader.GetInt32(24).Equals(1),
                AmenityGarage = reader.GetInt32(25).Equals(1),
                AmenityInsulated = reader.GetInt32(26).Equals(1),
                AmenityGarbageCollection = reader.GetInt32(27).Equals(1),
                AmenityCentralAc = reader.GetInt32(28).Equals(1),
                AmenityStormWindows = reader.GetInt32(29).Equals(1),
                AmenityFurnished = reader.GetInt32(30).Equals(1),
                AmenityWasher = reader.GetInt32(31).Equals(1),
                AmenityDryer = reader.GetInt32(32).Equals(1),
                AmenitySmokeDetectors = reader.GetInt32(33).Equals(1),
                AmenitySecuritySystem = reader.GetInt32(34).Equals(1),
                AmenityExteriorDoorDeadbolt = reader.GetInt32(35).Equals(1),
                AmenityFireSafetyCode = reader.GetInt32(36).Equals(1),
                AmenityCoDetectors = reader.GetInt32(37).Equals(1),
                AmenityElectricalSafetyCode = reader.GetInt32(38).Equals(1),
                RulesPetsAllowed = reader.GetInt32(39).Equals(1),
                BedroomsLocatedBasementAttic = reader.GetInt32(40).Equals(1),
                VerifiedProperty = reader.GetInt32(41).Equals(1),
                CertificateOfOccupancy = reader.GetInt32(42).Equals(1),
                RegisteredRentalRegistry = reader.GetInt32(43).Equals(1),
                ContactDetailsId = reader.GetInt32(44),
                Comments = reader.GetString(45)
            };
        }

        public override string ToString()
        {
            return
                $"ListingId: {ListingId}, " +
                $"ListingActive: {ListingActive}, " +
                $"StreetNumber: {StreetNumber}, " +
                $"StreetName: {StreetName}, " +
                $"Locality: {Locality}, " +
                $"PostalCode: {PostalCode}, " +
                $"ApartmentNumber: {ApartmentNumber}, " +
                $"UnitType: {UnitType}, " +
                $"BedroomsTotal: {BedroomsTotal}, " +
                $"BedroomsAvailable: {BedroomsAvailable}, " +
                $"DateAvailable: {DateAvailable}, " +
                $"DateListed: {DateListed}, " +
                $"DateExpiry: {DateExpiry}, " +
                $"LeaseLengthMonths: {LeaseLengthMonths}, " +
                $"AmtRent: {AmtRent}, " +
                $"AmtSecurity: {AmtSecurity}, " +
                $"AmenityHeating: {AmenityHeating}, " +
                $"AmenityGas: {AmenityGas}, " +
                $"AmenityWater: {AmenityWater}, " +
                $"AmenityElectricity: {AmenityElectricity}, " +
                $"AmenityInternet: {AmenityInternet}, " +
                $"AmenityCable: {AmenityCable}, " +
                $"AmenityStove: {AmenityStove}, " +
                $"AmenityRefrigerator: {AmenityRefrigerator}, " +
                $"AmenityOffStreetParking: {AmenityOffStreetParking}, " +
                $"AmenityGarage: {AmenityGarage}, " +
                $"AmenityInsulated: {AmenityInsulated}, " +
                $"AmenityGarbageCollection: {AmenityGarbageCollection}, " +
                $"AmenityCentralAc: {AmenityCentralAc}, " +
                $"AmenityStormWindows: {AmenityStormWindows}, " +
                $"AmenityFurnished: {AmenityFurnished}, " +
                $"AmenityWasher: {AmenityWasher}, " +
                $"AmenityDryer: {AmenityDryer}, " +
                $"AmenitySmokeDetectors: {AmenitySmokeDetectors}, " +
                $"AmenitySecuritySystem: {AmenitySecuritySystem}, " +
                $"AmenityExteriorDoorDeadbolt: {AmenityExteriorDoorDeadbolt}, " +
                $"AmenityFireSafetyCode: {AmenityFireSafetyCode}, " +
                $"AmenityCoDetectors: {AmenityCoDetectors}, " +
                $"AmenityElectricalSafetyCode: {AmenityElectricalSafetyCode}, " +
                $"RulesPetsAllowed: {RulesPetsAllowed}, " +
                $"BedroomsLocatedBasementAttic: {BedroomsLocatedBasementAttic}, " +
                $"VerifiedProperty: {VerifiedProperty}, " +
                $"CertificateOfOccupancy: {CertificateOfOccupancy}, " +
                $"RegisteredRentalRegistry: {RegisteredRentalRegistry}, " +
                $"ContactDetailsId: {ContactDetailsId}, " +
                $"Comments: {Comments}";
        }

        public string ToBareString()
        {
            return
                $"{ListingId}" +
                $"{ListingActive}" +
                $"{StreetNumber}" +
                $"{StreetName}" +
                $"{Locality}" +
                $"{PostalCode}" +
                $"{ApartmentNumber}" +
                $"{UnitType}" +
                $"{BedroomsTotal}" +
                $"{BedroomsAvailable}" +
                $"{DateAvailable}" +
                $"{DateListed}" +
                $"{DateExpiry}" +
                $"{LeaseLengthMonths}" +
                $"{AmtRent}" +
                $"{AmtSecurity}" +
                $"{AmenityHeating}" +
                $"{AmenityGas}" +
                $"{AmenityWater}" +
                $"{AmenityElectricity}" +
                $"{AmenityInternet}" +
                $"{AmenityCable}" +
                $"{AmenityStove}" +
                $"{AmenityRefrigerator}" +
                $"{AmenityOffStreetParking}" +
                $"{AmenityGarage}" +
                $"{AmenityInsulated}" +
                $"{AmenityGarbageCollection}" +
                $"{AmenityCentralAc}" +
                $"{AmenityStormWindows}" +
                $"{AmenityFurnished}" +
                $"{AmenityWasher}" +
                $"{AmenityDryer}" +
                $"{AmenitySmokeDetectors}" +
                $"{AmenitySecuritySystem}" +
                $"{AmenityExteriorDoorDeadbolt}" +
                $"{AmenityFireSafetyCode}" +
                $"{AmenityCoDetectors}" +
                $"{AmenityElectricalSafetyCode}" +
                $"{RulesPetsAllowed}" +
                $"{BedroomsLocatedBasementAttic}" +
                $"{VerifiedProperty}" +
                $"{CertificateOfOccupancy}" +
                $"{RegisteredRentalRegistry}" +
                $"{ContactDetailsId}" +
                $"{Comments}";
        }

        public Listing ReplaceNullWithDefaults()
        {
            return new Listing
            {
                ListingId = this.ListingId ?? -1,
                ListingActive = this.ListingActive ?? false,
                StreetNumber = this.StreetNumber ?? -1,
                StreetName = this.StreetName ?? "",
                Locality = this.Locality ?? "",
                PostalCode = this.PostalCode ?? -1,
                ApartmentNumber = this.ApartmentNumber ?? -1,
                UnitType = this.UnitType ?? 0,
                BedroomsTotal = this.BedroomsTotal ?? -1,
                BedroomsAvailable = this.BedroomsAvailable ?? -1,
//DateAvailable = this.DateAvailable ?? new DateTime("1970-01-01"),
//DateListed = this.DateListed ?? false,
//DateExpiry = this.DateExpiry ?? false,
                LeaseLengthMonths = this.LeaseLengthMonths ?? -1,
                AmtRent = this.AmtRent ?? -1,
                AmtSecurity = this.AmtSecurity ?? -1,
                AmenityHeating = this.AmenityHeating ?? false,
                AmenityGas = this.AmenityGas ?? false,
                AmenityWater = this.AmenityWater ?? false,
                AmenityElectricity = this.AmenityElectricity ?? false,
                AmenityInternet = this.AmenityInternet ?? false,
                AmenityCable = this.AmenityCable ?? false,
                AmenityStove = this.AmenityStove ?? false,
                AmenityRefrigerator = this.AmenityRefrigerator ?? false,
                AmenityOffStreetParking = this.AmenityOffStreetParking ?? false,
                AmenityGarage = this.AmenityGarage ?? false,
                AmenityInsulated = this.AmenityInsulated ?? false,
                AmenityGarbageCollection = this.AmenityGarbageCollection ?? false,
                AmenityCentralAc = this.AmenityCentralAc ?? false,
                AmenityStormWindows = this.AmenityStormWindows ?? false,
                AmenityFurnished = this.AmenityFurnished ?? false,
                AmenityWasher = this.AmenityWasher ?? false,
                AmenityDryer = this.AmenityDryer ?? false,
                AmenitySmokeDetectors = this.AmenitySmokeDetectors ?? false,
                AmenitySecuritySystem = this.AmenitySecuritySystem ?? false,
                AmenityExteriorDoorDeadbolt = this.AmenityExteriorDoorDeadbolt ?? false,
                AmenityFireSafetyCode = this.AmenityFireSafetyCode ?? false,
                AmenityCoDetectors = this.AmenityCoDetectors ?? false,
                AmenityElectricalSafetyCode = this.AmenityElectricalSafetyCode ?? false,
                RulesPetsAllowed = this.RulesPetsAllowed ?? false,
                BedroomsLocatedBasementAttic = this.BedroomsLocatedBasementAttic ?? false,
                VerifiedProperty = this.VerifiedProperty ?? false,
                CertificateOfOccupancy = this.CertificateOfOccupancy ?? false,
                RegisteredRentalRegistry = this.RegisteredRentalRegistry ?? false,
                ContactDetailsId = this.ContactDetailsId ?? -1,
                Comments = this.Comments ?? ""
            };
        }
    }
}