using System.Collections.Generic;

namespace Och.Models.Db
{
    public class ComplexApt
    {
        public ApartmentComplex AptComplex { get; set; }
        public List<ListingPicture> ListingPictures { get; set; }
        public ListingLocation ListingLocation { get; set; }
        public ListingContact ListingContact { get; set; }
    }
}