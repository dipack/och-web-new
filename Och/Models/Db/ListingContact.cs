using System.Data;

namespace Och.Models.Db
{
    public class ListingContact
    {
        public int ContactId { get; set; }
        public string Name { get; set; }
        public string PrimaryPhone { get; set; }
        public string SecondaryPhone { get; set; }
        public string Email { get; set; }

        public static ListingContact PopulateFromRecord(IDataRecord reader)
        {
            return new ListingContact
            {
                ContactId = reader.GetInt32(0),
                Name = reader.GetString(1),
                PrimaryPhone = reader.GetString(2),
                SecondaryPhone = reader.GetString(3),
                Email = reader.GetString(4)
            };
        }
    }
}