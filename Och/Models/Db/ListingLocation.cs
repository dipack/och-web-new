using System.Data;

namespace Och.Models.Db
{
    public class ListingLocation
    {
        public int ListingId { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }

        public static ListingLocation PopulateFromRecord(IDataRecord reader)
        {
            return new ListingLocation
            {
                ListingId = reader.GetInt32(0),
                Latitude = reader.GetDecimal(1),
                Longitude = reader.GetDecimal(2)
            };
        }
    }
}