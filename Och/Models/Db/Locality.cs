using System.Data;

namespace Och.Models.Db
{
    public class Locality
    {
        public int PostalCode { get; set; }
        public string Name { get; set; }

        public static Locality PopulateFromRecord(IDataRecord reader)
        {
            return new Locality
            {
                PostalCode = reader.GetInt32(0),
                Name = reader.GetString(1)
            };
        }
    }
}