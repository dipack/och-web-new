using System.Data;

namespace Och.Models.Db
{
    public class UnitType
    {
        public int UnitTypeId { get; set; }
        public string UnitTypeName { get; set; }

        public static UnitType PopulateFromRecord(IDataRecord reader)
        {
            return new UnitType
            {
                UnitTypeId = reader.GetInt32(0),
                UnitTypeName = reader.GetString(1)
            };
        }
    }
}