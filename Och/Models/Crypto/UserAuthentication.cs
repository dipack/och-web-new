using System;
using System.Security.Cryptography;

namespace Och.Models.Crypto
{
    public class UserAuthentication
    {
        // TODO: Make global variables for things like password length
        public static string HashAndSaltPassword(string password)
        {
            const int saltLength = 32;
            var salt = GetSalt(saltLength);
            byte[] hashBytes = HashPassword(salt, password);
            string passwordHash = Convert.ToBase64String(hashBytes);
            return passwordHash;
        }

        public static byte[] HashPassword(byte[] salt, string password)
        {
            var hashingAlgorithm = HashAlgorithmName.SHA512;
            // Sha512 gives us 512 bit length hashes = 64 bytes
            const int passwordHashLength = 64;
            const int saltLength = 32;
            const int maxIterations = 1000;
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, maxIterations, hashingAlgorithm);
            var hash = pbkdf2.GetBytes(passwordHashLength);
            byte[] hashBytes = new byte[passwordHashLength + saltLength];
            Array.Copy(salt, 0, hashBytes, 0, saltLength);
            Array.Copy(hash, 0, hashBytes, saltLength, passwordHashLength);
            return hashBytes;
        }

        public static bool ValidatePassword(string passwordHash, string enteredPassword)
        {
            byte[] hashBytes = Convert.FromBase64String(passwordHash);
            const int passwordHashLength = 64;
            const int saltLength = 32;
            var salt = new byte[saltLength];
            Array.Copy(hashBytes, 0, salt, 0, saltLength);
            var hashedEnteredPassword = HashPassword(salt, enteredPassword);
            return Convert.ToBase64String(hashedEnteredPassword).Equals(passwordHash);
        }

        public static byte[] GetSalt(int maxSaltLength = 32)
        {
            var salt = new byte[maxSaltLength];
            using (var random = new RNGCryptoServiceProvider())
            {
                random.GetNonZeroBytes(salt);
            }

            return salt;
        }
    }
}