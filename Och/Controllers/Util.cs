using System;
using System.Text.RegularExpressions;

namespace Och.Controllers
{
    public static class Util
    {
        public static string GenerateUID()
        {
            return Guid.NewGuid().ToString();
        }

        public static int LevenshteinDistance(string str1, string str2)
        {
            var l1 = str1.Length;
            var l2 = str2.Length;

            var distance = new int[l1, l2];

            for (var idx = 0; idx < l1; idx++)
            for (var jdx = 0; jdx < l2; jdx++)
            {
                if (idx >= 1 && idx <= l1 && jdx == 0)
                {
                    distance[idx, jdx] = idx;
                }

                else if (jdx >= 1 && jdx <= l1 && idx == 0)
                {
                    distance[idx, jdx] = jdx;
                }
                else
                {
                    distance[idx, jdx] = 0;
                }
            }

            for (var jdx = 1; jdx < l2; jdx++)
            for (var idx = 1; idx < l1; idx++)
            {
                var cost = 0;
                if (str1[idx] == str2[jdx])
                    cost = 0;
                else
                    cost = 1;
                distance[idx, jdx] = Math.Min(
                    distance[idx - 1, jdx] + 1,
                    Math.Min(
                        distance[idx, jdx - 1] + 1,
                        distance[idx - 1, jdx - 1] + cost
                    )
                );
            }

            return distance[l1 - 1, l2 - 1] + 1;
        }

        public static string PascalCaseToUnderscore(string input)
        {
            return Regex.Replace(input, "(?<=.)([A-Z])", "_$0",
                RegexOptions.Compiled).ToLower();
        }

        public static long GetUnixEpoch()
        {
            return (long) (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }
    }
}