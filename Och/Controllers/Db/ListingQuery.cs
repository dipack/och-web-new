using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using Och.Models.Auth;
using Och.Models.Crypto;
using Och.Models.Db;

namespace Och.Controllers.Db
{
    [Route("api/listing")]
    public class ListingQuery : Controller
    {
        private readonly IHostingEnvironment _environment;
        private readonly ILogger _logger;
        private ImageQuery imageQuery;
        private LocationQuery locationQuery;
        private ContactQuery contactQuery;


        public ListingQuery(IHostingEnvironment environment,
            ILogger<ListingQuery> logger,
            ImageQuery iQ,
            LocationQuery lQ,
            ContactQuery cQ)
        {
            _environment = environment;
            _logger = logger;
            imageQuery = iQ;
            locationQuery = lQ;
            contactQuery = cQ;
        }

        private MySqlConnection GetOCHDbConnection()
        {
            // See if we can use the connection string stored in the appsettings.json file
            // https://mysql-net.github.io/MySqlConnector/tutorials/net-core-mvc/
            const string connectionString =
                "Server=localhost;Port=3306;User ID=dipack;Password=dipackiscool;Database=och_web_1;";
            var connection = (new OCHDb(connectionString)).Connection;
            return connection;
        }

        private void TestCrypto()
        {
            var password = "IamCool";
            var samplePasswordHash = UserAuthentication.HashAndSaltPassword(password);
            var isEqual = UserAuthentication.ValidatePassword(samplePasswordHash, password);
            _logger.LogInformation($"Are the passwords equal? {isEqual}");
        }

        private static MySqlCommand ComposeGetListingLikeCmd(Listing value)
        {
            var searchCmd = new MySqlCommand($"SELECT * from {OCHDb.Tables.Listings} " +
                                             $"WHERE " +
                                             $"postal_code = @postal_code AND " +
                                             $"locality = @locality AND " +
                                             $"unit_type = @unit_type AND " +
                                             $"amt_rent BETWEEN @low AND @high");
            searchCmd.Parameters.AddWithValue("postal_code", value.PostalCode);
            searchCmd.Parameters.AddWithValue("locality", value.Locality);
            searchCmd.Parameters.AddWithValue("unit_type", value.UnitType);
            searchCmd.Parameters.AddWithValue("low", value.AmtRent);
            searchCmd.Parameters.AddWithValue("high", value.AmtSecurity);
            if (value.StreetName.Length > 0)
            {
//                searchCmd.CommandText += $" AND street_name LIKE %{value.StreetName}% ";
                var wildStreetName = $"%{value.StreetName}%";
                searchCmd.CommandText += " AND street_name LIKE @street_name ";
                searchCmd.Parameters.Add(new MySqlParameter("street_name", wildStreetName));
            }

            var amenities = value.GetType().GetProperties()
                .Where(property => property.Name.StartsWith("Amenity") && property.GetValue(value).Equals(true))
                .ToList();
            foreach (var amenity in amenities)
            {
                var underscore = Util.PascalCaseToUnderscore(amenity.Name);
                var clause = $" AND {underscore} = @{underscore} ";
                searchCmd.CommandText += clause;
                searchCmd.Parameters.AddWithValue(underscore, true);
            }

            return searchCmd;
        }

        [HttpGet("search")]
        public async Task<List<ComplexListing>> SearchGET([FromQuery] Listing value, [FromQuery] bool getAll = false)
        {
            // Set Listing properties to be nullable?
            // See how it affects the rest of the code
            var cleaned = value.ReplaceNullWithDefaults();
            return await SearchPOST(cleaned, getAll);
        }

        [HttpPost("search/{getAll}")]
        public async Task<List<ComplexListing>> SearchPOST([FromBody] Listing value, bool getAll = false)
        {
            var MIN_RELEVANT_RECORDS = 3;

            if (!ModelState.IsValid)
            {
                throw new InvalidCastException("Malformed model formed by mapping POST Request");
            }

            TestCrypto();
            var searchCmd = ComposeGetListingLikeCmd(value);
            var relevantListings = new List<ComplexListing>();
            using (var connection = GetOCHDbConnection())
            {
                await connection.OpenAsync();
                searchCmd.Connection = connection;
                using (var reader = await searchCmd.ExecuteReaderAsync())
                {
                    while (await reader.ReadAsync())
                    {
                        var listing = Listing.PopulateFromRecord(reader);
                        var complexListing = new ComplexListing
                        {
                            Listing = listing,
                            ListingLocation =
                                await locationQuery.GetListingLocation(listing.ListingId.GetValueOrDefault(-1)),
                            ListingPictures =
                                await imageQuery.GetListingPictures(listing.ListingId.GetValueOrDefault(-1)),
                            ListingContact =
                                await contactQuery.GetContact(listing.ContactDetailsId.GetValueOrDefault(-1))
                        };
                        relevantListings.Add(complexListing);
                    }
                }
            }

            // If we have too few listings that are similar to what the user is looking for,
            // then we simply return all listings from the database, ranked by similarity
            if (relevantListings.Count < MIN_RELEVANT_RECORDS)
            {
                _logger.LogInformation(
                    $"Too few relevant listings found ({relevantListings.Count} < {MIN_RELEVANT_RECORDS})");
                _logger.LogInformation("Fetching all listings in database and sorting based on similarity score");

                var getAllListingsCmd = new MySqlCommand($"SELECT * from {OCHDb.Tables.Listings}");
                var allListings = new List<ComplexListing>();
                using (var connection = GetOCHDbConnection())
                {
                    await connection.OpenAsync();
                    getAllListingsCmd.Connection = connection;
                    using (var reader = await getAllListingsCmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            var listing = Listing.PopulateFromRecord(reader);
                            var complexListing = new ComplexListing
                            {
                                Listing = listing,
                                ListingLocation =
                                    await locationQuery.GetListingLocation(listing.ListingId.GetValueOrDefault(-1)),
                                ListingPictures =
                                    await imageQuery.GetListingPictures(listing.ListingId.GetValueOrDefault(-1)),
                                ListingContact =
                                    await contactQuery.GetContact(listing.ContactDetailsId.GetValueOrDefault(-1))
                            };
                            allListings.Add(complexListing);
                        }
                    }
                }

                allListings.RemoveAll(listing =>
                    relevantListings.Exists(rl => rl.Listing.ListingId == listing.Listing.ListingId));
                allListings.Sort((x, y) =>
                    Util.LevenshteinDistance(x.Listing.ToBareString(), y.Listing.ToBareString()));
                relevantListings.AddRange(allListings);
            }

            if (!getAll)
            {
                var todayDateTime = DateTime.Today;
                return relevantListings.Where(l => l.Listing.DateExpiry >= todayDateTime).ToList();
            }
            else
            {
                _logger.LogInformation("Ignoring expired listings and returning all hits");
                return relevantListings;
            }
        }

        [Authorize(Roles = Role.Admin)]
        [HttpGet("get-all-listings")]
        public async Task<List<ComplexListing>> GetListings()
        {
            var listings = new List<ComplexListing>();
            using (var connection = GetOCHDbConnection())
            {
                await connection.OpenAsync();
                using (var cmd = new MySqlCommand($"SELECT * from {OCHDb.Tables.Listings}", connection))
                {
                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            var listing = Listing.PopulateFromRecord(reader);
                            var complexListing = new ComplexListing
                            {
                                Listing = listing,
                                ListingLocation =
                                    await locationQuery.GetListingLocation(listing.ListingId.GetValueOrDefault(-1)),
                                ListingPictures =
                                    await imageQuery.GetListingPictures(listing.ListingId.GetValueOrDefault(-1)),
                                ListingContact =
                                    await contactQuery.GetContact(listing.ContactDetailsId.GetValueOrDefault(-1))
                            };
                            listings.Add(complexListing);
                        }
                    }
                }
            }

            return listings;
        }

        [HttpPost("update")]
        public async Task<bool> UpdateListing([FromBody] Listing updatedListing)
        {
            if (!ModelState.IsValid)
            {
                throw new InvalidCastException("Malformed model formed by mapping POST Request");
            }

            var isSuccessful = false;

            using (var connection = GetOCHDbConnection())
            {
                await connection.OpenAsync();
                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandText =
                        $"UPDATE {OCHDb.Tables.Listings} SET listing_active = @listing_active, street_number = @street_number, street_name = @street_name, locality = @locality, postal_code = @postal_code, apartment_number = @apartment_number, unit_type = @unit_type, bedrooms_total = @bedrooms_total, bedrooms_available = @bedrooms_available, date_available = @date_available, date_listed = @date_listed, date_expiry = @date_expiry, lease_length_months = @lease_length_months, amt_rent = @amt_rent, amt_security = @amt_security, amenity_heating = @amenity_heating, amenity_gas = @amenity_gas, amenity_water = @amenity_water, amenity_electricity = @amenity_electricity, amenity_internet = @amenity_internet, amenity_cable = @amenity_cable, amenity_stove = @amenity_stove, amenity_refrigerator = @amenity_refrigerator, amenity_off_street_parking = @amenity_off_street_parking, amenity_garage = @amenity_garage, amenity_insulated = @amenity_insulated, amenity_garbage_collection = @amenity_garbage_collection, amenity_central_ac = @amenity_central_ac, amenity_storm_windows = @amenity_storm_windows, amenity_furnished = @amenity_furnished, amenity_washer = @amenity_washer, amenity_dryer = @amenity_dryer, amenity_smoke_detectors = @amenity_smoke_detectors, amenity_security_system = @amenity_security_system, amenity_exterior_door_deadbolt = @amenity_exterior_door_deadbolt, amenity_fire_safety_code = @amenity_fire_safety_code, amenity_CO_detectors = @amenity_CO_detectors, amenity_electrical_safety_code = @amenity_electrical_safety_code, rules_pets_allowed = @rules_pets_allowed, bedrooms_located_basement_attic = @bedrooms_located_basement_attic, verified_property = @verified_property, certificate_of_occupancy = @certificate_of_occupancy, registered_rental_registry = @registered_rental_registry, contact_details_id = @contact_details_id, comments = @comments " +
                        $"WHERE listing_id = @listing_id";
                    var upCmd = PopulateListingQuery(cmd, updatedListing, true);
                    int retCode = await upCmd.ExecuteNonQueryAsync();
                    if (retCode > 0)
                        isSuccessful = true;
                }
            }

            return isSuccessful;
        }

        [HttpPost("insert")]
        public async Task<long> InsertListing([FromBody] Listing newListing)
        {
            if (!ModelState.IsValid)
            {
                throw new InvalidCastException("Malformed model formed by mapping POST Request");
            }

            long lastInsertedId = -1;
            using (var connection = GetOCHDbConnection())
            {
                await connection.OpenAsync();
                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandText =
                        $"INSERT INTO {OCHDb.Tables.Listings} (listing_active,street_number,street_name,locality,postal_code,apartment_number,unit_type,bedrooms_total,bedrooms_available,date_available,date_listed,date_expiry,lease_length_months,amt_rent,amt_security,amenity_heating,amenity_gas,amenity_water,amenity_electricity,amenity_internet,amenity_cable,amenity_stove,amenity_refrigerator,amenity_off_street_parking,amenity_garage,amenity_insulated,amenity_garbage_collection,amenity_central_ac,amenity_storm_windows,amenity_furnished,amenity_washer,amenity_dryer,amenity_smoke_detectors,amenity_security_system,amenity_exterior_door_deadbolt,amenity_fire_safety_code,amenity_CO_detectors,amenity_electrical_safety_code,rules_pets_allowed,bedrooms_located_basement_attic,verified_property,certificate_of_occupancy,registered_rental_registry,contact_details_id,comments)VALUES(@listing_active,@street_number,@street_name,@locality,@postal_code,@apartment_number,@unit_type,@bedrooms_total,@bedrooms_available,@date_available,@date_listed,@date_expiry,@lease_length_months,@amt_rent,@amt_security,@amenity_heating,@amenity_gas,@amenity_water,@amenity_electricity,@amenity_internet,@amenity_cable,@amenity_stove,@amenity_refrigerator,@amenity_off_street_parking,@amenity_garage,@amenity_insulated,@amenity_garbage_collection,@amenity_central_ac,@amenity_storm_windows,@amenity_furnished,@amenity_washer,@amenity_dryer,@amenity_smoke_detectors,@amenity_security_system,@amenity_exterior_door_deadbolt,@amenity_fire_safety_code,@amenity_CO_detectors,@amenity_electrical_safety_code,@rules_pets_allowed,@bedrooms_located_basement_attic,@verified_property,@certificate_of_occupancy,@registered_rental_registry,@contact_details_id,@comments)";
                    var insertCmd = PopulateListingQuery(cmd, newListing);
                    await insertCmd.ExecuteNonQueryAsync();

                    lastInsertedId = insertCmd.LastInsertedId;
                }
            }

            return lastInsertedId;
        }

        public async Task<long> InsertAptComplexListing(ApartmentComplex complex)
        {
            var aptComplexListing = new Listing
            {
                ListingActive = complex.ListingActive,
                // Apartment
                UnitType = 1,
                Comments = "Apartment Complex"
            };

            return await InsertListing(aptComplexListing);
        }

        [HttpGet("get-listing/{listingId}")]
        public async Task<Listing> GetListing(int listingId)
        {
            using (var connection = GetOCHDbConnection())
            {
                await connection.OpenAsync();
                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandText = $"SELECT * from {OCHDb.Tables.Listings} WHERE listing_id = @Id";
                    cmd.Parameters.AddWithValue("Id", listingId);
                    using (var reader = await cmd.ExecuteReaderAsync())
                        while (await reader.ReadAsync())
                        {
                            return Listing.PopulateFromRecord(reader);
                        }
                }
            }

            return null;
        }

        [HttpGet("delete-listing/{listingId}")]
        public async Task<bool> DeleteListing(int listingId)
        {
            var isSuccessful = false;
            using (var connection = GetOCHDbConnection())
            {
                await connection.OpenAsync();
                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandText = $"DELETE from {OCHDb.Tables.Listings} WHERE listing_id = @Id";
                    cmd.Parameters.AddWithValue("Id", listingId);
                    var retCode = await cmd.ExecuteNonQueryAsync();
                    if (retCode > 0)
                        isSuccessful = true;
                }
            }

            return isSuccessful;
        }

        private static MySqlCommand PopulateListingQuery(MySqlCommand refCmd, Listing listing,
            bool useListingId = false)
        {
            var cmd = new MySqlCommand
            {
                Connection = refCmd.Connection,
                CommandText = refCmd.CommandText
            };

            if (useListingId)
            {
                cmd.Parameters.AddWithValue("listing_id", listing.ListingId);
            }

            cmd.Parameters.AddWithValue("listing_active", listing.ListingActive);
            cmd.Parameters.AddWithValue("street_number", listing.StreetNumber);
            cmd.Parameters.AddWithValue("street_name", listing.StreetName);
            cmd.Parameters.AddWithValue("locality", listing.Locality);
            cmd.Parameters.AddWithValue("postal_code", listing.PostalCode);
            cmd.Parameters.AddWithValue("apartment_number", listing.ApartmentNumber);
            cmd.Parameters.AddWithValue("unit_type", listing.UnitType);
            cmd.Parameters.AddWithValue("bedrooms_total", listing.BedroomsTotal);
            cmd.Parameters.AddWithValue("bedrooms_available", listing.BedroomsAvailable);
            cmd.Parameters.AddWithValue("date_available", listing.DateAvailable);
            cmd.Parameters.AddWithValue("date_listed", listing.DateListed);
            cmd.Parameters.AddWithValue("date_expiry", listing.DateExpiry);
            cmd.Parameters.AddWithValue("lease_length_months", listing.LeaseLengthMonths);
            cmd.Parameters.AddWithValue("amt_rent", listing.AmtRent);
            cmd.Parameters.AddWithValue("amt_security", listing.AmtSecurity);
            cmd.Parameters.AddWithValue("amenity_heating", listing.AmenityHeating);
            cmd.Parameters.AddWithValue("amenity_gas", listing.AmenityGas);
            cmd.Parameters.AddWithValue("amenity_water", listing.AmenityWater);
            cmd.Parameters.AddWithValue("amenity_electricity", listing.AmenityElectricity);
            cmd.Parameters.AddWithValue("amenity_internet", listing.AmenityInternet);
            cmd.Parameters.AddWithValue("amenity_cable", listing.AmenityCable);
            cmd.Parameters.AddWithValue("amenity_stove", listing.AmenityStove);
            cmd.Parameters.AddWithValue("amenity_refrigerator", listing.AmenityRefrigerator);
            cmd.Parameters.AddWithValue("amenity_off_street_parking", listing.AmenityOffStreetParking);
            cmd.Parameters.AddWithValue("amenity_garage", listing.AmenityGarage);
            cmd.Parameters.AddWithValue("amenity_insulated", listing.AmenityInsulated);
            cmd.Parameters.AddWithValue("amenity_garbage_collection", listing.AmenityGarbageCollection);
            cmd.Parameters.AddWithValue("amenity_central_ac", listing.AmenityCentralAc);
            cmd.Parameters.AddWithValue("amenity_storm_windows", listing.AmenityStormWindows);
            cmd.Parameters.AddWithValue("amenity_furnished", listing.AmenityFurnished);
            cmd.Parameters.AddWithValue("amenity_washer", listing.AmenityWasher);
            cmd.Parameters.AddWithValue("amenity_dryer", listing.AmenityDryer);
            cmd.Parameters.AddWithValue("amenity_smoke_detectors", listing.AmenitySmokeDetectors);
            cmd.Parameters.AddWithValue("amenity_security_system", listing.AmenitySecuritySystem);
            cmd.Parameters.AddWithValue("amenity_exterior_door_deadbolt",
                listing.AmenityExteriorDoorDeadbolt);
            cmd.Parameters.AddWithValue("amenity_fire_safety_code", listing.AmenityFireSafetyCode);
            cmd.Parameters.AddWithValue("amenity_CO_detectors", listing.AmenityCoDetectors);
            cmd.Parameters.AddWithValue("amenity_electrical_safety_code",
                listing.AmenityElectricalSafetyCode);
            cmd.Parameters.AddWithValue("rules_pets_allowed", listing.RulesPetsAllowed);
            cmd.Parameters.AddWithValue("bedrooms_located_basement_attic",
                listing.BedroomsLocatedBasementAttic);
            cmd.Parameters.AddWithValue("verified_property", listing.VerifiedProperty);
            cmd.Parameters.AddWithValue("certificate_of_occupancy", listing.CertificateOfOccupancy);
            cmd.Parameters.AddWithValue("registered_rental_registry", listing.RegisteredRentalRegistry);
            cmd.Parameters.AddWithValue("contact_details_id", listing.ContactDetailsId);
            cmd.Parameters.AddWithValue("comments", listing.Comments);

            return cmd;
        }
    }
}