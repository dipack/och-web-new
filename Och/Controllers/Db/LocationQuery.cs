using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using Och.Models.Db;


namespace Och.Controllers.Db
{
    [Route("api/listing")]
    public class LocationQuery : Controller
    {
        private readonly ILogger _logger;

        public LocationQuery(ILogger<LocationQuery> logger)
        {
            _logger = logger;
        }

        private MySqlConnection GetOCHDbConnection()
        {
            // See if we can use the connection string stored in the appsettings.json file
            // https://mysql-net.github.io/MySqlConnector/tutorials/net-core-mvc/
            const string connectionString =
                "Server=localhost;Port=3306;User ID=dipack;Password=dipackiscool;Database=och_web_1;";
            var connection = (new OCHDb(connectionString)).Connection;
            return connection;
        }

        [HttpGet("get-listing-location/{listingId}")]
        public async Task<ListingLocation> GetListingLocation(int listingId)
        {
            var location = new ListingLocation
            {
                ListingId = listingId,
                Latitude = (decimal) Constants.LatLonInvalidValue,
                Longitude = (decimal) Constants.LatLonInvalidValue
            };
            using (var connection = GetOCHDbConnection())
            {
                await connection.OpenAsync();
                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandText = $"SELECT * from {OCHDb.Tables.ListingLocations} WHERE listing_id = @Id";
                    cmd.Parameters.AddWithValue("Id", listingId);
                    using (var reader = await cmd.ExecuteReaderAsync())
                        while (await reader.ReadAsync())
                        {
                            location = ListingLocation.PopulateFromRecord(reader);
                        }
                }
            }

            return location;
        }

        [HttpPost("update-listing-location")]
        public async Task<bool> UpdateLocation([FromBody] ListingLocation updatedListing)
        {
            if (!ModelState.IsValid)
            {
                throw new InvalidCastException("Malformed model formed by mapping POST Request");
            }

            var isSuccessful = false;

            using (var connection = GetOCHDbConnection())
            {
                await connection.OpenAsync();
                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandText =
                        $"UPDATE {OCHDb.Tables.ListingLocations} SET latitude = @latitude, longitude = @longitude " +
                        $"WHERE listing_id = @Id;";
                    cmd.Parameters.AddWithValue("latitude", updatedListing.Latitude);
                    cmd.Parameters.AddWithValue("longitude", updatedListing.Longitude);
                    cmd.Parameters.AddWithValue("Id", updatedListing.ListingId);
                    int retCode = await cmd.ExecuteNonQueryAsync();
                    if (retCode > 0)
                        isSuccessful = true;
                }
            }

            return isSuccessful;
        }

        [HttpPost("insert-listing-location")]
        public async Task<long> InsertLocation([FromBody] ListingLocation newListing)
        {
            if (!ModelState.IsValid)
            {
                throw new InvalidCastException("Malformed model formed by mapping POST Request");
            }

            long lastInsertedId = -1;
            using (var connection = GetOCHDbConnection())
            {
                await connection.OpenAsync();
                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandText =
                        $"INSERT INTO {OCHDb.Tables.ListingLocations} (listing_id, latitude, longitude) VALUES (@listing_id, @latitude, @longitude);";
                    cmd.Parameters.AddWithValue("latitude", newListing.Latitude);
                    cmd.Parameters.AddWithValue("longitude", newListing.Longitude);
                    cmd.Parameters.AddWithValue("listing_id", newListing.ListingId);
                    await cmd.ExecuteNonQueryAsync();

                    lastInsertedId = cmd.LastInsertedId;
                }
            }

            return lastInsertedId;
        }

        [HttpGet("delete-listing-location/{listingId}")]
        public async Task<bool> DeletePicture(int listingId)
        {
            var isSuccessful = false;
            using (var connection = GetOCHDbConnection())
            {
                await connection.OpenAsync();
                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandText = $"DELETE from {OCHDb.Tables.ListingLocations} WHERE listing_id = @Id";
                    cmd.Parameters.AddWithValue("Id", listingId);
                    var retCode = await cmd.ExecuteNonQueryAsync();
                    if (retCode > 0)
                        isSuccessful = true;
                }
            }

            return isSuccessful;
        }
    }
}