using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using Och.Models.Db;

namespace Och.Controllers.Db
{
    [Route("api/listing")]
    public class SpringboardQuery : Controller
    {
        private readonly ILogger _logger;

        public SpringboardQuery(ILogger<SpringboardQuery> logger)
        {
            _logger = logger;
        }

        private MySqlConnection GetOCHDbConnection()
        {
            // See if we can use the connection string stored in the appsettings.json file
            // https://mysql-net.github.io/MySqlConnector/tutorials/net-core-mvc/
            const string connectionString =
                "Server=localhost;Port=3306;User ID=dipack;Password=dipackiscool;Database=och_web_1;";
            var connection = (new OCHDb(connectionString)).Connection;
            return connection;
        }

        [HttpGet("springboard")]
        public async Task<Springboard> GetSpringboardInfo()
        {
            var info = new Springboard
            {
                Localities = new List<Locality>(),
                UnitTypes = new List<UnitType>()
            };
            using (var connection = GetOCHDbConnection())
            {
                await connection.OpenAsync();
                using (var cmd = new MySqlCommand($"SELECT * from {OCHDb.Tables.Localities}", connection))
                {
                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            var locality = Locality.PopulateFromRecord(reader);
                            info.Localities.Add(locality);
                        }
                    }
                }

                using (var cmd = new MySqlCommand($"SELECT * from {OCHDb.Tables.UnitTypes}", connection))
                {
                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            var unitType = UnitType.PopulateFromRecord(reader);
                            info.UnitTypes.Add(unitType);
                        }
                    }
                }
            }

            return info;
        }
    }
}