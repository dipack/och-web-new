using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using Och.Models.Db;

namespace Och.Controllers.Db
{
    [Route("api/listing")]
    public class ImageQuery : Controller
    {
        private readonly IHostingEnvironment _environment;
        private readonly ILogger _logger;

        public ImageQuery(IHostingEnvironment environment,
            ILogger<ImageQuery> logger)
        {
            _environment = environment;
            _logger = logger;
        }

        public static class Base64Prefixes
        {
            public const string PNG = "data:image/png;base64,";
            public const string JPG = "data:image/jpeg;base64,";
        }

        public string GetRelativePath(string filename)
        {
            return $"/Static/Pictures/{filename}";
        }

        /**
         * Returns the absolute path for the filename
         */
        public string GetResolvedPath(string filename)
        {
            var webRootImagePath = Path.Combine("Static", "Pictures");
            var imagePath = CombineWithWebRootPath(webRootImagePath);
            return Path.Combine(imagePath, filename);
        }

        public string CombineWithWebRootPath(string path)
        {
            // MS Docs:
            // If arg2 for Path.Combine contains an absolute path,
            // arg2 is returned
            // WTF
            return Path.Combine(_environment.WebRootPath, path);
        }

        public string GenerateFileName(ListingPicture picture)
        {
            var uid = Util.GenerateUID().Replace("-", "");
            return $"{picture.ListingId}_{uid}";
        }

        private MySqlConnection GetOCHDbConnection()
        {
            // See if we can use the connection string stored in the appsettings.json file
            // https://mysql-net.github.io/MySqlConnector/tutorials/net-core-mvc/
            const string connectionString =
                "Server=localhost;Port=3306;User ID=dipack;Password=dipackiscool;Database=och_web_1;";
            var connection = (new OCHDb(connectionString)).Connection;
            return connection;
        }

        [HttpGet("get-listing-pictures/{listingId}")]
        public async Task<List<ListingPicture>> GetListingPictures(int listingId)
        {
            var listingPics = new List<ListingPicture>();
            using (var connection = GetOCHDbConnection())
            {
                await connection.OpenAsync();
                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandText = $"SELECT * from {OCHDb.Tables.ListingImages} WHERE listing_id = @Id";
                    cmd.Parameters.AddWithValue("Id", listingId);
                    using (var reader = await cmd.ExecuteReaderAsync())
                        while (await reader.ReadAsync())
                        {
                            var listingPic = ListingPicture.PopulateFromRecord(reader);
                            listingPics.Add(listingPic);
                        }
                }
            }

            return listingPics;
        }

        [HttpPost("update-listing-picture")]
        public async Task<bool> UpdatePicture([FromBody] ListingPicture updatedListing)
        {
            if (!ModelState.IsValid)
            {
                throw new InvalidCastException("Malformed model formed by mapping POST Request");
            }

            var isSuccessful = false;

            using (var connection = GetOCHDbConnection())
            {
                await connection.OpenAsync();
                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandText =
                        $"UPDATE {OCHDb.Tables.ListingImages} SET listing_id = @listing_id, image_url = @image_url, image_description = @image_description " +
                        $"WHERE image_id = @Id;";
                    cmd.Parameters.AddWithValue("listing_id", updatedListing.ListingId);
                    cmd.Parameters.AddWithValue("image_url", updatedListing.ImageUrl);
                    cmd.Parameters.AddWithValue("image_description", updatedListing.ImageDescription);
                    cmd.Parameters.AddWithValue("Id", updatedListing.ImageId);
                    int retCode = await cmd.ExecuteNonQueryAsync();
                    if (retCode > 0)
                        isSuccessful = true;
                }
            }

            return isSuccessful;
        }

        [HttpPost("insert-listing-picture")]
        public async Task<long> InsertPicture([FromBody] ListingPicture newPicture)
        {
            if (!ModelState.IsValid)
            {
                throw new InvalidCastException("Malformed model formed by mapping POST Request");
            }

            long lastInsertedId = -1;

            var truncatedBase64String = newPicture.ImageUrl;
            var filename = GenerateFileName(newPicture);
            var extension = "";

            if (truncatedBase64String.StartsWith(Base64Prefixes.PNG))
            {
                truncatedBase64String = truncatedBase64String.Replace(Base64Prefixes.PNG, "");
                extension = ".png";
            }
            else if (truncatedBase64String.StartsWith(Base64Prefixes.JPG))
            {
                truncatedBase64String = truncatedBase64String.Replace(Base64Prefixes.JPG, "");
                extension = ".jpg";
            }
            else
            {
                throw new BadImageFormatException("Base64 image string does not correspond to support types (PNG/JPG)");
            }

            filename += extension;
            var relativePath = GetRelativePath(filename);
            var resolvedPath = GetResolvedPath(filename);

            // To make sure we do not accidentally overwrite any files
            while (System.IO.File.Exists(resolvedPath))
            {
                _logger.LogWarning(
                    $"Regenerating filename for picture id {newPicture.ImageId} as {filename} already exists");
                filename = GenerateFileName(newPicture);
                filename += extension;
                relativePath = GetRelativePath(filename);
                resolvedPath = GetResolvedPath(filename);
            }


            byte[] imageBytes = Convert.FromBase64String(truncatedBase64String);

            using (MemoryStream stream = new MemoryStream(imageBytes))
            {
                var image = Image.FromStream(stream);
                image.Save(resolvedPath);
                using (var connection = GetOCHDbConnection())
                {
                    await connection.OpenAsync();
                    using (var cmd = new MySqlCommand())
                    {
                        cmd.Connection = connection;
                        cmd.CommandText =
                            $"INSERT INTO {OCHDb.Tables.ListingImages} (listing_id, image_url, image_description) VALUES (@listing_id, @image_url, @image_description);";
                        cmd.Parameters.AddWithValue("listing_id", newPicture.ListingId);
                        cmd.Parameters.AddWithValue("image_url", relativePath);
                        cmd.Parameters.AddWithValue("image_description", newPicture.ImageDescription);
                        await cmd.ExecuteNonQueryAsync();

                        lastInsertedId = cmd.LastInsertedId;
                    }
                }
            }


            return lastInsertedId;
        }

        [HttpGet("delete-listing-picture/{imageId}")]
        public async Task<bool> DeletePicture(int imageId)
        {
            var isSuccessful = false;
            using (var connection = GetOCHDbConnection())
            {
                await connection.OpenAsync();
                ListingPicture listingPic = null;
                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandText = $"SELECT * from {OCHDb.Tables.ListingImages} WHERE image_id = @Id";
                    cmd.Parameters.AddWithValue("Id", imageId);
                    using (var reader = await cmd.ExecuteReaderAsync())
                        while (await reader.ReadAsync())
                        {
                            listingPic = ListingPicture.PopulateFromRecord(reader);
                        }
                }

                if (listingPic != null)
                {
                    var absolutePath = $"{_environment.WebRootPath}/{listingPic.ImageUrl}";

                    if (System.IO.File.Exists(absolutePath))
                    {
                        _logger.LogWarning($"Deleting image with id {listingPic.ImageId} at path ({absolutePath}");
                        System.IO.File.Delete(absolutePath);
                    }
                }

                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandText = $"DELETE from {OCHDb.Tables.ListingImages} WHERE image_id = @Id";
                    cmd.Parameters.AddWithValue("Id", imageId);
                    var retCode = await cmd.ExecuteNonQueryAsync();
                    if (retCode > 0)
                        isSuccessful = true;
                }
            }

            return isSuccessful;
        }
    }
}