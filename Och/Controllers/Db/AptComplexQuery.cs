using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using Och.Models.Db;

namespace Och.Controllers.Db
{
    [Route("api/listing")]
    public class AptComplexQuery : Controller
    {
        private readonly IHostingEnvironment _environment;
        private readonly ILogger _logger;
        private ImageQuery imageQuery;
        private LocationQuery locationQuery;
        private ContactQuery contactQuery;
        private readonly ListingQuery listingQuery;


        public AptComplexQuery(IHostingEnvironment environment,
            ILogger<AptComplexQuery> logger,
            ImageQuery iQ,
            LocationQuery lQ,
            ContactQuery cQ,
            ListingQuery liQ)
        {
            _environment = environment;
            _logger = logger;
            imageQuery = iQ;
            locationQuery = lQ;
            contactQuery = cQ;
            listingQuery = liQ;
        }

        private MySqlConnection GetOCHDbConnection()
        {
            // See if we can use the connection string stored in the appsettings.json file
            // https://mysql-net.github.io/MySqlConnector/tutorials/net-core-mvc/
            const string connectionString =
                "Server=localhost;Port=3306;User ID=dipack;Password=dipackiscool;Database=och_web_1;";
            var connection = (new OCHDb(connectionString)).Connection;
            return connection;
        }

        [HttpGet("get-all-apt-complex")]
        public async Task<List<ComplexApt>> GetAllAptComplexes()
        {
            _logger.LogInformation("Fetching all apartment complexes");
            var complexApts = new List<ComplexApt>();
            using (var connection = GetOCHDbConnection())
            {
                await connection.OpenAsync();
                using (var cmd = new MySqlCommand($"SELECT * from {OCHDb.Tables.ApartmentComplexes}", connection))
                {
                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            var complex = ApartmentComplex.PopulateFromRecord(reader);
                            var complexListing = new ComplexApt
                            {
                                AptComplex = complex,
                                ListingLocation =
                                    await locationQuery.GetListingLocation(complex.ListingId.GetValueOrDefault(-1)),
                                ListingPictures =
                                    await imageQuery.GetListingPictures(complex.ListingId.GetValueOrDefault(-1)),
                                ListingContact =
                                    await contactQuery.GetContact(complex.ContactDetailsId.GetValueOrDefault(-1))
                            };
                            complexApts.Add(complexListing);
                        }
                    }
                }
            }

            return complexApts;
        }

        [HttpPost("update-apt-complex")]
        public async Task<bool> UpdateListing([FromBody] ApartmentComplex updatedListing)
        {
            if (!ModelState.IsValid)
            {
                throw new InvalidCastException("Malformed model formed by mapping POST Request");
            }

            _logger.LogInformation($"Updating apartment complex with ID {updatedListing.ListingId}");

            var isSuccessful = false;

            using (var connection = GetOCHDbConnection())
            {
                await connection.OpenAsync();
                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandText =
                        $"UPDATE {OCHDb.Tables.ApartmentComplexes} SET `apartment_name` = @apartment_name, `apartment_address` = @apartment_address, `contact_details_id` = @contact_details_id, `apartment_description` = @apartment_description, `apartment_url` = @apartment_url, `display_order` = @display_order, `listing_active` = @listing_active " +
                        $"WHERE listing_id = @listing_id";
                    var upCmd = PopulateAptComplexQuery(cmd, updatedListing, true);
                    int retCode = await upCmd.ExecuteNonQueryAsync();
                    if (retCode > 0)
                        isSuccessful = true;
                }
            }

            return isSuccessful;
        }

        [HttpPost("insert-apt-complex")]
        public async Task<long> InsertAptComplex([FromBody] ApartmentComplex newComplex)
        {
            if (!ModelState.IsValid)
            {
                throw new InvalidCastException("Malformed model formed by mapping POST Request");
            }

            _logger.LogInformation("Attempting to insert new apartment complex details into DB");

            long lastInsertedId = -1;
            using (var connection = GetOCHDbConnection())
            {
                await connection.OpenAsync();
                var listingId = await listingQuery.InsertAptComplexListing(newComplex);
                if (listingId != -1)
                {
                    newComplex.ListingId = (int) listingId;
                    using (var cmd = new MySqlCommand())
                    {
                        cmd.Connection = connection;
                        cmd.CommandText =
                            $"INSERT INTO {OCHDb.Tables.ApartmentComplexes} (`listing_id`, `apartment_name`,`apartment_address`,`contact_details_id`,`apartment_description`,`apartment_url`,`display_order`,`listing_active`) " +
                            $"VALUES (@listing_id, @apartment_name, @apartment_address,@contact_details_id,@apartment_description,@apartment_url,@display_order,@listing_active)";
                        var insertCmd = PopulateAptComplexQuery(cmd, newComplex, true);
                        await insertCmd.ExecuteNonQueryAsync();

                        lastInsertedId = insertCmd.LastInsertedId;

                        if (lastInsertedId == -1)
                        {
                            _logger.LogError("Failed to insert apartment complex into Apartment Complex DB");
                        }
                        else
                        {
                            // We need to return the listing id as that is the ultimate ID
                            lastInsertedId = listingId;
                        }
                    }
                }
                else
                {
                    _logger.LogError("Failed to insert Apartment Complex listing into Listings table as a pre-req");
                }
            }

            return lastInsertedId;
        }

        [HttpGet("get-apt-complex/{listingId}")]
        public async Task<ApartmentComplex> GetAptComplex(int listingId)
        {
            using (var connection = GetOCHDbConnection())
            {
                await connection.OpenAsync();
                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandText = $"SELECT * from {OCHDb.Tables.ApartmentComplexes} WHERE listing_id = @Id";
                    cmd.Parameters.AddWithValue("Id", listingId);
                    using (var reader = await cmd.ExecuteReaderAsync())
                        while (await reader.ReadAsync())
                        {
                            return ApartmentComplex.PopulateFromRecord(reader);
                        }
                }
            }

            return null;
        }

        [HttpGet("delete-apt-complex/{listingId}")]
        public async Task<bool> DeleteAptComplex(int listingId)
        {
            var isAptComplexDeleteSuccessful = false;
            var isListingDeleteSuccessful = false;
            using (var connection = GetOCHDbConnection())
            {
                await connection.OpenAsync();
                isListingDeleteSuccessful = await listingQuery.DeleteListing(listingId);
                if (!isListingDeleteSuccessful)
                {
                    _logger.LogError("Failed to delete Apartment Complex from Listings table");
                }

                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandText = $"DELETE from {OCHDb.Tables.ApartmentComplexes} WHERE listing_id = @Id";
                    cmd.Parameters.AddWithValue("Id", listingId);
                    var retCode = await cmd.ExecuteNonQueryAsync();
                    if (retCode > 0)
                        isAptComplexDeleteSuccessful = true;
                }
            }

            return isAptComplexDeleteSuccessful && isListingDeleteSuccessful;
        }

        private static MySqlCommand PopulateAptComplexQuery(MySqlCommand refCmd, ApartmentComplex listing,
            bool useListingId = false)
        {
            var cmd = new MySqlCommand
            {
                Connection = refCmd.Connection,
                CommandText = refCmd.CommandText
            };

            if (useListingId)
            {
                cmd.Parameters.AddWithValue("listing_id", listing.ListingId);
            }

            cmd.Parameters.AddWithValue("apartment_name", listing.ApartmentName);
            cmd.Parameters.AddWithValue("apartment_address", listing.ApartmentAddress);
            cmd.Parameters.AddWithValue("contact_details_id", listing.ContactDetailsId);
            cmd.Parameters.AddWithValue("apartment_description", listing.ApartmentDescription);
            cmd.Parameters.AddWithValue("apartment_url", listing.ApartmentUrl);
            cmd.Parameters.AddWithValue("display_order", listing.DisplayOrder);
            cmd.Parameters.AddWithValue("listing_active", listing.ListingActive);

            return cmd;
        }
    }
}