using System;
using MySql.Data.MySqlClient;

namespace Och.Controllers.Db
{
    public class OCHDb : IDisposable
    {
        public readonly MySqlConnection Connection;

        public static class Tables
        {
            public const string Listings = "listings";
            public const string ApartmentComplexes = "apartment_complexes";
            public const string ListingContacts = "listing_contacts";
            public const string ListingImages = "listing_images";
            public const string ListingLocations = "listing_locations";
            public const string Localities = "localities";
            public const string UnitTypes = "unit_types";
        }

        public OCHDb(string connectionString)
        {
            Connection = new MySqlConnection(connectionString);
        }

        public void Dispose()
        {
            Connection.Close();
        }
    }
}