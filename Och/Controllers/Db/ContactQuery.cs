using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using Och.Models.Db;

namespace Och.Controllers.Db
{
    [Route("api/listing")]
    public class ContactQuery : Controller
    {
        private readonly ILogger _logger;

        public ContactQuery(ILogger<ContactQuery> logger)
        {
            _logger = logger;
        }

        private MySqlConnection GetOCHDbConnection()
        {
            // See if we can use the connection string stored in the appsettings.json file
            // https://mysql-net.github.io/MySqlConnector/tutorials/net-core-mvc/
            const string connectionString =
                "Server=localhost;Port=3306;User ID=dipack;Password=dipackiscool;Database=och_web_1;";
            var connection = (new OCHDb(connectionString)).Connection;
            return connection;
        }

        [HttpGet("get-all-contacts")]
        public async Task<List<ListingContact>> GetAllContacts()
        {
            var contacts = new List<ListingContact>();
            using (var connection = GetOCHDbConnection())
            {
                await connection.OpenAsync();
                using (var cmd = new MySqlCommand($"SELECT * from {OCHDb.Tables.ListingContacts}", connection))
                {
                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            var contact = ListingContact.PopulateFromRecord(reader);
                            contacts.Add(contact);
                        }
                    }
                }
            }

            return contacts;
        }

        [HttpGet("get-contact/{contactId}")]
        public async Task<ListingContact> GetContact(int contactId)
        {
            using (var connection = GetOCHDbConnection())
            {
                await connection.OpenAsync();
                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandText = $"SELECT * from {OCHDb.Tables.ListingContacts} WHERE contact_id = @Id";
                    cmd.Parameters.AddWithValue("Id", contactId);
                    using (var reader = await cmd.ExecuteReaderAsync())
                        while (await reader.ReadAsync())
                        {
                            return ListingContact.PopulateFromRecord(reader);
                        }
                }
            }

            return new ListingContact();
        }

        [HttpPost("update-contact")]
        public async Task<bool> UpdateContact([FromBody] ListingContact updatedContact)
        {
            if (!ModelState.IsValid)
            {
                throw new InvalidCastException("Malformed model formed by mapping POST Request");
            }

            var isSuccessful = false;

            using (var connection = GetOCHDbConnection())
            {
                await connection.OpenAsync();
                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandText =
                        $"UPDATE {OCHDb.Tables.ListingContacts} SET name = @name, primary_phone = @primary_phone, secondary_phone = @secondary_phone, email = @email " +
                        $"WHERE contact_id = @contact_id";
                    cmd.Parameters.AddWithValue("contact_id", updatedContact.ContactId);
                    cmd.Parameters.AddWithValue("name", updatedContact.Name);
                    cmd.Parameters.AddWithValue("primary_phone", updatedContact.PrimaryPhone);
                    cmd.Parameters.AddWithValue("secondary_phone", updatedContact.SecondaryPhone);
                    cmd.Parameters.AddWithValue("email", updatedContact.Email);
                    int retCode = await cmd.ExecuteNonQueryAsync();
                    if (retCode > 0)
                        isSuccessful = true;
                }
            }

            return isSuccessful;
        }

        [HttpPost("insert-contact")]
        public async Task<long> InsertContact([FromBody] ListingContact newContact)
        {
            if (!ModelState.IsValid)
            {
                throw new InvalidCastException("Malformed model formed by mapping POST Request");
            }

            long lastInsertedId = -1;
            using (var connection = GetOCHDbConnection())
            {
                await connection.OpenAsync();
                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandText =
                        $"INSERT INTO {OCHDb.Tables.ListingContacts} (`name`, `primary_phone`, `secondary_phone`, `email`) VALUES (@name, @primary_phone, @secondary_phone, @email) ";
                    cmd.Parameters.AddWithValue("name", newContact.Name);
                    cmd.Parameters.AddWithValue("primary_phone", newContact.PrimaryPhone);
                    cmd.Parameters.AddWithValue("secondary_phone", newContact.SecondaryPhone);
                    cmd.Parameters.AddWithValue("email", newContact.Email);
                    await cmd.ExecuteNonQueryAsync();

                    lastInsertedId = cmd.LastInsertedId;
                }
            }

            return lastInsertedId;
        }

        [HttpGet("delete-contact/{contactId}")]
        public async Task<bool> DeleteContact(int contactId)
        {
            var isSuccessful = false;
            using (var connection = GetOCHDbConnection())
            {
                await connection.OpenAsync();
                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandText = $"DELETE from {OCHDb.Tables.ListingContacts} WHERE contact_id = @Id";
                    cmd.Parameters.AddWithValue("Id", contactId);
                    var retCode = await cmd.ExecuteNonQueryAsync();
                    if (retCode > 0)
                        isSuccessful = true;
                }
            }

            return isSuccessful;
        }
    }
}