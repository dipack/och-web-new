using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using Och.Controllers.Db;
using Och.Models.Crypto;
using Och.Models.Db;

namespace Och.Controllers.Sample
{
    [Route("api/[controller]")]
    public class OneLevelDeep : Controller
    {
        [HttpGet("[action]")]
        public string HelloWorld()
        {
            return "Hello world!";
        }

        public MySqlConnection GetOCHDbConnection()
        {
            const string connection_string =
                "Server=localhost;Port=3306;User ID=dipack;Password=dipackiscool;Database=mydb;";
            var connection = (new OCHDb(connection_string)).Connection;
            return connection;
        }

        [HttpPost("[action]")]
        public async Task<Listing[]> Search([FromBody] Listing value)
        {
            if (!ModelState.IsValid)
            {
                Console.WriteLine("Malformed model formed by mapping POST Request");
            }

            var password = "IamCool";
            var samplePasswordHash = UserAuthentication.HashAndSaltPassword(password);
            var isEqual = UserAuthentication.ValidatePassword(samplePasswordHash, password);
            Console.WriteLine($"Are the password equal? {isEqual}");

            var listings = new List<Listing>();
            using (var connection = GetOCHDbConnection())
            {
                await connection.OpenAsync();
                using (var cmd = new MySqlCommand("SELECT * from listings", connection))
                {
                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            var listing = Listing.PopulateFromRecord(reader);
                            if (listing.ToString().IndexOf(value.StreetName, StringComparison.OrdinalIgnoreCase) >= 0)
                                listings.Add(listing);
                            // After done searching for exact matches, add records with asc. L distance
                            Console.WriteLine(
                                $"L Distance is: {Util.LevenshteinDistance(listing.ToString(), value.ToString())}");
                        }
                    }
                }
            }

            return listings.ToArray();
        }

        [HttpGet("[action]")]
        public async Task<string> GetListings()
        {
            // See if we can use the connection string stored in the appsettings.json file
            // https://mysql-net.github.io/MySqlConnector/tutorials/net-core-mvc/
            var output = "";
            using (var connection = GetOCHDbConnection())
            {
                await connection.OpenAsync();
                using (var cmd = new MySqlCommand("SELECT * from listings_current", connection))
                {
                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        while (await reader.ReadAsync())
                        {
                            var listing = Listing.PopulateFromRecord(reader);
                            Console.WriteLine(listing.ToString());
                            output += listing.ToString();
                        }
                    }
                }
            }

            return output;
        }

        [HttpGet("[action]/{listingId}")]
        public async Task<ListingPicture[]> GetListingPictures(int listingId)
        {
            var listingPics = new List<ListingPicture>();
            using (var connection = GetOCHDbConnection())
            {
                await connection.OpenAsync();
                using (var cmd = new MySqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandText = "SELECT * from pictures WHERE listing_id = @Id";
                    cmd.Parameters.AddWithValue("Id", listingId);
                    using (var reader = await cmd.ExecuteReaderAsync())
                        while (await reader.ReadAsync())
                        {
                            var listingPic = new ListingPicture
                            {
                                ImageId = reader.GetInt32(0),
                                ListingId = reader.GetInt32(1),
                                ImageUrl = reader.GetString(2),
                                ImageDescription = reader.GetString(3)
                            };
                            listingPics.Add(listingPic);
                        }
                }
            }

            return listingPics.ToArray();
        }

       
    }
}
