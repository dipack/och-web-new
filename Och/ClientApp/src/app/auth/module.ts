import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { AuthenticationService } from './authentication.service';
import { LoginComponent } from './login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthGuard } from './auth-guard';

@NgModule({

    imports: [
        CommonModule,
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule
    ],
    declarations: [
        LoginComponent
    ],
    entryComponents: [
        LoginComponent
    ],
    exports: [
        LoginComponent
    ],
    providers: [
        AuthGuard,
        AuthenticationService
    ]
})
export class AuthModule {
}
