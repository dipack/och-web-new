import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/auth/user';


@Injectable()
export class UserService {
    constructor(private http: HttpClient) {
    }

    getAll() {
        return this.http.get<Array<User>>(`api/auth/users`);
    }

    getById(id: number) {
        return this.http.get<User>(`api/auth/users/${ id }`);
    }
}
