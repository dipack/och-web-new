import * as _ from 'lodash';
import { HttpHeaders } from '@angular/common/http';
import { DateTime } from 'luxon';
import { AUTH, DATETIME_FORMAT } from './constants';
import { User } from './models/auth/user';

export class OchUtil {

    static deepDifference(object, base) {
        function changes(object, base) {
            return _.transform(object, function (result, value, key) {
                if (!_.isEqual(value, base[key])) {
                    result[key] = (_.isObject(value) && _.isObject(base[key])) ? changes(value, base[key]) : value;
                }
                // @ts-ignore
                if (key === 'listingId' || key === 'contactId') {
                    result[key] = value;
                }
            });
        }

        return changes(object, base);
    }

    static getHeaders() {
        let headers = new HttpHeaders()
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');

        const lsUser = localStorage.getItem(AUTH.LS_USER_KEY);
        if (lsUser) {
            const user = User.populateFromRecord(JSON.parse(lsUser));
            if (user && user.token) {
                headers = headers.set('Authorization', `Bearer ${ user.token }`);
            }
        }
        return headers;
    }

    static constructUrl(baseUrl: string, action: string, ...args: string[]) {
        const parts = [baseUrl.substr(0, baseUrl.length - 1), 'api', 'listing', action, ...args];
        return parts.join('/');
    }

    static formatDate(dateStr: string, format: string = DATETIME_FORMAT.FROM_DB): DateTime {
        return DateTime.fromFormat(dateStr, format, { zone: DATETIME_FORMAT.ZONE });
    }

    static generateRandomId(): number {
        return Math.ceil(50000 * Math.random()) +
            // Unix epoch in ms
            Math.floor((new Date).getTime() / 1000);
    }

    static readImageFile(file: File): Promise<string> {
        const reader = new FileReader();
        return new Promise((resolve, reject) => {
            reader.onload = (event) => {
                try {
                    const src: string = (event.target as FileReader).result as string;
                    resolve(src);
                } catch {
                    reject('');
                }
            };
            reader.readAsDataURL(file);
        })
    }

    static isWindowWidthSmallerThanDesktop(): boolean {
        const windowWidth = window.innerWidth;
        return windowWidth < 1026;
    }

    static camelPad(str: string): string {
        return str
        // Look for long acronyms and filter out the last letter
            .replace(/([A-Z]+)([A-Z][a-z])/g, ' $1 $2')
            // Look for lower-case letters followed by upper-case letters
            .replace(/([a-z\d])([A-Z])/g, '$1 $2')
            // Look for lower-case letters followed by numbers
            .replace(/([a-zA-Z])(\d)/g, '$1 $2')
            .replace(/^./, function (str) {
                return str.toUpperCase();
            })
            // Remove any white space left around the word
            .trim();
    }

}
