import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { Route, RouterModule } from '@angular/router';

import { GenerateRange } from './pipes/generate-range.pipe';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { MarkdownModule } from 'ngx-markdown';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NavMenuModule } from './nav-menu/module';
import { FooterModule } from './footer/module';
import { DisplayStaticModule } from './display-static/module';
import { DisplayStaticComponent } from './display-static/display-static.component';
import { HomeModule } from './home/module';
import { SearchResultsComponent } from './search/search-results.component';
import { SearchModule } from './search/module';
import { AgmCoreModule } from '@agm/core';
import { API_KEYS } from './api-keys';
import { AdminListingModule } from './admin/listing/module';
import { AdminListingDisplayComponent } from './admin/listing/listing-display.component';
import { OchGridModule } from './grid/module';
import { ListingModule } from './listing/module';
import { ListingDisplayComponent } from './listing/listing-display.component';
import { SpringBoardModule } from './springboard/module';
import { AdminContactModule } from './admin/contact/module';
import { AdminContactDisplayComponent } from './admin/contact/contact-display.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AdminApartmentComplexModule } from './admin/apartment-complex/module';
import { AdminAptComplexDisplayComponent } from './admin/apartment-complex/apt-complex-display.component';
import { AuthModule } from './auth/module';
import { AuthGuard } from './auth/auth-guard';
import { JwtInterceptor } from './auth/jwt.interceptor';
import { ErrorInterceptor } from './auth/error.interceptor';
import { LoginComponent } from './auth/login.component';
import { Role } from './models/auth/role';

const appRoutes: Route[] = [
    { path: '', component: HomeComponent, pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    { path: 'search', component: SearchResultsComponent },
    {
        path: 'admin/listing',
        component: AdminListingDisplayComponent,
        canActivate: [AuthGuard],
        data: { roles: [Role.Admin] }
    },
    { path: 'admin/apt-complex', component: AdminAptComplexDisplayComponent },
    { path: 'admin/contact', component: AdminContactDisplayComponent },
    { path: 'advertise', component: DisplayStaticComponent, data: { markdownFileUrl: 'pages/advertise.md' } },
    { path: 'resources', component: DisplayStaticComponent, data: { markdownFileUrl: 'pages/resources.md' } },
    { path: 'roommates', component: DisplayStaticComponent, data: { markdownFileUrl: 'pages/roommates.md' } },
    { path: 'contact-us', component: DisplayStaticComponent, data: { markdownFileUrl: 'pages/contact.md' } },
    { path: 'listing/:listingId', component: ListingDisplayComponent }
];

@NgModule({
    declarations: [
        GenerateRange,
        AppComponent
    ],
    imports: [
        BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
        HttpClientModule,
        FormsModule,
        AngularFontAwesomeModule,
        NgxSpinnerModule,
        MarkdownModule.forRoot({
            loader: HttpClient
        }),
        AgmCoreModule.forRoot({
            apiKey: API_KEYS.GMAPS_JS
        }),
        RouterModule.forRoot(appRoutes),
        AuthModule,
        HomeModule,
        NavMenuModule,
        FooterModule,
        DisplayStaticModule,
        SearchModule,
        AdminListingModule,
        AdminApartmentComplexModule,
        AdminContactModule,
        OchGridModule,
        ListingModule,
        SpringBoardModule
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {
}
