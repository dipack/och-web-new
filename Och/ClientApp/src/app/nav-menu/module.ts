import { NgModule } from '@angular/core';
import { NavMenuComponent } from './nav-menu.component';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    imports: [
        RouterModule,
        NgbModule
    ],
    declarations: [
        NavMenuComponent
    ],
    exports: [
        NavMenuComponent
    ]
})
export class NavMenuModule {
}
