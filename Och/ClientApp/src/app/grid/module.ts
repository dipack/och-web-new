import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NumericEditor } from './numeric-editor.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule
    ],
    declarations: [
        NumericEditor
    ],
    exports: [
        NumericEditor
    ]
})
export class OchGridModule {}
