import { ValueParserParams } from 'ag-grid-community';

export class OchGridUtil {

    static NumericValueParser(params: ValueParserParams) {
        return Number(params.newValue);
    }

    static BooleanValueParser(params: ValueParserParams) {
        const trueStr = String(true);
        const falseStr = String(false);
        let retVal = params.newValue;
        if (trueStr.indexOf(retVal) === 0) {
            retVal = true;
        } else if (falseStr.indexOf(retVal) === 0) {
            retVal = false;
        } else {
            retVal = params.oldValue;
        }
        return retVal;
    }
}
