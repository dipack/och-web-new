import { NgModule } from "@angular/core";
import { FooterComponent } from "./footer.component";
import { MarkdownModule } from "ngx-markdown";

@NgModule({
    imports: [
        MarkdownModule.forChild()
    ],
    declarations: [
        FooterComponent
    ],
    exports: [
        FooterComponent
    ]
})
export class FooterModule {}
