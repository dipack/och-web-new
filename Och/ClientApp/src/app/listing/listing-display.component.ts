import * as _ from 'lodash';
import { Component, Input, OnInit } from '@angular/core';
import { SearchService } from '../search/search.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ComplexListing } from '../models/complex-listing';
import { SpringboardService } from '../springboard/springboard.service';
import { DateTime } from 'luxon';
import { OchUtil } from '../util';
import { NgxSpinnerService } from 'ngx-spinner';
import { Listing } from '../models/listing';

@Component({
    selector: 'app-listing-display',
    templateUrl: 'listing-display.component.html',
    styleUrls: [
        'listing-display.component.css'
    ]
})
export class ListingDisplayComponent implements OnInit {
    @Input()
    public listingId: number = -1;

    public listing: ComplexListing;

    constructor(
        readonly route: ActivatedRoute,
        readonly spinner: NgxSpinnerService,
        readonly searchService: SearchService,
        readonly springboardService: SpringboardService
    ) {

    }

    ngOnInit(): void {
        this.spinner.show();
        this.route.paramMap.subscribe((params: ParamMap) => {
            if (this.listingId === -1) {
                this.listingId = Number(params.get('listingId')) || -1;
                this.fetchListing(this.listingId).then(() => {
                    this.spinner.hide();
                })
            } else {
                this.spinner.hide();
            }
        })
    }

    fetchListing(listingId: number) {
        return this.searchService.getListing(listingId)
            .then((complexListing: ComplexListing) => {
                this.listing = complexListing;
            })
            .catch((reason: any) => {
                console.error(`Failed to fetch listing with id ${ listingId }`, reason);
                this.listing = new ComplexListing(undefined, [], undefined, undefined);
            })
    }

    formatListingActive(listing: ComplexListing): string {
        if (listing.listing.listingActive) {
            return `Active`;
        } else {
            return `Inactive`;
        }
    }

    formatAddress(listing: ComplexListing): string {
        const l = listing.listing;
        const locality = _.find(this.springboardService.Localities, { postalCode: l.postalCode }) || this.springboardService.Localities[0];
        return `${ l.streetNumber } ${ l.streetName }, ${ locality.name } - ${ l.postalCode }`;
    }

    formatAddressMapLink(listing: ComplexListing): string {
        const l: Listing = listing.listing;
        const mapUrl = 'https://maps.google.com/maps?q=';
        const address = `${ l.streetNumber } ${ l.streetName } ${ l.postalCode }`;
        return `${ mapUrl }${ encodeURIComponent(address) }`;
    }

    formatUnitType(listing: ComplexListing): string {
        const u = listing.listing.unitType;
        const unit = _.find(this.springboardService.UnitTypes, { unitTypeId: u });
        if (unit) {
            return `${ unit.unitTypeName }`;
        } else {
            return `Unknown`;
        }
    }

    formatBedrooms(listing: ComplexListing): string {
        const open = listing.listing.bedroomsAvailable;
        const total = listing.listing.bedroomsTotal;
        return `${ open || '?' }/${ total || '?' }`;
    }

    formatAmounts(listing: ComplexListing): string {
        const rent = listing.listing.amtRent || 'Unknown';
        const security = listing.listing.amtSecurity || 'Unknown';
        return `$${ rent } per month/$${ security }`;
    }

    formatDate(dateStr: string): string {
        const d = DateTime.fromISO(dateStr);
        return d.toLocaleString(DateTime.DATE_HUGE);
    }

    formatAmenities(listing: ComplexListing): any[] {
        return _.map(listing.listing, (value: any, key: string) => {
            if (key.startsWith('amenity')) {
                const prop = key.substr('amenity'.length);
                const friendlyProp = OchUtil.camelPad(prop);
                return { amenity: friendlyProp, value: value };
            } else if (_.includes(['rulesPetsAllowed', 'bedroomsLocatedBasementAttic', 'verifiedProperty', 'certificateOfOccupancy', 'registereRentalRegistry'], key)) {
                const friendlyProp = OchUtil.camelPad(key);
                return { amenity: friendlyProp, value: value };
            }
        }).filter(_ => _ !== undefined);
    }

    formatEmailHref(email: string): string {
        return `mailto:${ email }`;
    }
}
