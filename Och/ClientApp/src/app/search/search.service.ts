import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Listing } from '../models/listing';
import { ComplexListing } from '../models/complex-listing';
import { ListingPicture } from '../models/listing-picture';
import { Observable } from 'rxjs';
import { OchUtil } from '../util';
import { ListingContact } from '../models/listing-contact';
import { LAT_LON_INVALID_VALUE, LatLon, MapService } from './map.service';
import { DateTime } from 'luxon';
import { ENDPOINTS } from '../constants';
import { ListingLocation } from '../models/listing-location';
import { ComplexApt } from '../models/complex-apt';
import { ApartmentComplex } from '../models/apartment-complex';

@Injectable()
export class SearchService {
    public searchResults: ComplexListing[] = [];

    constructor(readonly http: HttpClient,
                readonly mapService: MapService,
                @Inject('BASE_URL') readonly baseUrl: string) {
    }

    constructUrl(action: string, ...args: string[]) {
        return OchUtil.constructUrl(this.baseUrl, action, ...args);
    }

    private getHeaders() {
        return OchUtil.getHeaders();
    }

    updateListing(listing: ComplexListing): Promise<boolean> {
        const headers = this.getHeaders();
        const body = JSON.stringify(listing.listing);
        return this.http.post<boolean>(this.constructUrl(ENDPOINTS.LISTING.UPDATE), body, { headers: headers })
            .toPromise()
            // Not offering a mechanism to update listing pictures,
            // instead, just delete and re-up for now
            .then(() => {
                // Update location
                return this.updateLocation(listing.listingLocation).toPromise();
            })
            .catch(() => Promise.reject(false));
    };

    updateAptComplex(complex: ComplexApt): Promise<boolean> {
        const headers = this.getHeaders();
        const body = JSON.stringify(complex.aptComplex);
        return this.http.post<boolean>(this.constructUrl(ENDPOINTS.APT_COMPLEX.UPDATE), body, { headers: headers })
            .toPromise()
            // Not offering a mechanism to update listing pictures,
            // instead, just delete and re-up for now
            .then(() => {
                // Update location
                return this.updateLocation(complex.listingLocation).toPromise();
            })
            .catch(() => Promise.reject(false));
    };

    deleteListing(listingId: number): Observable<boolean> {
        const header = this.getHeaders();
        return this.http.get<boolean>(
            this.constructUrl(ENDPOINTS.LISTING.DELETE, listingId.toString()),
            { headers: header }
        );
    }

    deleteAptComplex(complexListingId: number): Observable<boolean> {
        const header = this.getHeaders();
        return this.http.get<boolean>(
            this.constructUrl(ENDPOINTS.APT_COMPLEX.DELETE, complexListingId.toString()),
            { headers: header }
        );
    }

    // updatePicture(picture: ListingPicture): Observable<boolean> {
    //     const headers = this.getHeaders();
    //     const body = JSON.stringify(picture);
    //     return this.http.post<boolean>(
    //         this.constructUrl(ENDPOINTS.PICTURE.UPDATE),
    //         body,
    //         { headers: headers }
    //     );
    // }

    deletePicture(imageId: number): Observable<boolean> {
        const header = this.getHeaders();
        return this.http.get<boolean>(
            this.constructUrl(ENDPOINTS.PICTURE.DELETE, imageId.toString()),
            { headers: header }
        );
    }

    updateContact(contact: ListingContact): Observable<boolean> {
        const headers = this.getHeaders();
        const body = JSON.stringify(contact);
        return this.http.post<boolean>(
            this.constructUrl(ENDPOINTS.CONTACT.UPDATE),
            body,
            { headers: headers }
        );
    }

    deleteContact(contactId: number): Observable<boolean> {
        const header = this.getHeaders();
        return this.http.get<boolean>(
            this.constructUrl(ENDPOINTS.CONTACT.DELETE, contactId.toString()),
            { headers: header }
        );
    }

    updateLocation(location: ListingLocation): Observable<boolean> {
        const headers = this.getHeaders();
        const body = JSON.stringify(location);
        return this.http.post<boolean>(
            this.constructUrl(ENDPOINTS.LOCATION.UPDATE),
            body,
            { headers: headers }
        );
    }

    deleteLocation(listingId: number): Observable<boolean> {
        const header = this.getHeaders();
        return this.http.get<boolean>(
            this.constructUrl(ENDPOINTS.LOCATION.DELETE, listingId.toString()),
            { headers: header }
        );
    }

    private searchGetListing(listing: Listing, getAll: boolean = false): Observable<Array<ComplexListing>> {
        const headers = this.getHeaders();
        const obj = Object.assign({}, listing);
        const objKeys = Object.keys(obj);
        const queryParams = objKeys.map(key => {
            let escaped = '';
            if (key.toLowerCase().indexOf('date') > -1) {
                const isoDate = DateTime.fromISO(obj[key]).toISODate();
                escaped = encodeURIComponent(isoDate);
            } else {
                escaped = encodeURIComponent(obj[key]);
            }

            return `${ key }=${ escaped }`;
        }).concat(`getAll=${ getAll }`);
        const queryString = queryParams.join('&');
        return this.http.get<Array<ComplexListing>>(
            this.constructUrl(ENDPOINTS.LISTING.SEARCH) + `?${ queryString }`,
            { headers: headers }
        );
    }

    searchForSimilar(listing: Listing, getAll: boolean = false): Promise<Array<ComplexListing>> {
        return this.searchGetListing(listing, getAll)
            .toPromise()
            .then((listingResults: Array<ComplexListing>) => {
                return listingResults.map(r => ComplexListing.populateFromRecord(r));
            })
            .catch((e) => {
                console.error(`Error in search for listing like`, listing);
                console.error(e);
                return e;
            });
    }

    getListing(listingId: number): Promise<ComplexListing> {
        const complexListing = new ComplexListing(new Listing(), [], undefined, undefined);
        const headers = this.getHeaders();
        return new Promise((resolve, reject) => {
            this.http.get(this.constructUrl(ENDPOINTS.LISTING.GET_ONE, listingId.toString()), { headers: headers }).toPromise()
                .then((listing: Listing) => {
                    complexListing.listing = listing;
                })
                .then(() => {
                    return this.getImages(listingId).toPromise();
                })
                .then((listingImages: ListingPicture[]) => {
                    complexListing.listingPictures = listingImages;
                    return this.getLocation(listingId).toPromise();
                })
                .then((location: ListingLocation) => {
                    complexListing.listingLocation = ListingLocation.populateFromRecord(location);
                    return this.getContact(complexListing.listing.contactDetailsId).toPromise();
                })
                .then((contact: ListingContact) => {
                    complexListing.listingContact = contact;
                    return resolve(complexListing);
                })
                .catch((e) => {
                    console.error(`Error in search for listing ${ listingId }`, e);
                    reject(e);
                })
        });

    }

    getAllListings(): Promise<Array<ComplexListing>> {
        const headers = this.getHeaders();
        return new Promise((resolve, reject) => {
            this.http.get<Array<ComplexListing>>(this.constructUrl(ENDPOINTS.LISTING.GET_ALL), { headers: headers }).toPromise()
                .then((listings: Array<ComplexListing>) => {
                    return resolve(listings);
                })
                .catch((e) => {
                    console.error(`Error in fetching all listings`, e);
                    reject(e);
                })
        });
    }

    getAllAptComplexes(): Promise<Array<ComplexApt>> {
        const headers = this.getHeaders();
        return new Promise((resolve, reject) => {
            this.http.get<Array<ComplexApt>>(this.constructUrl(ENDPOINTS.APT_COMPLEX.GET_ALL), { headers: headers }).toPromise()
                .then((complexes: Array<ComplexApt>) => {
                    return resolve(complexes);
                })
                .catch((e) => {
                    console.error(`Error in fetching all apartment complexes`, e);
                    reject(e);
                })
        });
    }

    getAllContacts(): Promise<Array<ListingContact>> {
        const headers = this.getHeaders();
        return new Promise((resolve, reject) => {
            this.http.get<Array<ListingContact>>(this.constructUrl(ENDPOINTS.CONTACT.GET_ALL), { headers: headers }).toPromise()
                .then((contacts: Array<ListingContact>) => {
                    return resolve(contacts);
                })
                .catch((e) => {
                    console.error(`Error in fetching all contacts`, e);
                    reject(e);
                })
        });
    }

    addNewListing(listing: ComplexListing): Promise<boolean> {
        // Do not add contact - as that has a separate flow for it,
        // we do not add existing contacts to prevent duplication
        // Only need to add listing details, pictures, and location,
        // as those are unique for each listing
        let actualListingId = listing.listing.listingId;
        return this.insertListing(listing.listing).toPromise()
            .then((listingId: number) => {
                actualListingId = listingId;
                return Promise.all(listing.listingPictures.map((pic: ListingPicture) => {
                    const p = ListingPicture.populateFromRecord({
                        ...pic,
                        // When creating a new listing, and adding pics to it
                        // we use the temp Id generated at runtime
                        // This obviously needs to be updated to reflect the assigned listingId
                        listingId: actualListingId
                    });
                    return this.insertImage(p).toPromise();
                }))
            })
            .then(() => {
                if (listing.listingLocation.latitude === LAT_LON_INVALID_VALUE || listing.listingLocation.longitude === LAT_LON_INVALID_VALUE) {
                    return this.mapService.geocodePlace(listing.listing)
                        .then((location: LatLon) => {
                            return this.insertLocation(actualListingId, location).toPromise();
                        });
                } else {
                    return this.insertLocation(actualListingId, listing.listingLocation).toPromise();
                }
            })
            .then(() => true)
            .catch(() => false);
    }

    addNewAptComplex(complex: ComplexApt): Promise<boolean> {
        // Do not add contact - as that has a separate flow for it,
        // we do not add existing contacts to prevent duplication
        // Only need to add listing details, pictures, and location,
        // as those are unique for each listing
        let actualListingId = complex.aptComplex.listingId;
        return this.insertAptComplex(complex.aptComplex).toPromise()
            .then((listingId: number) => {
                actualListingId = listingId;
                return Promise.all(complex.listingPictures.map((pic: ListingPicture) => {
                    const p = ListingPicture.populateFromRecord({
                        ...pic,
                        // When creating a new listing, and adding pics to it
                        // we use the temp Id generated at runtime
                        // This obviously needs to be updated to reflect the assigned listingId
                        listingId: actualListingId
                    });
                    return this.insertImage(p).toPromise();
                }))
            })
            .then(() => {
                if (complex.listingLocation.latitude === LAT_LON_INVALID_VALUE || complex.listingLocation.longitude === LAT_LON_INVALID_VALUE) {
                    return this.mapService.geocodeAddress(complex.aptComplex.apartmentAddress)
                        .then((location: LatLon) => {
                            return this.insertLocation(actualListingId, location).toPromise();
                        });
                } else {
                    return this.insertLocation(actualListingId, complex.listingLocation).toPromise();
                }
            })
            .then(() => true)
            .catch(() => false);
    }

    private insertListing(listing: Listing): Observable<number> {
        const headers = this.getHeaders();
        const body = JSON.stringify(listing);
        return this.http.post<number>(
            this.constructUrl(ENDPOINTS.LISTING.INSERT),
            body,
            { headers: headers }
        );
    }

    private insertAptComplex(aptComplex: ApartmentComplex): Observable<number> {
        const headers = this.getHeaders();
        const body = JSON.stringify(aptComplex);
        return this.http.post<number>(
            this.constructUrl(ENDPOINTS.APT_COMPLEX.INSERT),
            body,
            { headers: headers }
        );
    }

    insertContact(contact: ListingContact): Observable<number> {
        const headers = this.getHeaders();
        const body = JSON.stringify(contact);
        return this.http.post<number>(
            this.constructUrl(ENDPOINTS.CONTACT.INSERT),
            body,
            { headers: headers }
        );
    }

    insertLocation(listingId: number, location: LatLon): Observable<number> {
        const headers = this.getHeaders();
        const body = JSON.stringify({ listingId: listingId, ...location });
        return this.http.post<number>(
            this.constructUrl(ENDPOINTS.LOCATION.INSERT),
            body,
            { headers: headers }
        );
    }

    insertImage(image: ListingPicture): Observable<number> {
        const headers = this.getHeaders();
        const body = JSON.stringify(image);
        return this.http.post<number>(
            this.constructUrl(ENDPOINTS.PICTURE.INSERT),
            body,
            { headers: headers }
        );
    }

    getImages(listingId: number): Observable<Array<ListingPicture>> {
        return this.http.get<Array<ListingPicture>>(
            this.constructUrl(ENDPOINTS.PICTURE.GET_ALL_FOR_ONE, listingId.toString())
        );
    }

    getLocation(listingId: number) {
        return this.http.get(
            this.constructUrl(ENDPOINTS.LOCATION.GET_ONE, listingId.toString())
        );
    }

    getContact(contactId: number) {
        return this.http.get(
            this.constructUrl(ENDPOINTS.CONTACT.GET_ONE, contactId.toString())
        );
    }
}
