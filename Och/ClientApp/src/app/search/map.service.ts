import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API_KEYS } from '../api-keys';
import { Listing } from '../models/listing';
import { GGeocodeResult, GoogleGeocodeResponse } from '../models/google';

export interface LatLon {
    latitude: number;
    longitude: number;
}

export const LAT_LON_INVALID_VALUE = -999;

@Injectable()
export class MapService {
    constructor(readonly http: HttpClient) {

    }

    geocodePlace(listing: Listing): Promise<LatLon> {
        const l: Listing = listing;
        const address = encodeURIComponent(`${ l.streetNumber } ${ l.streetName } ${ l.postalCode }`);
        return this.geocodeAddress(address);
    }

    geocodeAddress(address: string): Promise<LatLon> {

        const apiKey = API_KEYS.GMAPS_JS;
        const geocodeUrl = `https://maps.googleapis.com/maps/api/geocode/json?address=${ address }&key=${ apiKey }`;
        return this.http.get<GoogleGeocodeResponse>(geocodeUrl)
            .toPromise()
            .then((response: GoogleGeocodeResponse) => {
                if (response.results.length > 0) {
                    const head: GGeocodeResult = response.results[0];
                    return { latitude: head.geometry.location.lat, longitude: head.geometry.location.lng };
                } else {
                    return { latitude: 43.005, longitude: -78.788 };
                }
            })
    }

}
