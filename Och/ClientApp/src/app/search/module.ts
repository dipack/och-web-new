import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { SearchResultsComponent } from './search-results.component';
import { SearchService } from './search.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SearchResultsMapComponent } from './search-results-map.component';
import { AgmCoreModule } from '@agm/core';
import { AgGridModule } from 'ag-grid-angular';
import { MapService } from './map.service';

@NgModule({
    imports: [
        HttpClientModule,
        FormsModule,
        CommonModule,
        NgbModule,
        AgmCoreModule,
        AgGridModule.withComponents([])
    ],
    declarations: [
        SearchResultsComponent,
        SearchResultsMapComponent
    ],
    exports: [
        SearchResultsComponent
    ],
    providers: [
        SearchService,
        MapService
    ]
})
export class SearchModule {
}
