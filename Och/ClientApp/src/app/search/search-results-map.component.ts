import * as _ from 'lodash';
import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { SearchService } from './search.service';
import { AgmMap } from '@agm/core/directives/map';
import { ComplexListing } from '../models/complex-listing';

@Component({
    selector: 'app-search-results-map',
    templateUrl: 'search-results-map.component.html',
    styleUrls: [
        'search-results.component.css'
    ]
})
export class SearchResultsMapComponent implements OnInit, OnChanges {
    @ViewChild('searchResultsMap')
    private searchResultsMap: AgmMap;

    @Input()
    public hoverOverListingId: number;

    private UBNorthCoords = {
        lat: 43.004318,
        lon: -78.789026
    };

    public MapLat: number = this.UBNorthCoords.lat;
    public MapLon: number = this.UBNorthCoords.lon;

    constructor(readonly searchService: SearchService) {
    }

    ngOnInit(): void {
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.hoverOverListingId.currentValue &&
            changes.hoverOverListingId.currentValue !== changes.hoverOverListingId.previousValue) {
            const listing = _.find(this.searchService.searchResults, (res: ComplexListing) => {
                return res.listing.listingId === this.hoverOverListingId;
            });
            if (listing) {
                this.MapLat = listing.listingLocation.latitude;
                this.MapLon = listing.listingLocation.longitude;
            }
        }
    }

    // Todo: Switch to one unified util for formatting purposes
    formatAddress(listing: ComplexListing): string {
        const l = listing.listing;
        return `${ l.streetNumber } ${ l.streetName }, ${ l.postalCode }`
    }
}
