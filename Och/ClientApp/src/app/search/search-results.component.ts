import * as _ from 'lodash';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { SearchService as ListingSearchService } from './search.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ComplexListing } from '../models/complex-listing';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Listing } from '../models/listing';
import { SearchQueryParams } from '../home/home.component';
import { ListingModifyComponent } from '../admin/listing/listing-modify.component';
import { SpringboardService } from '../springboard/springboard.service';
import { UnitType } from '../models/unit-type';
import { OchUtil } from '../util';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    encapsulation: ViewEncapsulation.None,
    selector: 'app-search-results',
    templateUrl: 'search-results.component.html',
    styleUrls: [
        'search-results.component.css'
    ]
})
export class SearchResultsComponent implements OnInit {
    public resultListings: ComplexListing[] = [];
    public hoverOverListingId: number = -1;

    public debouncedListingMouseEnter = _.debounce(this.onListingMouseEnter, 400, { trailing: true });

    constructor(
        readonly router: Router,
        readonly route: ActivatedRoute,
        readonly modalService: NgbModal,
        readonly spinner: NgxSpinnerService,
        readonly springboardService: SpringboardService,
        readonly searchService: ListingSearchService) {
    }

    isWindowWidthSmallerThanDesktop(): boolean {
        return OchUtil.isWindowWidthSmallerThanDesktop();
    }

    ngOnInit(): void {
        this.spinner.show();
        this.route.queryParamMap.subscribe((params: any) => {
            if (!_.isEmpty((params.params))) {
                const listing = this.formatAmenities(params.params as SearchQueryParams);
                const getAll = (params.params as SearchQueryParams).getAll || false;
                console.log('Looking for all listings?', getAll);
                console.log('Listing to search for:', listing);

                return this.searchService.searchForSimilar(listing, getAll)
                    .then((cls: Array<ComplexListing>) => {
                        this.searchService.searchResults = cls;
                    })
                    .then(() => {
                        if (_.isEmpty(this.searchService.searchResults)) {
                            this.router.navigateByUrl('/')
                                .then(() => {
                                    console.warn('Returning to the home page as there are no search results');
                                });
                        }
                        this.resultListings = this.searchService.searchResults;
                    })
                    .then(() => {
                        this.spinner.hide();
                    })
                    .catch((e) => {
                        console.error(e);
                        this.spinner.hide();
                    });
            } else {
                this.spinner.hide();
                this.router.navigateByUrl('/')
                    .then(() => {
                        console.warn('Returning to the home page as there are no queries parameters!');
                    });
            }
        });
    }

    formatAmenities(queryParams: SearchQueryParams): Listing {
        const listing = Listing.populateFromRecord({
            postalCode: Number(queryParams.postalCode),
            bedroomsAvailable: Number(queryParams.bedroomsAvailable),
            amtRent: Number(queryParams.budgetMin),
            amtSecurity: Number(queryParams.budgetMax),
            unitType: Number(queryParams.unitType)
        });
        if (queryParams.amenities && queryParams.amenities.length) {
            queryParams.amenities.forEach(amenity => {
                const prop = _.capitalize(amenity);
                const propertyName = `amenity${ prop }`;
                if (listing.hasOwnProperty(propertyName)) {
                    listing[propertyName] = true;
                }
            });
        }
        return listing;
    }


    onListingClick(listing: ComplexListing): void {
        const modal = this.modalService.open(ListingModifyComponent, {
            ariaLabelledBy: 'modal-basic-title',
            windowClass: 'listing-modify-modal-window-class'
        });
        modal.componentInstance.listing = listing;
        modal.componentInstance.unitTypes = this.springboardService.UnitTypes;
        modal.componentInstance.localities = this.springboardService.Localities;
        modal.componentInstance.isEditable = false;
        modal.result.then(() => {
            console.log('Legit close');
        }, (action: string) => {
        });
    }

    formatListingName(listing: ComplexListing): string {
        return `${ listing.listing.streetNumber } ${ listing.listing.streetName } ${ listing.listing.locality }`
    }

    formatUnitType(listing: ComplexListing): string {
        const unit: UnitType = _.find(this.springboardService.UnitTypes, { unitTypeId: listing.listing.unitType });
        if (unit) {
            return unit.unitTypeName;
        } else {
            return `Unknown`;
        }
    }

    formatBedrooms(listing: ComplexListing): string {
        const open = listing.listing.bedroomsAvailable;
        const total = listing.listing.bedroomsTotal;
        return `Bedrooms (Open/Total): ${ open || '?' }/${ total || '?' }`;
    }

    onListingMouseEnter(listing: ComplexListing): void {
        const id = listing.listing.listingId;
        if (id !== this.hoverOverListingId) {
            this.hoverOverListingId = id;
        }
    }

    formatAmounts(listing: ComplexListing): string {
        const rent = listing.listing.amtRent || 'Unknown';
        const security = listing.listing.amtSecurity || 'Unknown';
        return `$${ rent } per month`
    }

}
