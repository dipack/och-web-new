import * as _ from 'lodash';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SpringboardService } from '../springboard/springboard.service';
import { Locality } from '../models/locality';


export interface SearchQueryParams {
    postalCode: number;
    amenities: string[];
    bedroomsAvailable: number;
    budgetMin: number;
    budgetMax: number;
    unitType: number;
    getAll?: boolean;
}

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: [
        './home.component.css'
    ]
})
export class HomeComponent implements OnInit {
    public options;
    public userSelections;
    public bedroomsRange = [];

    constructor(readonly router: Router,
                readonly springboardService: SpringboardService) {
    }

    ngOnInit(): void {
        this.options = {
            rent: { max: 1000, min: 200 },
            security: { max: 500, min: 100 },
            areas: this.springboardService.Localities,
            bedrooms: { max: 4, min: 1 },
            unitTypes: this.springboardService.UnitTypes,
            leaseLength: { max: 11, min: 3 },
            isVerified: [true, false],
            amenities: [
                'heating', 'gas', 'water', 'electricity', 'internet', 'cable', 'stove', 'refrigerator', 'offStreetParking', 'garage',
                'insulated', 'garbageCollection', 'centralAc', 'stormWindows', 'furnished', 'washer', 'dryer',
                'smokeDetectors', 'securitySystem', 'exteriorDoorDeadbolt', 'fireSafetyCode', 'coDetectors', 'electricalSafetyCode'
            ]
        };
        this.userSelections = {
            priceRange: { min: this.options.rent.min, max: this.options.rent.max },
            area: { name: 'Buffalo', postalCode: 14214 },
            bedrooms: 2,
            unitType: 1,
            leaseLength: 11,
            isVerified: true,
            amenities: ['heating', 'water', 'electricity']
        };
        this.bedroomsRange = this.generateRange(this.options.bedrooms.min, this.options.bedrooms.max);
    }

    generateRange(minValue: number, maxValue: number, step: number = 1) {
        return Array(Math.ceil((maxValue - minValue + 1) / step)).fill(minValue).map((x, y) => x + y * step)
    }

    populateUserSelection(property: string, value: any) {
        console.log('Adding to property', property, value);
        if (property === 'amenities') {
            if (!this.userSelections.amenities.includes(value)) {
                this.userSelections.amenities.push(value);
            }
        } else {
            this.userSelections[property] = value;
        }
    }

    removeUserSelection(property: string, value: any) {
        console.log('Removing from property', property, value);
        if (property === 'amenities') {
            if (this.userSelections.amenities.includes(value)) {
                _.remove(this.userSelections.amenities, value);
            }
        } else {
            // Todo: Handle removal case
            console.error('Cannot handle removal of anything apart from Amenities for now');
        }
    }


    onSearchClick() {
        const u = this.userSelections;
        const area = _.find(this.options.areas as Array<Locality>, { postalCode: Number(u.area) });
        const listing: SearchQueryParams = {
            postalCode: Number(area.postalCode),
            bedroomsAvailable: Number(u.bedrooms),
            budgetMin: Number(u.priceRange.min),
            budgetMax: Number(u.priceRange.max),
            unitType: Number(u.unitType),
            amenities: u.amenities,
            // Todo: Fix this in final release
            getAll: true
        };
        return this.router.navigate(
            ['/search'],
            { queryParams: listing }
        );
    }
}
