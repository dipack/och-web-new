import * as _ from 'lodash';
import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UnitType } from '../models/unit-type';
import { Locality } from '../models/locality';
import { OchUtil } from '../util';
import { ENDPOINTS } from '../constants';
import { SearchService } from '../search/search.service';
import { ComplexApt } from '../models/complex-apt';
import { Observable } from 'rxjs';
import { User } from '../models/auth/user';
import { AuthenticationService } from '../auth/authentication.service';

export interface SpringboardResponse {
    unitTypes: Readonly<Array<UnitType>>;
    localities: Readonly<Array<Locality>>;
}

@Injectable()
export class SpringboardService {
    public UnitTypes: Readonly<Array<UnitType>> = [];
    public Localities: Readonly<Array<Locality>> = [];
    public ApartmentComplexes: Readonly<Array<ComplexApt>> = [];

    constructor(readonly http: HttpClient,
                readonly authService: AuthenticationService,
                readonly searchService: SearchService,
                @Inject('BASE_URL') readonly baseUrl: string) {
    }

    constructUrl(action: string, ...args: string[]) {
        return OchUtil.constructUrl(this.baseUrl, action, ...args);
    }

    hasData(): boolean {
        return this.UnitTypes.length > 0 &&
            this.Localities.length > 0;
    }

    launchOffBoard(): Promise<void> {
        const headers = OchUtil.getHeaders();
        return this.http.get<SpringboardResponse>(this.constructUrl(ENDPOINTS.SPRINGBOARD), { headers: headers }).toPromise()
            .then((response: SpringboardResponse) => {
                this.UnitTypes = response.unitTypes.map(u => UnitType.populateFromRecord(u)).sort((x, y) => {
                    return x.unitTypeId < y.unitTypeId ? -1 : 1;
                });
                this.Localities = response.localities.map(l => Locality.populateFromRecord(l)).sort((x, y) => {
                    return x.postalCode < y.postalCode ? -1 : 1;
                });
                return this.searchService.getAllAptComplexes();
            })
            .then((apts: Array<ComplexApt>) => {
                this.ApartmentComplexes = apts;
            })
            .catch(console.error)
    }

    isApartmentComplex(listingId: number): boolean {
        return _.find(this.ApartmentComplexes, (ca: ComplexApt) => {
            return ca.aptComplex.listingId === listingId;
        }) !== undefined;
    }

}
