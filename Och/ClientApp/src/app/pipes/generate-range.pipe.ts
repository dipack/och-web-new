import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: 'generateRange'
})
export class GenerateRange implements PipeTransform {
    transform(value: any, minValue: number, maxValue: number, step: number = 1): any {
        return Array(Math.ceil((maxValue - minValue) / step)).fill(minValue).map((x, y) => x + y * step)
    }
}
