import { Component, OnInit } from '@angular/core';
import { SpringboardService } from './springboard/springboard.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    public readyToLoadApp = false;

    constructor(readonly springboardService: SpringboardService) {

    }

    ngOnInit(): void {
        this.springboardService.launchOffBoard()
            .then(() => {
                console.log('Successfully fetched base information');
                this.readyToLoadApp = true;
            });
    }
}
