import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MarkdownService } from 'ngx-markdown';

@Component({
    selector: 'app-display-static',
    templateUrl: 'display-static.component.html'
})
export class DisplayStaticComponent implements OnInit {
    @Input()
    public markdownFileUrl: string;
    // Display disclaimers page if invalid markdown file url is passed
    private static DEFAULT_MARKDOWN_URL = 'pages/disclaimer.md';

    constructor(readonly route: ActivatedRoute,
                readonly markdownService: MarkdownService) {
    }

    ngOnInit(): void {
        const windowPathname = window.location.pathname;
        this.markdownService.renderer.heading = (text: string, level: number) => {
            const escapedText = text.toLowerCase().replace(/[^\w]+/g, '-');
            return `
            <h${ level }>
                <a name="${ escapedText }" class="fa fa-link" href="${ windowPathname }#${ escapedText }"><span class="header-link"></span></a>
                ${ text }
            </h${ level }>
            `;
        };
        this.markdownFileUrl = this.route.snapshot.data['markdownFileUrl'] || DisplayStaticComponent.DEFAULT_MARKDOWN_URL;
    }
}
