import { NgModule } from "@angular/core";
import { MarkdownModule } from "ngx-markdown";
import { DisplayStaticComponent } from "./display-static.component";

@NgModule({
    imports: [
        MarkdownModule.forChild()
    ],
    declarations: [
        DisplayStaticComponent
    ],
    exports: [
        DisplayStaticComponent
    ]
})
export class DisplayStaticModule {}
