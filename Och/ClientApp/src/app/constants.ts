// @ts-ignore
export const DATETIME_FORMAT = {
    FROM_DB: 'yyyy-MM-ddTHH:mm:ss',
    ISO_DATE: 'yyyy-MM-dd',
    ZONE: 'America/New_York'
};
export const ENDPOINTS = {
    AUTHENTICATE: 'api/auth/authenticate',
    ADMIN: {
        LISTING: '/admin/listing',
        CONTACT: '/admin/contact'
    },
    SPRINGBOARD: 'springboard',
    APT_COMPLEX: {
        UPDATE: 'update-apt-complex',
        INSERT: 'insert-apt-complex',
        GET_ONE: 'get-apt-complex',
        GET_ALL: 'get-all-apt-complex',
        DELETE: 'delete-apt-complex',
    },
    LISTING: {
        UPDATE: 'update',
        INSERT: 'insert',
        GET_ONE: 'get-listing',
        GET_ALL: 'get-all-listings',
        DELETE: 'delete-listing',
        SEARCH: 'search'
    },
    CONTACT: {
        UPDATE: 'update-contact',
        INSERT: 'insert-contact',
        DELETE: 'delete-contact',
        GET_ONE: 'get-contact',
        GET_ALL: 'get-all-contacts'
    },
    PICTURE: {
        INSERT: 'insert-listing-picture',
        GET_ALL_FOR_ONE: 'get-listing-pictures',
        UPDATE: 'update-listing-picture',
        DELETE: 'delete-listing-picture'
    },
    LOCATION: {
        INSERT: 'insert-listing-location',
        UPDATE: 'update-listing-location',
        DELETE: 'delete-listing-location',
        GET_ONE: 'get-listing-location'
    }
};

export const AUTH = {
    LS_USER_KEY: 'currentUser'
};

// https://www.iana.org/assignments/media-types/media-types.xhtml
export const SUPPORTED_IANA_FORMATS = [
    'image/png', 'image/x-png',
    'image/jpeg',
    'image/gif'
];
