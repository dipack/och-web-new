import { LAT_LON_INVALID_VALUE } from '../search/map.service';

export class ListingLocation {
    constructor(public listingId: number = -1,
                public latitude: number = LAT_LON_INVALID_VALUE,
                public longitude: number = LAT_LON_INVALID_VALUE) {
    }


    static populateFromRecord(reader: any): ListingLocation {
        return new ListingLocation(
            reader.listingId,
            reader.latitude,
            reader.longitude
        );
    }
}
