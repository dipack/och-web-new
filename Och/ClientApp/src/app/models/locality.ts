export class Locality {
    constructor(
        public postalCode: number,
        public name: string
    ) {
    }

    static populateFromRecord(reader: any): Locality {
        return new Locality(reader.postalCode, reader.name);
    }
}
