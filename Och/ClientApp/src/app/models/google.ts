export interface GAddressComponent {
    long_name: string;
    short_name: string;
    types: Array<string>;
}

export interface GLatLng {
    lat: number;
    lng: number;
}

export interface GGeometry {
    location: GLatLng;
    location_type: string;
    viewport: { northeast: GLatLng, southwest: GLatLng };
}

export interface GGeocodeResult {
    place_id: string;
    formatted_address: string;
    address_components: Array<GAddressComponent>;
    geometry: GGeometry;
    types: Array<string>;
}

export interface GoogleGeocodeResponse {
    results: Array<GGeocodeResult>;
    status: string;
}
