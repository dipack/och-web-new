import { DateTime } from 'luxon';

export class Listing {
    constructor(
        public listingId: number = -1,
        public listingActive: boolean = false,
        public streetNumber: number = 0,
        public streetName: string = '',
        public locality: string = '',
        public postalCode: number = 14214,
        public apartmentNumber: number = 0,
        public unitType: number = 1,
        public bedroomsTotal: number = 1,
        public bedroomsAvailable: number = 1,
        public dateAvailable: string = Listing.getDefaultAvailableDate(),
        public dateListed: string = Listing.getDefaultListedDate(),
        public dateExpiry: string = Listing.getDefaultExpiryDate(),
        public leaseLengthMonths: number = 11,
        public amtRent: number = 0,
        public amtSecurity: number = 0,
        public amenityHeating: boolean = false,
        public amenityGas: boolean = false,
        public amenityWater: boolean = false,
        public amenityElectricity: boolean = false,
        public amenityInternet: boolean = false,
        public amenityCable: boolean = false,
        public amenityStove: boolean = false,
        public amenityRefrigerator: boolean = false,
        public amenityOffStreetParking: boolean = false,
        public amenityGarage: boolean = false,
        public amenityInsulated: boolean = false,
        public amenityGarbageCollection: boolean = false,
        public amenityCentralAc: boolean = false,
        public amenityStormWindows: boolean = false,
        public amenityFurnished: boolean = false,
        public amenityWasher: boolean = false,
        public amenityDryer: boolean = false,
        public amenitySmokeDetectors: boolean = false,
        public amenitySecuritySystem: boolean = false,
        public amenityExteriorDoorDeadbolt: boolean = false,
        public amenityFireSafetyCode: boolean = false,
        public amenityCoDetectors: boolean = false,
        public amenityElectricalSafetyCode: boolean = false,
        public rulesPetsAllowed: boolean = false,
        public bedroomsLocatedBasementAttic: boolean = false,
        public verifiedProperty: boolean = false,
        public certificateOfOccupancy: boolean = false,
        public registeredRentalRegistry: boolean = false,
        public contactDetailsId: number = -1,
        public comments: string = '') {
        this.dateAvailable = DateTime.fromISO(this.dateAvailable).toISODate();
        this.dateListed = DateTime.fromISO(this.dateListed).toISODate();
        this.dateExpiry = DateTime.fromISO(this.dateExpiry).toISODate();
    }

    static populateFromRecord(reader: any): Listing {
        return new Listing
        (
            reader.listingId,
            reader.listingActive,
            reader.streetNumber,
            reader.streetName,
            reader.locality,
            reader.postalCode,
            reader.apartmentNumber,
            reader.unitType,
            reader.bedroomsTotal,
            reader.bedroomsAvailable,
            reader.dateAvailable,
            reader.dateListed,
            reader.dateExpiry,
            reader.leaseLengthMonths,
            reader.amtRent,
            reader.amtSecurity,
            reader.amenityHeating,
            reader.amenityGas,
            reader.amenityWater,
            reader.amenityElectricity,
            reader.amenityInternet,
            reader.amenityCable,
            reader.amenityStove,
            reader.amenityRefrigerator,
            reader.amenityOffStreetParking,
            reader.amenityGarage,
            reader.amenityInsulated,
            reader.amenityGarbageCollection,
            reader.amenityCentralAc,
            reader.amenityStormWindows,
            reader.amenityFurnished,
            reader.amenityWasher,
            reader.amenityDryer,
            reader.amenitySmokeDetectors,
            reader.amenitySecuritySystem,
            reader.amenityExteriorDoorDeadbolt,
            reader.amenityFireSafetyCode,
            reader.amenityCoDetectors,
            reader.amenityElectricalSafetyCode,
            reader.rulesPetsAllowed,
            reader.bedroomsLocatedBasementAttic,
            reader.verifiedProperty,
            reader.certificateOfOccupancy,
            reader.registeredRentalRegistry,
            reader.contactDetailsId,
            reader.comments
        );
    }

    static getDefaultAvailableDate(): string {
        return DateTime.local().plus({ month: 1 }).toISODate();
    }

    static getDefaultListedDate(): string {
        return DateTime.local().toISODate();
    }

    static getDefaultExpiryDate(): string {
        return DateTime.local().plus({ month: 3 }).toISODate();
    }


    // public formatDates(format: string): void {
    //     this.dateAvailable = OchUtil.formatDate(this.dateAvailable.toString(), format).toISODate();
    //     this.dateListed = OchUtil.formatDate(this.dateListed.toString(), format).toISODate();
    //     this.dateExpiry = OchUtil.formatDate(this.dateExpiry.toString(), format).toISODate();
    // }
}


