import { Listing } from './listing';
import { ListingPicture } from './listing-picture';
import { ListingContact } from './listing-contact';
import { ListingLocation } from './listing-location';

export class ComplexListing {
    constructor(
        public listing: Listing = new Listing(),
        public listingPictures: Array<ListingPicture> = [],
        public listingLocation: ListingLocation = new ListingLocation(),
        public listingContact: ListingContact = new ListingContact()
    ) {
    }

    static populateFromRecord(json: any) {
        const cl = new ComplexListing(
            json.listing || new Listing(),
            json.listingPictures || [],
            json.listingLocation || new ListingLocation(),
            json.listingContact || new ListingContact()
        );
        if (json.listing) {
            cl.listing = Listing.populateFromRecord(json.listing);
        }
        if (json.listingPictures && json.listingPictures.length > 0) {
            cl.listingPictures = json.listingPictures.map(lp => ListingPicture.populateFromRecord(lp));
        }
        if (json.listingContact) {
            cl.listingContact = ListingContact.populateFromRecord(json.listingContact);
        }
        if (json.listingLocation) {
            cl.listingLocation = ListingLocation.populateFromRecord(json.listingLocation);
        }
        return cl;
    }
}
