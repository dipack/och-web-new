export class UnitType {
    constructor(
        public unitTypeId: number,
        public unitTypeName: string
    ) {
    }

    static populateFromRecord(reader: any): UnitType {
        return new UnitType(reader.unitTypeId, reader.unitTypeName);
    }
}
