export class ApartmentComplex {

    constructor(
        public apartmentId: number = -1,
        public apartmentName: string = '',
        public listingId: number = -1,
        public apartmentAddress: string = '',
        public contactDetailsId: number = -1,
        public apartmentDescription: string = '',
        public apartmentUrl: string = '',
        public displayOrder: number = -1,
        public listingActive: boolean = false
    ) {

    }

    static populateFromRecord(reader: any): ApartmentComplex {
        return new ApartmentComplex(
            reader.apartmentId,
            reader.apartmentName,
            reader.listingId,
            reader.apartmentAddress,
            reader.contactDetailsId,
            reader.apartmentDescription,
            reader.apartmentUrl,
            reader.displayOrder,
            reader.listingActive
        );

    }
}
