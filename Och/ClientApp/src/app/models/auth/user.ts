import { Role } from './role';

export class User {
    constructor(public id: number = -1,
                public firstName: string = '',
                public lastName: string = '',
                public username: string = '',
                public password: string = '',
                public role: string = Role.User,
                public token: string = '') {

    }

    static populateFromRecord(reader: any) {
        return new User(
            reader.id,
            reader.firstName,
            reader.lastName,
            reader.username,
            reader.password,
            reader.role,
            reader.token
        )
    }
}
