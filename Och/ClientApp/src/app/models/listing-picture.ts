export class ListingPicture {
    constructor(
        public imageId: number,
        public listingId: number,
        public imageUrl: string,
        public imageDescription: string,
    ) {
    }

    static populateFromRecord(reader: any): ListingPicture {
        return new ListingPicture(
            reader.imageId,
            reader.listingId,
            reader.imageUrl,
            reader.imageDescription,
        );
    }
}
