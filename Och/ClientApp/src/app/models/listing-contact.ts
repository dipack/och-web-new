export class ListingContact {
    constructor(
        public contactId: number = -1,
        public name: string = '',
        public primaryPhone: string = '',
        public secondaryPhone: string = '',
        public email: string = '',
    ) {
    }

    static populateFromRecord(reader: any): ListingContact {
        return new ListingContact(
            reader.contactId,
            reader.name,
            reader.primaryPhone,
            reader.secondaryPhone,
            reader.email,
        );
    }
}
