import * as _ from 'lodash';
import { forkJoin } from 'rxjs';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { SearchService } from '../../search/search.service';
import { CellClickedEvent, ColDef, GridApi, GridOptions, RowClickedEvent, RowNode } from 'ag-grid-community';
import { ComplexListing } from '../../models/complex-listing';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OchUtil } from '../../util';
import { OchGridUtil } from '../../grid/grid-util';
import { ComplexListingChanges, ListingModifyComponent } from './listing-modify.component';
import { SpringboardService } from '../../springboard/springboard.service';
import { ListingContact } from '../../models/listing-contact';
import { ListingPicture } from '../../models/listing-picture';
import { NgxSpinnerService } from 'ngx-spinner';
import { Locality } from '../../models/locality';

export const MODAL_ACTIONS = {
    SAVE: 'save',
    REVERT: 'revert',
    DISMISS: 'dismiss'
};

export interface ToAdd {
    listings: Array<ComplexListing>;
    pictures: Array<ListingPicture>;
}

export interface ToDelete {
    listings: Array<number>;
    pictures: Array<number>;
}

@Component({
    encapsulation: ViewEncapsulation.None,
    selector: 'app-admin-listing-display',
    templateUrl: 'listing-display.component.html',
    styleUrls: [
        'listing-display.component.css'
    ]
})
export class AdminListingDisplayComponent implements OnInit {
    public listings: Array<ComplexListing> = [];
    public originalListings: Readonly<Array<ComplexListing>>;
    public differences: Array<Partial<ComplexListing>> = [];

    private gridApi: GridApi;
    private gridColumnApi: any;
    private allContacts: Array<ListingContact> = [];

    private toAdd: ToAdd = {
        listings: [],
        pictures: [],
    };

    private toDelete: ToDelete = {
        listings: [],
        pictures: []
    };

    private defaultColumnDefinition: ColDef = {
        sortable: true,
        filter: true,
        editable: false,
        width: 150
    };

    public gridOptions: GridOptions = {};

    public readyToLoadPage = false;

    constructor(readonly modalService: NgbModal,
                readonly spinner: NgxSpinnerService,
                readonly springboardService: SpringboardService,
                readonly searchService: SearchService) {
    }

    ngOnInit(): void {
        this.spinner.show();
        this.gridOptions = this.getGridOptions();
        this.loadGridData()
            .then(() => {
                this.readyToLoadPage = true;
                this.spinner.hide();
            })
            .catch((e) => {
                this.spinner.hide();
                this.readyToLoadPage = true;
                console.error('Failed to load grid data');
                console.error(e);
            });
    }

    onAddNewListingClick(): void {
        const modal = this.modalService.open(ListingModifyComponent, {
            ariaLabelledBy: 'modal-basic-title',
            windowClass: 'listing-modify-modal-window-class'
        });
        modal.componentInstance.listing = new ComplexListing();
        modal.componentInstance.unitTypes = this.springboardService.UnitTypes;
        modal.componentInstance.localities = this.springboardService.Localities;
        modal.componentInstance.contacts = this.allContacts;
        modal.componentInstance.isEditable = true;
        modal.componentInstance.onSubmitModifications.subscribe((changes: ComplexListingChanges) => {
            const listing = changes.listing;
            let randomId = OchUtil.generateRandomId();
            const currentIds = this.listings.map(l => l.listing.listingId);
            while (_.includes(currentIds, randomId)) {
                randomId = OchUtil.generateRandomId();
            }
            listing.listing.listingId = randomId;
            changes.listing = ComplexListing.populateFromRecord(listing);
            // For a new listing, the id is -1, this way they all keep their IDs
            // We need to update the listingId after the listing has been successfully inserted
            changes.picturesToAdd = changes.picturesToAdd.map(pic => {
                return ListingPicture.populateFromRecord({
                    ...pic,
                    listingId: changes.listing.listing.listingId
                });
            });
            return this.onListingAdded(changes);
        });
        modal.result.then(() => {
            console.log('Legit close');
        }, (action: string) => {
        });

    }

    loadGridData(): Promise<any> {
        const self = this;
        return this.searchService.getAllListings()
            .then((results: Array<ComplexListing>) => {
                self.listings = results
                    .map(r => ComplexListing.populateFromRecord(r))
                    .filter(r => !self.springboardService.isApartmentComplex(r.listing.listingId));
                self.originalListings = _.cloneDeep(self.listings);
                self.resetLocalTrackers();
            })
            .then(() => {
                return self.searchService.getAllContacts()
                    .then((contacts: Array<ListingContact>) => {
                        self.allContacts = contacts;
                    });
            })
            .catch(console.error);
    }

    onGridReady(params: any) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    }

    onListingAdded(changes: ComplexListingChanges) {
        console.log('New listing created is', changes);
        // We never really take into account picture Ids being deleted for new listings
        // as they don't yet exist in the DB
        this.toAdd.listings.push(changes.listing);
        this.toAdd.pictures.push(...changes.picturesToAdd);
        this.listings.push(changes.listing);
        this.gridApi.updateRowData({
            add: [changes.listing]
        });
    }

    onListingModified(event: RowClickedEvent, changes: ComplexListingChanges) {
        console.log('Listing modified as follows', changes);
        if (this.isListingToBeAdded(changes.listing.listing.listingId)) {
            _.remove(this.toAdd.listings, (l) => {
                return l.listing.listingId === changes.listing.listing.listingId;
            });
            this.toAdd.listings.push(changes.listing);
        }
        this.toAdd.pictures.push(...changes.picturesToAdd);
        this.toDelete.pictures.push(...changes.pictureIdsToDelete);
        event.node.setData(changes.listing);
        this.computeListingDetailDifferences();
    }

    computeListingDetailDifferences(): Array<Partial<ComplexListing>> {
        const self = this;
        self.listings = [];
        self.gridApi.forEachNode((row: RowNode, idx: number) => {
            self.listings.push(row.data);
        });
        const difference: Array<Partial<ComplexListing>> = OchUtil
            .deepDifference(self.listings, self.originalListings)
            .filter((cl: ComplexListing) => {
                return (cl.listing !== undefined || cl.listingLocation !== undefined)
                    && !this.isListingToBeAdded(cl.listing.listingId);
            });

        if (difference.length) {
            this.differences = difference;
        }
        return difference;
    }

    isThereAChange(): boolean {
        return (
            this.differences.length > 0 ||
            this.toAdd.listings.length > 0 ||
            this.toDelete.listings.length > 0 ||
            this.toAdd.pictures.length > 0 ||
            this.toDelete.pictures.length > 0
        );
    }

    onSaveChangesClick(content: any) {
        const self = this;
        self.computeListingDetailDifferences();
        if (this.isThereAChange()) {
            this.modalService
                .open(content, { ariaLabelledBy: 'modal-basic-title' })
                .result
                .then(() => {
                    this.spinner.show();
                    const listingsToUpdate = self.differences
                        .map((diff: Partial<ComplexListing>) => {
                            let listingId;
                            if (diff.listing) {
                                listingId = diff.listing.listingId;
                            } else if (diff.listingLocation) {
                                listingId = diff.listingLocation.listingId;
                            }
                            if (!self.isListingToBeAdded(listingId)) {
                                return _.find(self.listings, (c: ComplexListing) => {
                                    return c.listing.listingId === listingId;
                                });
                            }
                        })
                        .filter(l => l !== undefined);

                    const listingIdsToDelete = self.toDelete.listings
                        .map((id: number) => {
                            if (!self.isListingToBeAdded(id)) {
                                return id;
                            }
                        })
                        .filter(l => l !== undefined);

                    const listingsToAdd = self.toAdd.listings
                        .map((listing: ComplexListing) => {
                            if (!_.includes(self.toDelete.listings, listing.listing.listingId)) {
                                // Bundle new listing pics with the new listing
                                const pictures = self.toAdd.pictures.filter((pic) => pic.listingId === listing.listing.listingId);
                                const remainingPictures = self.toAdd.pictures.filter((pic) => pic.listingId !== listing.listing.listingId);
                                listing.listingPictures = pictures;
                                self.toAdd.pictures = remainingPictures;
                                return listing;
                            }
                        })
                        .filter(l => l !== undefined);

                    const picturesToAdd = self.toAdd.pictures.map(p => p);
                    const pictureIdsToDelete = self.toDelete.pictures.map(i => i);

                    // Resetting these values as they control how the buttons on the page work
                    self.resetLocalTrackers();

                    console.log('Listings to update', listingsToUpdate);
                    console.log('Listing Ids to delete', listingIdsToDelete);
                    console.log('Listings to add', listingsToAdd);

                    return Promise
                        .all(
                            [
                                ...listingsToUpdate.map(l => self.searchService.updateListing(l)),
                                ...listingsToAdd.map(l => self.searchService.addNewListing(l)),
                                ...picturesToAdd.map(p => self.searchService.insertImage(p).toPromise()),
                                ...pictureIdsToDelete.map(id => self.searchService.deletePicture(id).toPromise()),
                                ...listingIdsToDelete.map(id => {
                                    return forkJoin(
                                        self.searchService.deleteListing(id),
                                        self.searchService.deleteLocation(id)
                                    ).toPromise();
                                }),
                            ] as any[]
                        )
                        .then(() => {
                            this.spinner.hide();
                            return this.loadGridData();
                        });
                }, (action: string) => {
                    if (action === 'revert') {
                        console.error('Revert');
                        self.listings = _.cloneDeep(self.originalListings);
                        self.resetLocalTrackers();
                    } else if (action === 'dismiss') {
                        return undefined;
                    }
                });
        }
    }

    getGridOptions(): GridOptions {
        const self = this;
        const onDeleteRowClick = (params: any) => {
            const currentRow: ComplexListing = params.data;
            if (!_.includes(self.toDelete.listings, currentRow.listing.listingId)) {
                console.warn(`Scheduling listing id: ${ currentRow.listing.listingId } for deletion`);
                self.toDelete.listings.push(currentRow.listing.listingId);
            } else {
                console.warn(`Not deleting listing id: ${ currentRow.listing.listingId }`);
                _.remove(self.toDelete.listings, (id: number) => {
                    return id === currentRow.listing.listingId;
                })
            }
            self.gridApi.redrawRows();
        };
        return {
            columnDefs: [
                {
                    headerName: '',
                    onCellClicked: onDeleteRowClick,
                    width: 50,
                    cellRenderer: (params: any) => {
                        const currentRow: ComplexListing = params.data;
                        if (!_.includes(self.toDelete.listings, currentRow.listing.listingId)) {
                            return `<a class="fa fa-trash"></a>`
                        } else {
                            return `<a class="fa fa-recycle"></a>`
                        }
                    }
                },
                {
                    headerName: 'Listing ID',
                    field: 'listing.listingId',
                    editable: false,
                    sortable: false,
                    filter: false
                },
                { headerName: 'Active Listing?', field: 'listing.listingActive' },
                {
                    headerName: 'Address',
                    cellRenderer: (params: any) => {
                        const currentRow: ComplexListing = params.data;
                        const locality: Locality = _.find(self.springboardService.Localities, { postalCode: currentRow.listing.postalCode });
                        return `${ currentRow.listing.streetNumber } ${ currentRow.listing.streetName } ${ locality ? locality.name : '' } ${ currentRow.listing.postalCode }`;
                    }
                },
                { headerName: 'Date Listed', field: 'listing.dateListed', filter: 'agDateColumnFilter' },
                { headerName: 'Date Expiry', field: 'listing.dateExpiry', filter: 'agDateColumnFilter' },
                {
                    headerName: 'Rent (per month)',
                    field: 'listing.amtRent',
                    valueParser: OchGridUtil.NumericValueParser
                }
            ],
            defaultColDef: {
                ...self.defaultColumnDefinition,
                onCellClicked(event: CellClickedEvent): void {
                    const currentRow: ComplexListing = event.data;
                    const cl = _.find(self.listings, (cl: ComplexListing) => {
                        return cl.listing.listingId === currentRow.listing.listingId;
                    });
                    if (cl) {
                        cl.listingPictures = self.getListingPictures(cl.listing.listingId);
                        const modal = self.modalService.open(ListingModifyComponent, {
                            ariaLabelledBy: 'modal-basic-title',
                            windowClass: 'listing-modify-modal-window-class'
                        });
                        modal.componentInstance.listing = cl;
                        modal.componentInstance.unitTypes = self.springboardService.UnitTypes;
                        modal.componentInstance.localities = self.springboardService.Localities;
                        modal.componentInstance.contacts = self.allContacts;
                        modal.componentInstance.isEditable = true;
                        modal.componentInstance.onSubmitModifications.subscribe((changes: ComplexListingChanges) => {
                            return self.onListingModified(event, changes);
                        });
                        modal.result.then(() => {
                            console.log('Legit close');
                        }, (action: string) => {
                        });
                    } else {
                        console.error(`Cannot find complex listing with id ${ currentRow.listing.listingId }`);
                    }
                }
            },
            rowClassRules: {
                'mark-for-deletion': (params: any) => {
                    const listingId: number = params.data.listing.listingId;
                    return _.includes(self.toDelete.listings, listingId);
                }
            }
        };
    }

    resetLocalTrackers(): void {
        this.differences = [];
        this.toAdd.listings = [];
        this.toAdd.pictures = [];
        this.toDelete.listings = [];
        this.toDelete.pictures = [];
    }

    isListingToBeAdded(listingId: number) {
        const found = _.find(this.toAdd.listings, (l: ComplexListing) => {
            return l.listing.listingId === listingId;
        });
        // This means that the current listing is NOT a newly added row
        return !found ? false : true;
    }

    openReadonlyModalFromId(listingId: string) {
        const found = _.find(this.listings, (l: ComplexListing) => {
            return l.listing.listingId === Number(listingId);
        });
        if (found) {
            this.openReadonlyModalFor(found);
        } else {
            console.warn(`No listing found with listing id ${ listingId }`);
        }
    }

    openReadonlyModalFor(listing: ComplexListing) {
        const pictures = this.getListingPictures(listing.listing.listingId);
        const updatedListing = _.cloneDeep(listing);
        updatedListing.listingPictures.push(...pictures);
        const modal = this.modalService.open(ListingModifyComponent, {
            ariaLabelledBy: 'modal-basic-title',
            windowClass: 'listing-modify-modal-window-class'
        });
        modal.componentInstance.listing = updatedListing;
        modal.componentInstance.unitTypes = this.springboardService.UnitTypes;
        modal.componentInstance.localities = this.springboardService.Localities;
        modal.componentInstance.isEditable = false;
        modal.result.then(() => {
            console.log('Legit close');
        }, (action: string) => {
        });
    }

    getListingPictures(listingId: number): Array<ListingPicture> {
        const apt: ComplexListing = _.find(this.listings, (ca: ComplexListing) => {
            return ca.listing.listingId === listingId;
        });
        const pics: Array<ListingPicture> = [];
        if (apt) {
            pics.push(...apt.listingPictures);
        }
        return pics
            .concat(...this.toAdd.pictures.filter(p => p.listingId === listingId))
            .filter(p => !_.includes(this.toDelete.pictures, p.listingId));
    }
}
