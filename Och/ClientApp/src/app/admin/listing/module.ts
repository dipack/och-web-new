import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbDatepickerModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AgGridModule } from 'ag-grid-angular';
import { AdminListingDisplayComponent as ListingDisplayComponent } from './listing-display.component';
import { NumericEditor } from '../../grid/numeric-editor.component';
import { OchGridModule } from '../../grid/module';
import { ListingModifyComponent } from './listing-modify.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgbModule,
        NgbDatepickerModule,
        ReactiveFormsModule,
        OchGridModule,
        AgGridModule.withComponents([
            NumericEditor
        ])
    ],
    declarations: [
        ListingDisplayComponent,
        ListingModifyComponent
    ],
    entryComponents: [
        ListingModifyComponent
    ],
    exports: [
        ListingDisplayComponent
    ]
})
export class AdminListingModule {
}
