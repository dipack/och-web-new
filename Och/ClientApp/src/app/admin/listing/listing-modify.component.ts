import * as _ from 'lodash';
import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { ComplexListing } from '../../models/complex-listing';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal, NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { UnitType } from '../../models/unit-type';
import { Locality } from '../../models/locality';
import { OchUtil } from '../../util';
import { DateTime } from 'luxon';
import { DATETIME_FORMAT, ENDPOINTS, SUPPORTED_IANA_FORMATS } from '../../constants';
import { Router } from '@angular/router';
import { ListingContact } from '../../models/listing-contact';
import { ListingPicture } from '../../models/listing-picture';
import { LAT_LON_INVALID_VALUE, LatLon, MapService } from '../../search/map.service';
import { Listing } from '../../models/listing';
import { ListingLocation } from '../../models/listing-location';

export interface ComplexListingChanges {
    listing: ComplexListing;
    picturesToAdd: Array<ListingPicture>;
    pictureIdsToDelete: Array<number>;
    listingLocation: ListingLocation;
}

@Component({
    // Need this to override bootstrap CSS
    encapsulation: ViewEncapsulation.None,
    selector: 'app-listing-modify',
    templateUrl: 'listing-modify.component.html',
    styleUrls: [
        'listing-modify.component.css'
    ]
})
export class ListingModifyComponent implements OnInit {
    @Input()
    public listing: ComplexListing;

    @Input()
    public unitTypes: Array<UnitType> = [];

    @Input()
    public localities: Array<Locality> = [];

    @Input()
    public contacts: Array<ListingContact> = [];

    @Input()
    public isEditable: boolean = false;

    @Input()
    public isContactEditable: boolean = false;

    @Output()
    public onSubmitModifications = new EventEmitter<ComplexListingChanges>();

    public listingModifyForm: FormGroup;

    private originalListingPictures: Array<ListingPicture> = [];
    private picturesToAdd: Array<ListingPicture> = [];
    private pictureIdsToDelete: Array<number> = [];

    public supportedImageFormats: string = SUPPORTED_IANA_FORMATS.join(',');

    constructor(readonly router: Router,
                readonly mapService: MapService,
                readonly activeModal: NgbActiveModal) {
    }

    isWindowWidthSmallerThanDesktop(): boolean {
        return OchUtil.isWindowWidthSmallerThanDesktop();
    }

    ngOnInit(): void {
        this.listingModifyForm = this.createFormGroup();
        this.originalListingPictures = _.cloneDeep(this.listing.listingPictures);
    }

    /**
     * Disable submit when:
     * 1. Form is pristine, or Form is invalid
     * 2. Images have not been changed
     */
    shouldSubmitButtonBeDisabled(): boolean {
        const haveImagesChanged = !_.isEqual(this.originalListingPictures, this.getListingPictures());
        let retVal = true;
        if (!this.listingModifyForm.pristine) {
            // If form is invalid
            retVal = !this.listingModifyForm.valid;
        }
        if (haveImagesChanged) {
            retVal = false;
        }
        return retVal;
    }

    /**
     * Disable revert when:
     * 1. Form is pristine
     * 2. Images have not changed
     */
    shouldRevertButtonBeDisabled(): boolean {
        const haveImagesChanged = !_.isEqual(this.originalListingPictures, this.getListingPictures());
        let retVal = true;
        if (!this.listingModifyForm.pristine) {
            retVal = false;
        }
        if (haveImagesChanged) {
            retVal = false;
        }
        return retVal;
    }

    getListingPictures(): Array<ListingPicture> {
        return this.listing.listingPictures
            .concat(...this.picturesToAdd)
            .filter((pic: ListingPicture) => !_.includes(this.pictureIdsToDelete, pic.imageId));
    }

    onSubmitClick(): void {
        const modified = ComplexListing.populateFromRecord({
            ...this.listing,
            listing: {
                ...this.listingModifyForm.controls.listing.value,
                dateAvailable: ListingModifyComponent.convertNgbDateToDate(this.listingModifyForm.controls.listing.value.dateAvailable as any),
                dateListed: ListingModifyComponent.convertNgbDateToDate(this.listingModifyForm.controls.listing.value.dateListed as any),
                dateExpiry: ListingModifyComponent.convertNgbDateToDate(this.listingModifyForm.controls.listing.value.dateExpiry as any),
            },
            listingContact: {
                ...this.listingModifyForm.controls.listingContact.value,
                // Cannot pick this up from the form as Angular then goes wacky
                // and does not include the rest of the fields
                contactId: this.listingModifyForm.controls.listing.value.contactDetailsId
            },
            listingLocation: {
                ...this.listingModifyForm.controls.listingLocation.value,
                listingId: this.listingModifyForm.controls.listing.value.listingId
            }
            // We do not add pictures, as we do not want them to be unnecessarily updated
        });
        const changes: ComplexListingChanges = {
            listing: modified,
            picturesToAdd: this.picturesToAdd,
            pictureIdsToDelete: this.pictureIdsToDelete,
            listingLocation: modified.listingLocation
        };
        this.onSubmitModifications.emit(changes);
        this.activeModal.close();
    }

    onRevertClick(): void {
        if (!this.listingModifyForm.pristine) {
            this.listingModifyForm.reset(this.listing);
        }
        this.listing.listingPictures = this.originalListingPictures;
        this.picturesToAdd = [];
    }

    onGoToListingClick(): Promise<boolean> {
        const listingId: number = this.listing.listing.listingId;
        this.activeModal.close();
        return this.router.navigateByUrl(`/listing/${ listingId }`);
    }

    onGoToAdminContactClick() {
        this.activeModal.close();
        return this.router.navigateByUrl(ENDPOINTS.ADMIN.CONTACT);
    }

    onDeletePictureClick(imageId: string) {
        const id = Number(imageId);
        this.pictureIdsToDelete.push(id);
    }

    onImageFileSelected(event: Event) {
        const files: FileList = (event.target as HTMLInputElement).files;
        const filtered = _.filter(files, (fi: File) => {
            return _.includes(SUPPORTED_IANA_FORMATS, fi.type);
        });
        const promises = _.map(filtered, (fi: File) => {
            return OchUtil.readImageFile(fi);
        });
        Promise.all(promises)
            .then((sources: string[]) => {
                const pictures = sources.map(src => {
                    return ListingPicture.populateFromRecord({
                        imageId: OchUtil.generateRandomId(),
                        listingId: this.listing.listing.listingId,
                        imageUrl: src,
                        imageDescription: 'New image'
                    })
                });
                this.picturesToAdd.push(...pictures);
            });
    }

    onSelectLocality(postalCode: string) {
        const locality: Locality = _.find(this.localities, { postalCode: Number(postalCode) });
        let pc = this.localities[0].postalCode;
        let ln = this.localities[0].name;
        if (locality) {
            pc = locality.postalCode;
            ln = locality.name;
            // Locality is set by postal code, and NOT by user
            this.listingModifyForm.get('listing.locality').setValue(ln);
        }
        this.listingModifyForm.get('listing.postalCode').setValue(pc);
    }

    onSelectUnitType(unitTypeId: string) {
        const typeId = Number(unitTypeId);
        let unitType: UnitType = _.find(this.unitTypes, { unitTypeId: typeId });
        const defaultUnitType = this.unitTypes[0];
        if (!unitType) {
            unitType = defaultUnitType;
        }
        this.listingModifyForm.get('listing.unitType').setValue(unitType.unitTypeId);
    }

    onSelectContact(contactId: string) {
        const contact: ListingContact = _.find(this.contacts, { contactId: Number(contactId) });
        if (contact) {
            this.listingModifyForm.get('listing.contactDetailsId').setValue(contact.contactId);
            this.listingModifyForm.get('listingContact.name').setValue(contact.name);
            this.listingModifyForm.get('listingContact.primaryPhone').setValue(contact.primaryPhone);
            this.listingModifyForm.get('listingContact.secondaryPhone').setValue(contact.secondaryPhone);
            this.listingModifyForm.get('listingContact.email').setValue(contact.email);
            // Required as Angular does not count forms as dirty if they are changed from the backend
            this.listingModifyForm.get('listing').markAsDirty();
        }
    }

    onDateSelect(modelName: string, selectedDate: NgbDate) {
        const jsDate = new Date(selectedDate.year, selectedDate.month - 1, selectedDate.day);
        const formattedDate = DateTime.fromJSDate(jsDate).toISODate();
        const formControlName = `listing.${ modelName }`;
        const ngbDate = ListingModifyComponent.convertDateToNgbDate(formattedDate, DATETIME_FORMAT.ISO_DATE);
        this.listingModifyForm.get(formControlName).setValue(ngbDate);
    }

    createFormGroup(): FormGroup {
        const locality: Locality = _.find(this.localities, { postalCode: Number(this.listing.listing.postalCode) });
        let pc = this.localities[0].postalCode;
        let ln = this.localities[0].name;
        if (locality) {
            pc = locality.postalCode;
            ln = locality.name;
        }
        const fg = new FormGroup({
            listing: new FormGroup({
                listingId: new FormControl({
                    value: this.listing.listing.listingId,
                    disabled: !this.isEditable
                }, [Validators.required]),
                listingActive: new FormControl({
                    value: this.listing.listing.listingActive,
                    disabled: !this.isEditable
                }, [Validators.required]),
                streetNumber: new FormControl({
                    value: this.listing.listing.streetNumber,
                    disabled: !this.isEditable
                }, [Validators.required]),
                streetName: new FormControl({
                    value: this.listing.listing.streetName,
                    disabled: !this.isEditable
                }, [Validators.required]),
                locality: new FormControl({
                    value: ln,
                    disabled: true
                }, [Validators.required]),
                postalCode: new FormControl({
                    value: pc,
                    disabled: !this.isEditable
                }, [Validators.required]),
                apartmentNumber: new FormControl({
                    value: this.listing.listing.apartmentNumber,
                    disabled: !this.isEditable
                }, [Validators.required]),
                unitType: new FormControl({
                    value: this.listing.listing.unitType,
                    disabled: !this.isEditable
                }, [Validators.required]),
                bedroomsTotal: new FormControl({
                    value: this.listing.listing.bedroomsTotal,
                    disabled: !this.isEditable
                }, [Validators.required]),
                bedroomsAvailable: new FormControl({
                    value: this.listing.listing.bedroomsAvailable,
                    disabled: !this.isEditable
                }, [Validators.required]),
                dateAvailable: new FormControl({
                    value: ListingModifyComponent.convertDateToNgbDate(this.listing.listing.dateAvailable.toString()),
                    disabled: !this.isEditable
                }, [Validators.required]),
                dateListed: new FormControl({
                    value: ListingModifyComponent.convertDateToNgbDate(this.listing.listing.dateListed.toString()),
                    disabled: !this.isEditable
                }, [Validators.required]),
                dateExpiry: new FormControl({
                    value: ListingModifyComponent.convertDateToNgbDate(this.listing.listing.dateExpiry.toString()),
                    disabled: !this.isEditable
                }, [Validators.required]),
                leaseLengthMonths: new FormControl({
                    value: this.listing.listing.leaseLengthMonths,
                    disabled: !this.isEditable
                }, [Validators.required]),
                amtRent: new FormControl({
                    value: this.listing.listing.amtRent,
                    disabled: !this.isEditable
                }, [Validators.required]),
                amtSecurity: new FormControl({
                    value: this.listing.listing.amtSecurity,
                    disabled: !this.isEditable
                }, [Validators.required]),
                amenityHeating: new FormControl({
                    value: this.listing.listing.amenityHeating,
                    disabled: !this.isEditable
                }, [Validators.required]),
                amenityGas: new FormControl({
                    value: this.listing.listing.amenityGas,
                    disabled: !this.isEditable
                }, [Validators.required]),
                amenityWater: new FormControl({
                    value: this.listing.listing.amenityWater,
                    disabled: !this.isEditable
                }, [Validators.required]),
                amenityElectricity: new FormControl({
                    value: this.listing.listing.amenityElectricity,
                    disabled: !this.isEditable
                }, [Validators.required]),
                amenityInternet: new FormControl({
                    value: this.listing.listing.amenityInternet,
                    disabled: !this.isEditable
                }, [Validators.required]),
                amenityCable: new FormControl({
                    value: this.listing.listing.amenityCable,
                    disabled: !this.isEditable
                }, [Validators.required]),
                amenityStove: new FormControl({
                    value: this.listing.listing.amenityStove,
                    disabled: !this.isEditable
                }, [Validators.required]),
                amenityRefrigerator: new FormControl({
                    value: this.listing.listing.amenityRefrigerator,
                    disabled: !this.isEditable
                }, [Validators.required]),
                amenityOffStreetParking: new FormControl({
                    value: this.listing.listing.amenityOffStreetParking,
                    disabled: !this.isEditable
                }, [Validators.required]),
                amenityGarage: new FormControl({
                    value: this.listing.listing.amenityGarage,
                    disabled: !this.isEditable
                }, [Validators.required]),
                amenityInsulated: new FormControl({
                    value: this.listing.listing.amenityInsulated,
                    disabled: !this.isEditable
                }, [Validators.required]),
                amenityGarbageCollection: new FormControl({
                    value: this.listing.listing.amenityGarbageCollection,
                    disabled: !this.isEditable
                }, [Validators.required]),
                amenityCentralAc: new FormControl({
                    value: this.listing.listing.amenityCentralAc,
                    disabled: !this.isEditable
                }, [Validators.required]),
                amenityStormWindows: new FormControl({
                    value: this.listing.listing.amenityStormWindows,
                    disabled: !this.isEditable
                }, [Validators.required]),
                amenityFurnished: new FormControl({
                    value: this.listing.listing.amenityFurnished,
                    disabled: !this.isEditable
                }, [Validators.required]),
                amenityWasher: new FormControl({
                    value: this.listing.listing.amenityWasher,
                    disabled: !this.isEditable
                }, [Validators.required]),
                amenityDryer: new FormControl({
                    value: this.listing.listing.amenityDryer,
                    disabled: !this.isEditable
                }, [Validators.required]),
                amenitySmokeDetectors: new FormControl({
                    value: this.listing.listing.amenitySmokeDetectors,
                    disabled: !this.isEditable
                }, [Validators.required]),
                amenitySecuritySystem: new FormControl({
                    value: this.listing.listing.amenitySecuritySystem,
                    disabled: !this.isEditable
                }, [Validators.required]),
                amenityExteriorDoorDeadbolt: new FormControl({
                    value: this.listing.listing.amenityExteriorDoorDeadbolt,
                    disabled: !this.isEditable
                }, [Validators.required]),
                amenityFireSafetyCode: new FormControl({
                    value: this.listing.listing.amenityFireSafetyCode,
                    disabled: !this.isEditable
                }, [Validators.required]),
                amenityCoDetectors: new FormControl({
                    value: this.listing.listing.amenityCoDetectors,
                    disabled: !this.isEditable
                }, [Validators.required]),
                amenityElectricalSafetyCode: new FormControl({
                    value: this.listing.listing.amenityElectricalSafetyCode,
                    disabled: !this.isEditable
                }, [Validators.required]),
                rulesPetsAllowed: new FormControl({
                    value: this.listing.listing.rulesPetsAllowed,
                    disabled: !this.isEditable
                }, [Validators.required]),
                bedroomsLocatedBasementAttic: new FormControl({
                    value: this.listing.listing.bedroomsLocatedBasementAttic,
                    disabled: !this.isEditable
                }, [Validators.required]),
                verifiedProperty: new FormControl({
                    value: this.listing.listing.verifiedProperty,
                    disabled: !this.isEditable
                }, [Validators.required]),
                certificateOfOccupancy: new FormControl({
                    value: this.listing.listing.certificateOfOccupancy,
                    disabled: !this.isEditable
                }, [Validators.required]),
                registeredRentalRegistry: new FormControl({
                    value: this.listing.listing.registeredRentalRegistry,
                    disabled: !this.isEditable
                }, [Validators.required]),
                contactDetailsId: new FormControl({
                    value: this.listing.listing.contactDetailsId,
                    disabled: !this.isEditable
                }, [Validators.required]),
                comments: new FormControl({ value: this.listing.listing.comments, disabled: !this.isEditable }, []),
            }),
            listingContact: new FormGroup({
                name: new FormControl({
                    value: this.listing.listingContact.name,
                    disabled: !this.isContactEditable
                }, [Validators.required]),
                primaryPhone: new FormControl({
                    value: this.listing.listingContact.primaryPhone,
                    disabled: !this.isContactEditable
                }),
                secondaryPhone: new FormControl({
                    value: this.listing.listingContact.secondaryPhone,
                    disabled: !this.isContactEditable
                }),
                email: new FormControl({
                    value: this.listing.listingContact.email,
                    disabled: !this.isContactEditable
                })
            }),
            listingLocation: new FormGroup({
                latitude: new FormControl({
                    value: this.listing.listingLocation.latitude,
                    disabled: true
                }, [Validators.required]),
                longitude: new FormControl({
                    value: this.listing.listingLocation.longitude,
                    disabled: true
                }, [Validators.required])
            })
        });
        return fg;
    }

    geocodeAddress(listing: Listing): Promise<LatLon> {
        return this.mapService.geocodePlace(listing)
            .then((latLon: LatLon) => {
                // this.listing.listingLocation.latitude = latLon.latitude;
                // this.listing.listingLocation.longitude = latLon.longitude;
                this.listingModifyForm.get('listingLocation.latitude').setValue(latLon.latitude);
                this.listingModifyForm.get('listingLocation.longitude').setValue(latLon.longitude);
                // Required as Angular does not count forms as dirty if they are changed from the backend
                this.listingModifyForm.get('listing').markAsDirty();
                return latLon;
            })
    }

    formatAddressHref(listing: ComplexListing): string {
        const gmapUrl = `https://maps.google.com/maps?q=`;
        let latitude = listing.listingLocation.latitude;
        let longitude = listing.listingLocation.longitude;
        if (latitude === LAT_LON_INVALID_VALUE) {
            latitude = this.listingModifyForm.get('listingLocation.latitude').value;
        }
        if (longitude === LAT_LON_INVALID_VALUE) {
            longitude = this.listingModifyForm.get('listingLocation.longitude').value;
        }
        return `${ gmapUrl }${ latitude },${ longitude }`;
    }

    static convertDateToNgbDate(dateStr: string, format: string = DATETIME_FORMAT.ISO_DATE): NgbDate {
        const date = OchUtil.formatDate(dateStr, format);
        return new NgbDate(date.year, date.month, date.day);
    }

    static convertNgbDateToDate(ngbDate: NgbDate): string {
        const jsDate = new Date(ngbDate.year, ngbDate.month - 1, ngbDate.day);
        return DateTime.fromJSDate(jsDate).toISODate();
    }
}
