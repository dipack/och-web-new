import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbDatepickerModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AgGridModule } from 'ag-grid-angular';
import { NumericEditor } from '../../grid/numeric-editor.component';
import { OchGridModule } from '../../grid/module';
import { AdminAptComplexDisplayComponent } from './apt-complex-display.component';
import { AdminAptComplexModifyComponent } from './apt-complex-modify.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgbModule,
        NgbDatepickerModule,
        ReactiveFormsModule,
        OchGridModule,
        AgGridModule.withComponents([
            NumericEditor
        ])
    ],
    declarations: [
        AdminAptComplexDisplayComponent,
        AdminAptComplexModifyComponent
    ],
    entryComponents: [
        AdminAptComplexModifyComponent
    ],
    exports: [
        AdminAptComplexDisplayComponent
    ]
})
export class AdminApartmentComplexModule {
}
