import * as _ from 'lodash';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { SearchService } from '../../search/search.service';
import { CellClickedEvent, ColDef, GridApi, GridOptions, RowClickedEvent, RowNode } from 'ag-grid-community';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OchUtil } from '../../util';
import { OchGridUtil } from '../../grid/grid-util';
import { AdminAptComplexModifyComponent, ComplexAptComplexChanges } from './apt-complex-modify.component';
import { SpringboardService } from '../../springboard/springboard.service';
import { ListingContact } from '../../models/listing-contact';
import { ListingPicture } from '../../models/listing-picture';
import { NgxSpinnerService } from 'ngx-spinner';
import { ComplexApt } from '../../models/complex-apt';
import { forkJoin } from 'rxjs';

export const MODAL_ACTIONS = {
    SAVE: 'save',
    REVERT: 'revert',
    DISMISS: 'dismiss'
};

interface ToAdd {
    complexes: Array<ComplexApt>;
    pictures: Array<ListingPicture>;
}

interface ToDelete {
    complexes: Array<number>;
    pictures: Array<number>;
}

@Component({
    encapsulation: ViewEncapsulation.None,
    selector: 'app-admin-apt-complex-display',
    templateUrl: 'apt-complex-display.component.html',
    styleUrls: [
        'apt-complex-display.component.css'
    ]
})
export class AdminAptComplexDisplayComponent implements OnInit {
    public complexes: Array<ComplexApt> = [];
    public originalAptComplexes: Readonly<Array<ComplexApt>>;
    public differences: Array<Partial<ComplexApt>> = [];

    private gridApi: GridApi;
    private gridColumnApi: any;
    private allContacts: Array<ListingContact> = [];

    private toAdd: ToAdd = {
        complexes: [],
        pictures: [],
    };

    private toDelete: ToDelete = {
        complexes: [],
        pictures: []
    };

    private defaultColumnDefinition: ColDef = {
        sortable: true,
        filter: true,
        editable: false,
        width: 150
    };

    public gridOptions: GridOptions = {};

    public readyToLoadPage = false;

    constructor(readonly modalService: NgbModal,
                readonly spinner: NgxSpinnerService,
                readonly springboardService: SpringboardService,
                readonly searchService: SearchService) {
    }

    ngOnInit(): void {
        this.spinner.show();
        this.gridOptions = this.getGridOptions();
        this.loadGridData()
            .then(() => {
                this.readyToLoadPage = true;
                this.spinner.hide();
            })
            .catch((e) => {
                this.spinner.hide();
                this.readyToLoadPage = true;
                console.error('Failed to load grid data');
                console.error(e);
            });
    }

    onAddNewListingClick(): void {
        const modal = this.modalService.open(AdminAptComplexModifyComponent, {
            ariaLabelledBy: 'modal-basic-title',
            windowClass: 'listing-modify-modal-window-class'
        });
        modal.componentInstance.aptComplex = new ComplexApt();
        modal.componentInstance.contacts = this.allContacts;
        modal.componentInstance.isEditable = true;
        modal.componentInstance.onSubmitModifications.subscribe((changes: ComplexAptComplexChanges) => {
            const listing = changes.aptComplex;
            let randomId = OchUtil.generateRandomId();
            const currentIds = this.complexes.map(l => l.aptComplex.listingId);
            while (_.includes(currentIds, randomId)) {
                randomId = OchUtil.generateRandomId();
            }
            listing.aptComplex.listingId = randomId;
            changes.aptComplex = ComplexApt.populateFromRecord(listing);
            // For a new listing, the id is -1, this way they all keep their IDs
            // We need to update the listingId after the listing has been successfully inserted
            changes.picturesToAdd = changes.picturesToAdd.map(pic => {
                return ListingPicture.populateFromRecord({
                    ...pic,
                    listingId: changes.aptComplex.aptComplex.listingId
                });
            });
            return this.onListingAdded(changes);
        });
        modal.result.then(() => {
            console.log('Legit close');
        }, (action: string) => {
        });

    }

    loadGridData(): Promise<any> {
        const self = this;
        return this.searchService.getAllAptComplexes()
            .then((results: Array<ComplexApt>) => {
                self.complexes = results.map(r => ComplexApt.populateFromRecord(r));
                self.originalAptComplexes = _.cloneDeep(self.complexes);
                self.resetLocalTrackers();
            })
            .then(() => {
                return self.searchService.getAllContacts()
                    .then((contacts: Array<ListingContact>) => {
                        self.allContacts = contacts;
                    });
            })
            .catch(console.error);
    }

    onGridReady(params: any) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    }

    onListingAdded(changes: ComplexAptComplexChanges) {
        console.log('New listing created is', changes);
        // We never really take into account picture Ids being deleted for new complexes
        // as they don't yet exist in the DB
        this.toAdd.complexes.push(changes.aptComplex);
        this.toAdd.pictures.push(...changes.picturesToAdd);
        this.complexes.push(changes.aptComplex);
        this.gridApi.updateRowData({
            add: [changes.aptComplex]
        });
    }

    onListingModified(event: RowClickedEvent, changes: ComplexAptComplexChanges) {
        console.log('Listing modified as follows', changes);
        if (this.isListingToBeAdded(changes.aptComplex.aptComplex.listingId)) {
            _.remove(this.toAdd.complexes, (l) => {
                return l.aptComplex.listingId === changes.aptComplex.aptComplex.listingId;
            });
            this.toAdd.complexes.push(changes.aptComplex);
        }
        this.toAdd.pictures.push(...changes.picturesToAdd);
        this.toDelete.pictures.push(...changes.pictureIdsToDelete);
        event.node.setData(changes.aptComplex);
        this.computeListingDetailDifferences();
    }

    computeListingDetailDifferences(): Array<Partial<ComplexApt>> {
        const self = this;
        self.complexes = [];
        self.gridApi.forEachNode((row: RowNode, idx: number) => {
            self.complexes.push(row.data);
        });
        const difference: Array<Partial<ComplexApt>> = OchUtil
            .deepDifference(self.complexes, self.originalAptComplexes)
            .filter((cl: ComplexApt) => {
                return (cl.aptComplex !== undefined || cl.listingLocation !== undefined)
                    && !this.isListingToBeAdded(cl.aptComplex.listingId);
            });

        if (difference.length) {
            this.differences = difference;
        }
        return difference;
    }

    isThereAChange(): boolean {
        return (
            this.differences.length > 0 ||
            this.toAdd.complexes.length > 0 ||
            this.toDelete.complexes.length > 0 ||
            this.toAdd.pictures.length > 0 ||
            this.toDelete.pictures.length > 0
        );
    }

    onSaveChangesClick(content: any) {
        const self = this;
        self.computeListingDetailDifferences();
        if (this.isThereAChange()) {
            this.modalService
                .open(content, { ariaLabelledBy: 'modal-basic-title' })
                .result
                .then(() => {
                    this.spinner.show();
                    const complexesToUpdate = self.differences
                        .map((diff: Partial<ComplexApt>) => {
                            let listingId;
                            if (diff.aptComplex) {
                                listingId = diff.aptComplex.listingId;
                            } else if (diff.listingLocation) {
                                listingId = diff.listingLocation.listingId;
                            }
                            if (!self.isListingToBeAdded(listingId)) {
                                return _.find(self.complexes, (c: ComplexApt) => {
                                    return c.aptComplex.listingId === listingId;
                                });
                            }
                        })
                        .filter(l => l !== undefined);

                    const listingIdsToDelete = self.toDelete.complexes
                        .map((id: number) => {
                            if (!self.isListingToBeAdded(id)) {
                                return id;
                            }
                        })
                        .filter(l => l !== undefined);

                    const complexesToAdd = self.toAdd.complexes
                        .map((listing: ComplexApt) => {
                            if (!_.includes(self.toDelete.complexes, listing.aptComplex.listingId)) {
                                // Bundle new listing pics with the new listing
                                const pictures = self.toAdd.pictures.filter((pic) => pic.listingId === listing.aptComplex.listingId);
                                const remainingPictures = self.toAdd.pictures.filter((pic) => pic.listingId !== listing.aptComplex.listingId);
                                listing.listingPictures = pictures;
                                self.toAdd.pictures = remainingPictures;
                                return listing;
                            }
                        })
                        .filter(l => l !== undefined);

                    const picturesToAdd = self.toAdd.pictures.map(p => p);
                    const pictureIdsToDelete = self.toDelete.pictures.map(i => i);

                    // Resetting these values as they control how the buttons on the page work
                    self.resetLocalTrackers();

                    console.log('Listings to update', complexesToUpdate);
                    console.log('Listing Ids to delete', listingIdsToDelete);
                    console.log('Listings to add', complexesToAdd);

                    return Promise
                        .all(
                            [
                                ...complexesToUpdate.map(l => self.searchService.updateAptComplex(l)),
                                ...complexesToAdd.map(l => self.searchService.addNewAptComplex(l)),
                                ...picturesToAdd.map(p => self.searchService.insertImage(p).toPromise()),
                                ...pictureIdsToDelete.map(id => self.searchService.deletePicture(id).toPromise()),
                                ...listingIdsToDelete.map(id => {
                                    return forkJoin(
                                        self.searchService.deleteAptComplex(id),
                                        self.searchService.deleteLocation(id)
                                    ).toPromise();
                                }),
                            ] as any[]
                        )
                        .then(() => {
                            this.spinner.hide();
                            return this.loadGridData();
                        });
                }, (action: string) => {
                    if (action === 'revert') {
                        console.error('Revert');
                        self.complexes = _.cloneDeep(self.originalAptComplexes);
                        self.resetLocalTrackers();
                    } else if (action === 'dismiss') {
                        return undefined;
                    }
                });
        }
    }

    getGridOptions(): GridOptions {
        const self = this;
        const onDeleteRowClick = (params: any) => {
            const currentRow: ComplexApt = params.data;
            if (!_.includes(self.toDelete.complexes, currentRow.aptComplex.listingId)) {
                console.warn(`Scheduling listing id: ${ currentRow.aptComplex.listingId } for deletion`);
                self.toDelete.complexes.push(currentRow.aptComplex.listingId);
            } else {
                console.warn(`Not deleting listing id: ${ currentRow.aptComplex.listingId }`);
                _.remove(self.toDelete.complexes, (id: number) => {
                    return id === currentRow.aptComplex.listingId;
                })
            }
            self.gridApi.redrawRows();
        };
        return {
            columnDefs: [
                {
                    headerName: '',
                    onCellClicked: onDeleteRowClick,
                    width: 50,
                    cellRenderer: (params: any) => {
                        const currentRow: ComplexApt = params.data;
                        if (!_.includes(self.toDelete.complexes, currentRow.aptComplex.listingId)) {
                            return `<a class="fa fa-trash"></a>`
                        } else {
                            return `<a class="fa fa-recycle"></a>`
                        }
                    }
                },
                {
                    headerName: 'Listing ID',
                    field: 'aptComplex.listingId',
                    editable: false,
                    sortable: false,
                    filter: false
                },
                { headerName: 'Active Listing?', field: 'aptComplex.listingActive' },
                { headerName: 'Name', field: 'aptComplex.apartmentName' },
                { headerName: 'Address', field: 'aptComplex.apartmentAddress' },
                { headerName: 'URL', field: 'aptComplex.apartmentUrl' },
            ],
            defaultColDef: {
                ...self.defaultColumnDefinition,
                onCellClicked(event: CellClickedEvent): void {
                    const currentRow: ComplexApt = event.data;
                    const cl = _.find(self.complexes, (cl: ComplexApt) => {
                        return cl.aptComplex.listingId === currentRow.aptComplex.listingId;
                    });
                    if (cl) {
                        cl.listingPictures = self.getListingPictures(cl.aptComplex.listingId);
                        const modal = self.modalService.open(AdminAptComplexModifyComponent, {
                            ariaLabelledBy: 'modal-basic-title',
                            windowClass: 'listing-modify-modal-window-class'
                        });
                        modal.componentInstance.aptComplex = cl;
                        modal.componentInstance.contacts = self.allContacts;
                        modal.componentInstance.isEditable = true;
                        modal.componentInstance.onSubmitModifications.subscribe((changes: ComplexAptComplexChanges) => {
                            return self.onListingModified(event, changes);
                        });
                        modal.result.then(() => {
                            console.log('Legit close');
                        }, (action: string) => {
                        });
                    } else {
                        console.error(`Cannot find complex listing with id ${ currentRow.aptComplex.listingId }`);
                    }
                }
            },
            rowClassRules: {
                'mark-for-deletion': (params: any) => {
                    const listingId: number = params.data.aptComplex.listingId;
                    return _.includes(self.toDelete.complexes, listingId);
                }
            }
        };
    }

    resetLocalTrackers(): void {
        this.differences = [];
        this.toAdd.complexes = [];
        this.toAdd.pictures = [];
        this.toDelete.complexes = [];
        this.toDelete.pictures = [];
    }

    isListingToBeAdded(listingId: number) {
        const found = _.find(this.toAdd.complexes, (l: ComplexApt) => {
            return l.aptComplex.listingId === listingId;
        });
        // This means that the current listing is NOT a newly added row
        return !found ? false : true;
    }

    openReadonlyModalFromId(listingId: string) {
        const found = _.find(this.complexes, (l: ComplexApt) => {
            return l.aptComplex.listingId === Number(listingId);
        });
        if (found) {
            this.openReadonlyModalFor(found);
        } else {
            console.warn(`No listing found with listing id ${ listingId }`);
        }
    }

    openReadonlyModalFor(listing: ComplexApt) {
        const pictures = this.getListingPictures(listing.aptComplex.listingId);
        const updatedListing = _.cloneDeep(listing);
        updatedListing.listingPictures.push(...pictures);
        const modal = this.modalService.open(AdminAptComplexModifyComponent, {
            ariaLabelledBy: 'modal-basic-title',
            windowClass: 'listing-modify-modal-window-class'
        });
        modal.componentInstance.aptComplex = updatedListing;
        modal.componentInstance.isEditable = false;
        modal.result.then(() => {
            console.log('Legit close');
        }, (action: string) => {
        });
    }

    getListingPictures(listingId: number): Array<ListingPicture> {
        const apt: ComplexApt = _.find(this.complexes, (ca: ComplexApt) => {
            return ca.aptComplex.listingId === listingId;
        });
        const pics: Array<ListingPicture> = [];
        if (apt) {
            pics.push(...apt.listingPictures);
        }
        return pics
            .concat(...this.toAdd.pictures.filter(p => p.listingId === listingId))
            .filter(p => !_.includes(this.toDelete.pictures, p.imageId));
    }

    // Todo: Fix this to include deleted pic listings as well
    computeChangedListings(): Array<ComplexApt> {
        return _.uniqBy(this.differences
                .map(diff => _.find(this.complexes, (ca) => ca.aptComplex.listingId === diff.aptComplex.listingId))
                .concat(...this.toAdd.pictures.map(pic => _.find(this.complexes, ca => ca.aptComplex.listingId === pic.listingId))),
            (ca: ComplexApt) => ca.aptComplex.listingId);
        // .concat(...this.toDelete.pictures.map(pic => _.find(this.complexes, ca => ca.aptComplex.listingId === pic))
    }
}
