import * as _ from 'lodash';
import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { OchUtil } from '../../util';
import { ENDPOINTS, SUPPORTED_IANA_FORMATS } from '../../constants';
import { Router } from '@angular/router';
import { ListingContact } from '../../models/listing-contact';
import { ListingPicture } from '../../models/listing-picture';
import { LAT_LON_INVALID_VALUE, LatLon, MapService } from '../../search/map.service';
import { Listing } from '../../models/listing';
import { ListingLocation } from '../../models/listing-location';
import { ApartmentComplex } from '../../models/apartment-complex';
import { ComplexApt } from '../../models/complex-apt';

export interface ComplexAptComplexChanges {
    aptComplex: ComplexApt;
    picturesToAdd: Array<ListingPicture>;
    pictureIdsToDelete: Array<number>;
    listingLocation: ListingLocation;
}

@Component({
    // Need this to override bootstrap CSS
    encapsulation: ViewEncapsulation.None,
    selector: 'app-admin-apt-complex-modify',
    templateUrl: 'apt-complex-modify.component.html',
    styleUrls: [
        'apt-complex-modify.component.css'
    ]
})
export class AdminAptComplexModifyComponent implements OnInit {
    @Input()
    public aptComplex: ComplexApt;

    @Input()
    public contacts: Array<ListingContact> = [];

    @Input()
    public isEditable: boolean = false;

    @Input()
    public isContactEditable: boolean = false;

    @Output()
    public onSubmitModifications = new EventEmitter<ComplexAptComplexChanges>();

    public aptComplexModifyForm: FormGroup;

    private originalAptComplexPictures: Array<ListingPicture> = [];
    private picturesToAdd: Array<ListingPicture> = [];
    private pictureIdsToDelete: Array<number> = [];

    public supportedImageFormats: string = SUPPORTED_IANA_FORMATS.join(',');

    constructor(readonly router: Router,
                readonly mapService: MapService,
                readonly activeModal: NgbActiveModal) {
    }

    isWindowWidthSmallerThanDesktop(): boolean {
        return OchUtil.isWindowWidthSmallerThanDesktop();
    }

    ngOnInit(): void {
        this.aptComplexModifyForm = this.createFormGroup();
        this.originalAptComplexPictures = _.cloneDeep(this.aptComplex.listingPictures);
    }

    /**
     * Disable submit when:
     * 1. Form is pristine, or Form is invalid
     * 2. Images have not been changed
     */
    shouldSubmitButtonBeDisabled(): boolean {
        const haveImagesChanged = !_.isEqual(this.originalAptComplexPictures, this.getListingPictures());
        let retVal = true;
        if (!this.aptComplexModifyForm.pristine) {
            // If form is invalid
            retVal = !this.aptComplexModifyForm.valid;
        }
        if (haveImagesChanged) {
            retVal = false;
        }
        return retVal;
    }

    /**
     * Disable revert when:
     * 1. Form is pristine
     * 2. Images have not changed
     */
    shouldRevertButtonBeDisabled(): boolean {
        const haveImagesChanged = !_.isEqual(this.originalAptComplexPictures, this.getListingPictures());
        let retVal = true;
        if (!this.aptComplexModifyForm.pristine) {
            retVal = false;
        }
        if (haveImagesChanged) {
            retVal = false;
        }
        return retVal;
    }

    getListingPictures(): Array<ListingPicture> {
        return this.aptComplex.listingPictures
            .concat(...this.picturesToAdd)
            .filter((pic: ListingPicture) => !_.includes(this.pictureIdsToDelete, pic.imageId));
    }

    onSubmitClick(): void {
        const modified = ComplexApt.populateFromRecord({
            ...this.aptComplex,
            aptComplex: {
                ...this.aptComplexModifyForm.controls.listing.value
            },
            listingContact: {
                ...this.aptComplexModifyForm.controls.listingContact.value,
                // Cannot pick this up from the form as Angular then goes wacky
                // and does not include the rest of the fields
                contactId: this.aptComplexModifyForm.controls.listing.value.contactDetailsId
            },
            listingLocation: {
                ...this.aptComplexModifyForm.controls.listingLocation.value,
                listingId: this.aptComplexModifyForm.controls.listing.value.listingId
            }
            // We do not add pictures, as we do not want them to be unnecessarily updated
        });
        const changes: ComplexAptComplexChanges = {
            aptComplex: modified,
            picturesToAdd: this.picturesToAdd,
            pictureIdsToDelete: this.pictureIdsToDelete,
            listingLocation: modified.listingLocation
        };
        this.onSubmitModifications.emit(changes);
        this.activeModal.close();
    }

    onRevertClick(): void {
        if (!this.aptComplexModifyForm.pristine) {
            this.aptComplexModifyForm.reset(this.aptComplex);
        }
        this.aptComplex.listingPictures = this.originalAptComplexPictures;
        this.picturesToAdd = [];
    }

    onGoToListingClick(): Promise<boolean> {
        const listingId: number = this.aptComplex.aptComplex.listingId;
        this.activeModal.close();
        return this.router.navigateByUrl(`/listing/${ listingId }`);
    }

    onGoToAdminContactClick() {
        this.activeModal.close();
        return this.router.navigateByUrl(ENDPOINTS.ADMIN.CONTACT);
    }

    onDeletePictureClick(imageId: string) {
        const id = Number(imageId);
        this.pictureIdsToDelete.push(id);
    }

    onImageFileSelected(event: Event) {
        const files: FileList = (event.target as HTMLInputElement).files;
        const filtered = _.filter(files, (fi: File) => {
            return _.includes(SUPPORTED_IANA_FORMATS, fi.type);
        });
        const promises = _.map(filtered, (fi: File) => {
            return OchUtil.readImageFile(fi);
        });
        Promise.all(promises)
            .then((sources: string[]) => {
                const pictures = sources.map(src => {
                    return ListingPicture.populateFromRecord({
                        imageId: OchUtil.generateRandomId(),
                        listingId: this.aptComplex.aptComplex.listingId,
                        imageUrl: src,
                        imageDescription: 'New image'
                    })
                });
                this.picturesToAdd.push(...pictures);
            });
    }

    onSelectContact(contactId: string) {
        const contact: ListingContact = _.find(this.contacts, { contactId: Number(contactId) });
        if (contact) {
            this.aptComplexModifyForm.get('listing.contactDetailsId').setValue(contact.contactId);
            this.aptComplexModifyForm.get('listingContact.name').setValue(contact.name);
            this.aptComplexModifyForm.get('listingContact.primaryPhone').setValue(contact.primaryPhone);
            this.aptComplexModifyForm.get('listingContact.secondaryPhone').setValue(contact.secondaryPhone);
            this.aptComplexModifyForm.get('listingContact.email').setValue(contact.email);
            // Required as Angular does not count forms as dirty if they are changed from the backend
            this.aptComplexModifyForm.get('listing').markAsDirty();
        }
    }

    createFormGroup(): FormGroup {
        const fg = new FormGroup({
            listing: new FormGroup({
                listingId: new FormControl({
                    value: this.aptComplex.aptComplex.listingId,
                    disabled: !this.isEditable
                }, [Validators.required]),
                listingActive: new FormControl({
                    value: this.aptComplex.aptComplex.listingActive,
                    disabled: !this.isEditable
                }, [Validators.required]),
                contactDetailsId: new FormControl({
                    value: this.aptComplex.aptComplex.contactDetailsId,
                    disabled: !this.isEditable
                }, [Validators.required]),
                apartmentName: new FormControl({
                    value: this.aptComplex.aptComplex.apartmentName,
                    disabled: !this.isEditable
                }, [Validators.required]),
                apartmentAddress: new FormControl({
                    value: this.aptComplex.aptComplex.apartmentAddress,
                    disabled: !this.isEditable
                }, [Validators.required]),
                apartmentDescription: new FormControl({
                    value: this.aptComplex.aptComplex.apartmentDescription,
                    disabled: !this.isEditable
                }, [Validators.required]),
                apartmentUrl: new FormControl({
                    value: this.aptComplex.aptComplex.apartmentUrl,
                    disabled: !this.isEditable
                }, [Validators.required]),
                displayOrder: new FormControl({
                    value: this.aptComplex.aptComplex.displayOrder,
                    disabled: !this.isEditable
                }, [Validators.required]),
            }),
            listingContact: new FormGroup({
                name: new FormControl({
                    value: this.aptComplex.listingContact.name,
                    disabled: !this.isContactEditable
                }, [Validators.required]),
                primaryPhone: new FormControl({
                    value: this.aptComplex.listingContact.primaryPhone,
                    disabled: !this.isContactEditable
                }),
                secondaryPhone: new FormControl({
                    value: this.aptComplex.listingContact.secondaryPhone,
                    disabled: !this.isContactEditable
                }),
                email: new FormControl({
                    value: this.aptComplex.listingContact.email,
                    disabled: !this.isContactEditable
                })
            }),
            listingLocation: new FormGroup({
                latitude: new FormControl({
                    value: this.aptComplex.listingLocation.latitude,
                    disabled: true
                }, [Validators.required]),
                longitude: new FormControl({
                    value: this.aptComplex.listingLocation.longitude,
                    disabled: true
                }, [Validators.required])
            })
        });
        return fg;
    }

    geocodeAddress(complex: ApartmentComplex): Promise<LatLon> {
        return this.mapService.geocodeAddress(complex.apartmentAddress)
            .then((latLon: LatLon) => {
                this.aptComplexModifyForm.get('listingLocation.latitude').setValue(latLon.latitude);
                this.aptComplexModifyForm.get('listingLocation.longitude').setValue(latLon.longitude);
                // Required as Angular does not count forms as dirty if they are changed from the backend
                this.aptComplexModifyForm.get('listing').markAsDirty();
                return latLon;
            })
    }

    // Todo: Use same formatters
    formatAddressHref(listing: ComplexApt): string {
        const gmapUrl = `https://maps.google.com/maps?q=`;
        let latitude = listing.listingLocation.latitude;
        let longitude = listing.listingLocation.longitude;
        if (latitude === LAT_LON_INVALID_VALUE) {
            latitude = this.aptComplexModifyForm.get('listingLocation.latitude').value;
        }
        if (longitude === LAT_LON_INVALID_VALUE) {
            longitude = this.aptComplexModifyForm.get('listingLocation.longitude').value;
        }
        return `${ gmapUrl }${ latitude },${ longitude }`;
    }
}
