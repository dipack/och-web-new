import * as _ from 'lodash';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { SearchService } from '../../search/search.service';
import { CellClickedEvent, ColDef, GridApi, GridOptions, RowClickedEvent, RowNode } from 'ag-grid-community';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OchUtil } from '../../util';
import { ListingContact } from '../../models/listing-contact';
import { ContactModifyComponent } from './contact-modify.component';
import { NgxSpinnerService } from 'ngx-spinner';

// export const MODAL_ACTIONS = {
//     SAVE: 'save',
//     REVERT: 'revert',
//     DISMISS: 'dismiss'
// };

@Component({
    encapsulation: ViewEncapsulation.None,
    selector: 'app-admin-contact-display',
    templateUrl: 'contact-display.component.html',
    styleUrls: [
        'contact-display.component.css'
    ]
})
export class AdminContactDisplayComponent implements OnInit {
    public contacts: Array<ListingContact> = [];
    public originalContacts: Readonly<Array<ListingContact>>;
    public differences: Array<Partial<ListingContact>> = [];
    public contactIdsToDelete: Array<number> = [];
    public contactsToAdd: Array<ListingContact> = [];

    private gridApi: GridApi;
    private gridColumnApi: any;

    private defaultColumnDefinition: ColDef = {
        sortable: true,
        filter: true,
        editable: false,
        width: 150
    };

    public gridOptions: GridOptions = {};

    public readyToLoadPage = false;

    constructor(readonly modalService: NgbModal,
                readonly spinner: NgxSpinnerService,
                readonly searchService: SearchService) {
    }

    ngOnInit(): void {
        this.spinner.show();
        this.gridOptions = this.getGridOptions();
        this.loadGridData()
            .then(() => {
                this.readyToLoadPage = true;
                this.spinner.hide();
            })
            .catch((e) => {
                this.readyToLoadPage = true;
                this.spinner.hide();
                console.error('Failed to load grid data');
                console.error(e);
            });
    }

    onAddNewContactClick(): void {
        const modal = this.modalService.open(ContactModifyComponent, { ariaLabelledBy: 'modal-basic-title' });
        modal.componentInstance.contact = new ListingContact();
        modal.componentInstance.isEditable = true;
        modal.componentInstance.onSubmitModifications.subscribe((newContact: ListingContact) => {
            return this.onContactAdded(newContact);
        });
        modal.result.then(() => {
            console.log('Legit close');
        }, (action: string) => {
        });

    }

    loadGridData(): Promise<any> {
        const self = this;
        return this.searchService.getAllContacts()
            .then((results: Array<ListingContact>) => {
                    self.contacts = results.map(r => ListingContact.populateFromRecord(r));
                    self.originalContacts = _.cloneDeep(self.contacts);
                    self.contactIdsToDelete = [];
                },
                console.error);
    }

    onGridReady(params: any) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    }

    onContactAdded(contact: ListingContact) {
        console.log('New contact created is', contact);
        this.contactsToAdd.push(contact);
        this.contacts.push(contact);
        this.gridApi.updateRowData({
            add: [contact]
        });
    }

    onContactModified(event: RowClickedEvent, contact: ListingContact) {
        console.log('Contact modified as follows', contact);
        event.node.setData(contact);
        this.computeDifferences();
    }

    computeDifferences(): Array<Partial<ListingContact>> {
        const self = this;
        self.contacts = [];
        self.gridApi.forEachNode((row: RowNode, idx: number) => {
            self.contacts.push(row.data);
        });
        const difference: Array<Partial<ListingContact>> = OchUtil
            .deepDifference(self.contacts, self.originalContacts)
            .filter((c: ListingContact) => !this.isContactToBeAdded(c.contactId));
        console.log('Differences', difference);
        if (difference.length) {
            this.differences = difference;
        }
        return difference;
    }

    onSaveChangesClick(content: any) {
        const self = this;
        const differences = self.computeDifferences();
        if (differences.length || self.contactIdsToDelete.length || self.contactsToAdd.length) {
            this.modalService
                .open(content, { ariaLabelledBy: 'modal-basic-title' })
                .result
                .then(() => {
                    console.log('Saving changes');
                    const contactsToUpdate = self.differences
                        .map((diff: Partial<ListingContact>) => {
                            if (!self.isContactToBeAdded(diff.contactId)) {
                                return _.find(self.contacts, (c: ListingContact) => {
                                    return c.contactId === diff.contactId;
                                });
                            }
                        })
                        .filter(l => l !== undefined);

                    const contactIdsToDelete = self.contactIdsToDelete
                        .map((id: number) => {
                            if (!self.isContactToBeAdded(id)) {
                                return id;
                            }
                        })
                        .filter(l => l !== undefined);

                    const contactsToAdd = self.contactsToAdd
                        .map((contact: ListingContact) => {
                            if (!_.includes(self.contactIdsToDelete, contact.contactId)) {
                                return contact;
                            }
                        })
                        .filter(l => l !== undefined);

                    // Resetting these values as they control how the buttons on the page work
                    self.differences = [];
                    self.contactIdsToDelete = [];
                    self.contactsToAdd = [];

                    console.log('Contacts to update', contactsToUpdate);
                    console.log('Contact Ids to delete', contactIdsToDelete);
                    console.log('Contacts to add', contactsToAdd);

                    return Promise
                        .all(
                            [
                                ...contactsToUpdate.map(l => self.searchService.updateContact(l).toPromise()),
                                ...contactIdsToDelete.map(id => self.searchService.deleteContact(id).toPromise()),
                                ...contactsToAdd.map(l => self.searchService.insertContact(l).toPromise() as Promise<any>)
                            ]
                        )
                        .then(() => self.loadGridData());
                }, (action: string) => {
                    if (action === 'revert') {
                        console.error('Revert');
                        self.contacts = _.cloneDeep(self.originalContacts);
                        self.differences = [];
                    } else if (action === 'dismiss') {
                        return undefined;
                    }
                });
        }
    }

    getGridOptions(): GridOptions {
        const self = this;
        const onDeleteRowClick = (params: any) => {
            const currentRow: ListingContact = params.data;
            if (!_.includes(self.contactIdsToDelete, currentRow.contactId)) {
                console.warn(`Scheduling listing id: ${ currentRow.contactId } for deletion`);
                self.contactIdsToDelete.push(currentRow.contactId);
            } else {
                console.warn(`Not deleting listing id: ${ currentRow.contactId }`);
                _.remove(self.contactIdsToDelete, (id: number) => {
                    return id === currentRow.contactId;
                })
            }
            self.gridApi.redrawRows();
        };
        return {
            columnDefs: [
                {
                    headerName: '',
                    onCellClicked: onDeleteRowClick,
                    width: 50,
                    cellRenderer: (params: any) => {
                        const currentRow: ListingContact = params.data;
                        if (!_.includes(self.contactIdsToDelete, currentRow.contactId)) {
                            return `<a class="fa fa-trash"></a>`
                        } else {
                            return `<a class="fa fa-recycle"></a>`
                        }
                    }
                },
                {
                    headerName: 'Contact ID',
                    field: 'contactId',
                    editable: false,
                    sortable: false,
                    filter: false
                },
                { headerName: 'Contact Name', field: 'name' },
                { headerName: 'Primary Phone', field: 'primaryPhone' },
                { headerName: 'Secondary Phone', field: 'secondaryPhone' },
                { headerName: 'Email Address', field: 'email' }
            ],
            defaultColDef: {
                ...self.defaultColumnDefinition,
                onCellClicked(event: CellClickedEvent): void {
                    const currentRow: ListingContact = event.data;
                    const listingContact = _.find(self.contacts, (cl: ListingContact) => {
                        return cl.contactId === currentRow.contactId;
                    });
                    if (listingContact) {
                        const modal = self.modalService.open(ContactModifyComponent, { ariaLabelledBy: 'modal-basic-title' });
                        modal.componentInstance.contact = listingContact;
                        modal.componentInstance.isEditable = true;
                        modal.componentInstance.onSubmitModifications.subscribe((modifiedContact: ListingContact) => {
                            return self.onContactModified(event, modifiedContact);
                        });
                        modal.result.then(() => {
                            console.log('Legit close');
                        }, (action: string) => {
                        });
                    } else {
                        console.error(`Cannot find complex listing with id ${ currentRow.contactId }`);
                    }
                }
            },
            rowClassRules: {
                'mark-for-deletion': (params: any) => {
                    const contactId: number = params.data.contactId;
                    return _.includes(self.contactIdsToDelete, contactId);
                }
            }
        };
    }

    isContactToBeAdded(contactId: number) {
        const found = _.find(this.contactsToAdd, (l: ListingContact) => {
            return l.contactId === contactId;
        });
        // This means that the current contact is NOT a newly added row
        return !found ? false : true;
    }

    openReadonlyModalFromId(contactId: string) {
        const found = _.find(this.contacts, (l: ListingContact) => {
            return l.contactId === Number(contactId);
        });
        if (found) {
            this.openReadonlyModalFor(found);
        } else {
            console.warn(`No contact found with id ${ contactId }`);
        }
    }

    openReadonlyModalFor(contact: ListingContact) {
        const modal = this.modalService.open(ContactModifyComponent, {
            ariaLabelledBy: 'modal-basic-title',
        });
        modal.componentInstance.contact = contact;
        modal.componentInstance.isEditable = false;
        modal.result.then(() => {
            console.log('Legit close');
        }, (action: string) => {
        });
    }
}
