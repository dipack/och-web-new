import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AdminContactDisplayComponent } from './contact-display.component';
import { ContactModifyComponent } from './contact-modify.component';
import { OchGridModule } from '../../grid/module';
import { AgGridModule } from 'ag-grid-angular';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgbModule,
        ReactiveFormsModule,
        OchGridModule,
        AgGridModule.withComponents([])
    ],
    declarations: [
        AdminContactDisplayComponent,
        ContactModifyComponent
    ],
    entryComponents: [
        ContactModifyComponent
    ],
    exports: [
        AdminContactDisplayComponent,
        ContactModifyComponent
    ]
})
export class AdminContactModule {
}
