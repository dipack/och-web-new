import * as _ from 'lodash';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ComplexListing } from '../../models/complex-listing';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal, NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { UnitType } from '../../models/unit-type';
import { Locality } from '../../models/locality';
import { OchUtil } from '../../util';
import { DateTime } from 'luxon';
import { DATETIME_FORMAT } from '../../constants';
import { Router } from '@angular/router';
import { ListingContact } from '../../models/listing-contact';
import { VALID } from '@angular/forms/src/model';

@Component({
    selector: 'app-contact-modify',
    templateUrl: 'contact-modify.component.html',
    styleUrls: [
        'contact-modify.component.css'
    ]
})
export class ContactModifyComponent implements OnInit {
    @Input()
    public contact: ListingContact;

    @Input()
    public isEditable: boolean = false;

    @Output()
    public onSubmitModifications = new EventEmitter<ListingContact>();

    public contactModifyForm: FormGroup;

    constructor(readonly router: Router,
                readonly activeModal: NgbActiveModal) {
    }

    ngOnInit(): void {
        if (this.contact) {
            // Break the link between the input and output object
            // this.listing = _.cloneDeep(this.listing);
        }
        this.contactModifyForm = this.createFormGroup();
    }

    onSubmitClick(): void {
        const modified: ListingContact = this.contactModifyForm.value;
        this.onSubmitModifications.emit(modified);
        this.activeModal.close();
    }

    onRevertClick(): void {
        this.contactModifyForm.reset(this.contact);
    }


    createFormGroup(): FormGroup {
        const fg = new FormGroup({
            contactId: new FormControl({
                value: this.contact.contactId,
                disabled: !this.isEditable
            }, [Validators.required]),
            name: new FormControl({
                value: this.contact.name,
                disabled: !this.isEditable
            }, [Validators.required]),
            primaryPhone: new FormControl({
                value: this.contact.primaryPhone,
                disabled: !this.isEditable
            }, [Validators.required]),
            secondaryPhone: new FormControl({
                value: this.contact.secondaryPhone,
                disabled: !this.isEditable
            }, [Validators.required]),
            email: new FormControl({
                value: this.contact.email,
                disabled: !this.isEditable
            }, [Validators.required])
        });
        return fg;
    }
}
