## Contact Us
![Harriman Hall](/och/resources/images/harriman.jpg)
#### SBI Off-Campus Housing
135 Diefendorf Hall

Buffalo, NY 14214

Phone: (716) 829-2224

Fax: (716) 829-2232

[ochstaff@buffalo.edu](mailto:ochstaff@buffalo.edu)
#### SBI Off-Campus Office Hours
#### Closed Aug 21st - Aug 25th
#### Email all inquires
We are open after Labor Day

Call for Appointment

Phone: (716) 829-2224

* [OCH Directory](http://subboard.com/och/resources.asp#directory)
