## Create Posting
To post your apartment, fill out the listing form by clicking the link below.
Once your student status is verified (first listing free/students only) or
payment is received, your listing will be available for **30 days** on the OCH
website and office.

#### Public Notice
All housing listed on this site is available on an equal opportunity basis and
postings which include references to race, color, religion, national origin,
disability, familial status, sex/gender, military status, marital status, age,
sexual orientation, gender identity and expression and lawful source of income
will not be permitted.
[Listing Form](terms.asp)
### Pricing

Effective 08/01/2017
#### Non-Student Rates
Each 30 Day Listing: $25 per unit

Each 90 Day Listing: $50 per unit

#### Student Rates
Listing: Free
#### [Apartment Complex Listings](apartment_listings_faq.asp)

30 Day Listing: $250

6 Month Listing: $200/month with signed contract

1 Year Listing: $150/month with signed contract
### How to Pay
We now accept the following methods of payment:
* Credit card payments via PayPal through this web site.

* Check or money order payment through the mail.

* Payment in person.
[Online payment page](pay.asp) (you will need your listing ID number)


Please do not send cash in the mail.
### Apartment Removal Protocol

Off Campus Housing reserves the right to remove rental listings.

* All listings found to misrepresent their compliance with state and local mandates will be removed from the Off-Campus Housing website immediately.

* Upon removal, a notification will be sent to the landlord listing the property.

* The removed property may not be listed again until the landlord presents a valid certificate of occupancy with an approved date no later than the date the property was removed from the Off-Campus Housing website.


### Apartment Scam Warning
#### Please read this apartment scam warning!
Most scams involve one or more of the following:
* Inquiry from someone far away, often in another country

* Western Union, Money Gram, cashier's check, money order, shipping, escrow service, or a guarantee

* Inability or refusal to meet face-to-face before consummating transaction

#### Example of a common Scam at OCH
* You receive an email offering to rent your apartment, sight unseen.

* Cashier's check is offered for your sale item, as a deposit for an apartment, or for just about anything else of value.

* Value of cashier's check often far exceeds your item - scammer offers to trust you, and asks you to wire the balance via money transfer service

* Banks will often cash these fake checks **and then hold you responsible when the check fails to clear** , including criminal prosecution in some cases!

* Scam often involves a third party (shipping agent, business associate owing buyer money, etc)
