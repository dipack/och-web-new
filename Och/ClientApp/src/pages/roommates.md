## Roommates
**If you already have an apartment** and would like to find a roommate(s), you
can **[post your apartment](/advertise)** for free (first listing/students
only) on the SBI Off-Campus Housing website for 90 days. Just use the
available bedrooms space to indicate how many roommates you are looking for.

**If you need an apartment** and a roommate(s), you can
**[search](/)** the SBI Off-Campus Housing website for apartments with
open bedrooms. For example, our search results might show:
Rent: $456

Bedrooms: 2

(Open): 1

Available: 07/04/2019

Area: Buffalo (Elmwood)

This could either be a student looking for a roommate, or a landlord renting
apartments out per room. By clicking the search results for apartment details,
you will find further details about the landlord or tenants as well as contact
information.

If you have any questions please [contact](/contact-us) the SBI Off-Campus
Housing Office.

#### Other resource(s)

* [Things to consider when looking for a roommate](http://www.forrent.com/apartment-tips/Find-a-Roommate.php)

### [SBI Legal Assistance](http://legalassistance.buffalo.edu/) Roommate Tips
**Roommate Tip 1:** Choose your roommates wisely: by signing your name to a
lease, you may become liable/responsible for the actions of your roommates
both criminally and with the University ([Student-Wide
Judiciary](http://www.ub-judiciary.buffalo.edu/swj.shtml)).

**Roommate Tip 2:** Some leases have a "joint and several liability" clause
meaning the entire amount of rent is due each month no matter what (e.g., your
roommate moves out or can't pay, you will be held responsible). Make sure you
find out before signing your lease and decide whether or not this is
acceptable to you.
#### "Are Roommates a Hazard?" - Legal Concerns (Rent Guidelines Board)

In today's tight housing market one way to make your income meet the rent is
to have a roommate.


* If you are the only person who has signed the lease, in addition to your
immediate family, state law allows you to have one roommate (i.e. an occupant
of the apartment who has not signed the lease). Your roommate's dependent
children are also permitted.

* Any lease provisions disallowing a roommate (and dependent children) are
illegal. If your lease originally had two or more tenants you may have an
additional roommate or roommates provided that the total number of tenants and
occupants (excluding occupant's dependent children) does not exceed the number
of tenants on the original lease. For example, if three tenants signed the
original lease and one moves out, you may have a roommate to replace the
departed tenant.


* Check your roommate's background thoroughly. If you don't get along, and your
roommate refuses to leave, you do have the right to evict your roommate.
However, if the roommate refuses to leave, the ensuing eviction process could
be both painful and expensive. Remember, until the eviction process is
complete you may have to live with this person.

* If you join someone as a roommate (i.e. your name isn't on the lease) another
set of problems can crop up. In most instances, if your roommate (the
leaseholder) leaves, you have no right to keep the apartment. The primary
tenant might also decide to temporarily sublet to someone, in which case you
will suddenly have a new house-mate not of your choosing.
##### What precautions can you take?
* If you are the leaseholder, be careful about choosing a roommate.

* If you join someone as a "roommate," try to find out as much as possible about
the primary tenants' plans. Try to "get on the lease" if possible. If this
isn't possible, your rights are very limited and your ability to stay in the
apartment may be cut short at any time.

##### Can I get my unpleasant roommate's name off the lease we both signed?

* If your roommate is named on the lease, s/he is technically a co-tenant, and
has the same rights as you do to the apartment.
Above text quoted directly from the NYC Rent Guidelines Board and reviewed by
[SBI Legal Assistance](http://legalassistance.buffalo.edu) *2008


#### Resources

* [Landlord/tenant rights](http://subboard.com/legal/information_packets/landlord-tenant_rights.asp)

* [Considerations for choosing a roommate](http://www.ub-housing.buffalo.edu/roommates.php)
