## Disclaimer

This Website is Owned by Sub-Board I, Inc (SBI).

SBI is not owned or operated by the University at Buffalo. SBI is a student-owned and operated not-for-profit corporation completely separate and distinct from the University at Buffalo. All information on this site is listed by SBI in its sole discretion. The University at Buffalo bears no responsibility for the content of this site.

The SBI Off-Campus Housing Office does not investigate, inspect, endorse, or guarantee the accuracy of the information provided in any listing, the condition of the accommodation, or the suitability or performance of either the lister or any prospective tenant. SBI shall not be responsible for any loss or damage suffered or incurred by any individual or entity arising out of or relating to the listings. SBI shall not be deemed to be a party, and shall have no responsibility or obligation to enforce such agreement.
