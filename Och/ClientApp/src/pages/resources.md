## Resources
### Apartment Safety

SBI Off-Campus Housing asks everyone posting on our site to fill out a nine
question apartment safety checklist. We also urge renters to check UB's off
campus housing [renters
checklist](http://www.ehs.buffalo.edu/offcampus/forms/RentersChecklist.pdf)
**before signing a lease** to help determine the safety of a new apartment. A
good indicator for making the decision of whether or not to rent an apartment
is asking yourself if you would recommend the apartment to a friend.
* [Off-Campus Housing Renter's Checklist](http://subboard.com/och/rental-checklist.pdf)
Buffalo residents can now register complaints, get information, and access
non-emergency police services by calling 311 or visiting the [Buffalo 311 Call and Resolution Center Website](http://www.ci.buffalo.ny.us/Home/City_Departments/Citizens_Services/311_Self-Service)

### Quick Tips

Quick Tips Provided By [SBI Legal
Assistance](http://legalassistance.buffalo.edu) and [UB Living Off
Campus](http://www.livingoffcampus.buffalo.edu/)

#### Before you sign the lease



* Verbal promises won't hold up in court - if you want something fixed before you move in, get it in writing.

* Ask the owner to disclose any and all defects or damages with the house.

* Some leases have a "joint and several liability" clause meaning the entire amount of rent is due each month no matter what (e.g., if your roommate moves out or can't pay, you will be held responsible). Make sure you find out before signing your lease and decide whether or not this is acceptable to you.

* Choose your roommates wisely; by signing your name to a lease, you may become liable/responsible for the actions of your roommates both criminally and with the University ([Student-Wide Judiciary](http://www.ub-judiciary.buffalo.edu/swj.shtml)).

* Document (with photographs, camcorder, etc.) the condition of the entire house - outside and in - when you first move in, because your landlord may try to keep your security deposit for any damages that are present when you move out.





### Protect your interests and be a good neighbor.
* #### Location Matters
    * Visit potential neighborhoods a few times, at different times of day, before settling down.

* #### You're in charge of your own safety

    * 80% of student fatalities due to fire occur in off-campus student housing. Know the location of smoke alarms and fire extinguishers, and how to maintain them. Do not rent any apartment that is not equipped with smoke detectors.

    * Know who to call in an emergency.

    * Lock doors and windows and leave an outside light on.

    * Do not travel alone, especially at night.

    * Never use a basement or an attic as a bedroom. They become traps in the event of fire.

    * Most importantly, report any crime by calling 911 (Buffalo Police) or the E District Station at 851-4416.


* #### Insurance is cheap
    * If your belongings are stolen or destroyed in a fire, your landlord isn't responsible for replacing them. Renters insurance could save you thousands.

    * Costs as little as $12 a month.

    * Can cover fire, theft (from apartment as well as other places, such as the trunk of your car), liability claims (e.g. your dog bites someone or a guest falls at your apartment).

* #### Go shopping
    * Setting up house can cost from hundreds to thousands of dollars.
    * Include these costs in your budget: cleaning supplies, small appliances, pots and pans, light bulbs, shower curtains, plunger.

    * Resist the urge to snag a couch off the street corner (bedbugs are very common).

* #### Rent is just one cost.

    * The cost of utilities is often overlooked. Expect to spend 31 to 35 percent of
    your income on rent and utilities.
    * Before you sign a lease, contact utility companies for past utility costs and setup fees and deposits.

    * If utilities are included in your rent, find out how much control you'll have over their use.

* #### Be nosy - even pushy
    * Visit the property and examine utility closets and appliances.

    * If something breaks, keep calling until it's fixed. Some landlords may not respond unless you call them frequently.

* #### You have rights
    * Inspect the property before signing a lease and have your lease reviewed by [SBI Legal Assistance](http://legalassistance.buffalo.edu).

    * Landlords and staff can't enter your apartment at will. In general, remember that while you're paying rent, the apartment is your private home first and the landlord's property second.

    * It is illegal for a landlord to rent a bedroom in a basement or an attic that is not equipped with a fire escape.

* #### Be a good neighbor
    * Get to know your neighbors - they can be your most valuable resource.

    * Be responsible. Keep noise down and keep your property clean.

    * Join a block club or visit a neighborhood center to learn how to help your neighborhood.
### Areas



UB students live all around Western New York, but most commonly in Buffalo's
[University Heights](http://subboard.com/och/areas.asp#university_heights),
[North Buffalo](http://subboard.com/och/areas.asp#north_buffalo-hertel),
[Allentown](http://subboard.com/och/areas.asp#allentown) or
[Elmwood](http://subboard.com/och/areas.asp#elmwood_village) neighborhoods;
[Amherst](http://subboard.com/och/areas.asp#amherst) and
[Tonawanda](http://subboard.com/och/areas.asp#tonawanda). The distance between
UB's North (Amherst) and South (Buffalo, Main St.) campuses is about 2 miles.
The UB Stampede (inter-campus bus) runs every 5 to 10 minutes Monday through
Friday during the academic year and every 30 to 60 minutes on weekends and
during the summer. - Taken From [UB Living Off
Campus](http://www.livingoffcampus.buffalo.edu/).

You can narrow your apartment search to a particular location or neighborhood
by selecting an area from the drop down menu on the [search](index.asp) page.
Each listing will show maps, travel times and proximity to UB North and South
Campus and the ARTF shuttle zone. Listings in popular student areas will have
more information including transportation options, cost of living, safety
tips, and things to do.  
  
If you're still having trouble deciding on an area, visit our
[areas](areas.asp) page or [contact](contact.asp) the SBI Off-Campus Housing
Office for more information.









### Directory



#### On-Campus Resources



* [Apartment Safety](http://www.ehs.buffalo.edu/offcampus/index.htm)

* [Campus Dining and Shops](http://myubcard.com)

* [Contact SBI Off-Campus Housing](http://www.offcampushousing.buffalo.edu/contact.asp)

* [UB Living Off Campus Guide](http://www.livingoffcampus.buffalo.edu/)

* [Student Health Center](http://www.student-affairs.buffalo.edu/shs/student-health)

* [Student Pharmacy](http://www.sbi-pharmacy.buffalo.edu)

* [UB Counseling Services](http://www.student-affairs.buffalo.edu/shs/ccenter)

* [SBI Legal Assistance - Free for UB Students](http://www.legalassistance.buffalo.edu)

* [SBI Health Education](http://www.healtheducation.buffalo.edu)

* [Safety Services](http://sbihealtheducation.org/?page_id=2)

* [University Police](http://www.public-safety.buffalo.edu)







#### Off-Campus Resources



* [311 Call and Resolution Center](http://www.ci.buffalo.ny.us/Home/City_Departments/Citizens_Services/311_Self-Service)

* [Erie County Health Department](http://www.erie.gov/health/site_offices_services.asp)

* [Buffalo Police E District (UB South Campus Area)](http://maps.google.com/maps?hl=en&ie=UTF8&q=buffalo+police+e+district&fb=1&cid=0,0,15304731761808843811&z=16&iwloc=A)

* [Amherst Police](http://www.amherst.ny.us/govt/govt_dept.asp?div_id=div_23&dept_id=dept_16)

* [Buffalo City Services](http://www.city-buffalo.com/Home/CityServices)

* [Municipality Contact Information](municipality_contact.asp)







#### Complex Listings



* [List of local apartment buildings](apartments.asp)





#### Legal



* [NYS Tenant Rights Guide](http://www.oag.state.ny.us/sites/default/files/pdfs/publications/Tenant_Rights_2011.pdf)

* [Small Claims Contact](http://www.subboard.com/legal/contact.asp)

* [NYS Office of the Attorney General](http://www.ag.ny.gov/)

* [Better Business Bureau](http://welcome.bbb.org/)

* [Housing Discrimination](http://www.homeny.org/)







#### Utilities



* [National Grid (Electric)](https://www.nationalgridus.com/niagaramohawk/index.asp)

* [National Fuel (Gas)](http://www.natfuel.com/)

* [Time Warner (Cable/Internet)](http://www.timewarnercable.com/wny/)

* [Verizon (Phone/Internet)](http://www.verizon.com/)

* [Erie County Water Authority](http://www.ecwa.org/web/home.jsp)





#### Transportation



* [Metro Bus and Metro Rail](http://www.nfta.com)

* [UB Parking & Transportation](http://www.ub-parking.buffalo.edu)

* [Buffalo Car Share](http://www.buffalocarshare.org/)

* [Good Going WNY](https://www.goodgoingwny.com)







#### Settling In



* [Galleria Mall](http://www.waldengalleria.com/)

* [Boulevard Mall](http://www.boulevard-mall.com/)

* [Ikea Burlington (apx. 1 hour drive)](http://www.ikea.com/ca/en/store/burlington)

* [Home Depot Elmwood Ave](http://www.homedepot.com/webapp/wcs/stores/servlet/StoreFinderViewDetails?storeId=10051&storeNum=1234)

* [Home Depot Niagara Falls Boulevard](http://www.homedepot.com/webapp/wcs/stores/servlet/StoreFinderViewDetails?storeId=10051&storeNum=1233)

* [Target](http://sites.target.com/site/en/spot/search_results.jsp?&mapType=standard&startAddress=amherst%2C+ny&startingLat=42.97849748434709&startingLong=-78.80000173746383&_requestid=228979)

* [Walmart](http://www.walmart.com/storeLocator/ca_storefinder_details_short.do?rx_title=com.wm.www.apps.storelocator.page.serviceLink.title.default&edit_object_id=2210&rx_dest=%2Findex.gsp&sfsearch_state=&sfsearch_city=&sfsearch_zip=14214)

* [Uhaul](http://www.uhaul.com)

* [Salvation Army Thrift Store](http://maps.google.com/maps?f=q&hl=en&geocode=&q=salvation+army+thrift+store,+amherst,+ny&ie=UTF8&z=13&iwloc=A)

* [Kohls](http://www.kohlscorporation.com/maps/SiteAdvantage/advantage.asp?template=map&transaction=locMap&recordId=963&text2=1)
