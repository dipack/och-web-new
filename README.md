# UB's Off-Campus Housing website (Subboard Inc.)

## Overview
A rewrite of Subboard Inc.'s off-campus housing website, done entirely by me. Unfortunately, Subboard got shut down by University at Buffalo in July 2019, due to reasons this
README will not go into. What that meant is that this website never got to go live. I worked on this website from January 2019 to May 2019.

## Tech Stack
The web UI is written in Angular 7, with the backend written in C#, utilising .NET Core. The database used was MySQL server.

I chose .NET Core for two reasons:
1. Subboard's servers all ran Windows Server 2012, which meant that IIS was the best platform to deploy such services
2. .NET Core was open-sourced by Microsoft in [2017](https://docs.microsoft.com/en-us/dotnet/framework/get-started/net-core-and-open-source), which lead to the development
of [mono](https://www.mono-project.com/), a .NET framework implementation for Linux, thus making it easy for me to re-deploy this project to Linux-based servers, in the future,
if it ever came to it.

## Screenshots
### Landing Page
![Landing page](Och/Screenshots/landing_page.jpg)
